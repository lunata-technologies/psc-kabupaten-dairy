<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminLog extends Model
{
    protected $table = "admin_log";
    protected $guarded = [];

    public function createLog($admin_uid, $description){
        $this->admin_uid = $admin_uid;
        $this->description = $description;
        if($this->save())
            return response()->json(['error' => false, 'message' => 'Success create Admin Log !']);
        return response()->json(['error' => true, 'message' => 'Failed create Admin Log !']);        
    }
}
