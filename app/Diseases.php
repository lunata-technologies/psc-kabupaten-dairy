<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diseases extends Model
{
    protected $table = "detail_diseases";
    protected $guarded = [];
}
