<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $primaryKey = "uid";
    public $incrementing = false;
    protected $table = "employee";
    protected $guarded = [];

    public $appends = [
        'faskes',
        'faskes_detail'
    ];

    public function hospital_appends(){
        return $this->belongsTo(Hospital::class, 'rest_id');
    }

    public function getFaskesAttribute(){
        $rest_id = $this->rest_id;

        if($rest_id == 1){
            $name = "Rumah Sakit";
        }elseif($rest_id == 2){
            $name = "Puskesmas";
        }elseif($rest_id == 2){
            $name = "Posko";
        }else{
            $name = " ";
        }

        return $name;
    }

    public function getFaskesDetailAttribute(){
        return $this->hospital_appends->name;
    }
}
