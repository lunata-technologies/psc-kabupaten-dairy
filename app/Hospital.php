<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hospital extends Model
{
    protected $table = "hospitals";
    protected $guarded = [];
    use SoftDeletes;
}
