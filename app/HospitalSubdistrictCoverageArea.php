<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HospitalSubdistrictCoverageArea extends Model
{
    protected $table = 'hospitals_coverage_subdistrict_area';
    protected $guarded = [];
    protected $appends = ['subdistrict_name'];

    public function subdistrictAppend(){
        return $this->belongsTo(SubDistrict::class, 'subdistrict_id');
    }
    public function getSubdistrictNameAttribute(){
        return $this->subdistrictAppend->name ?? '-';
    }
}
