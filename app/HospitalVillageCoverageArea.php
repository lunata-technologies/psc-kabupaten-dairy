<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HospitalVillageCoverageArea extends Model
{
    protected $table = 'hospital_coverage_village_area';
    protected $guarded = [];
    protected $appends = ['village_name'];

    public function villageAppends(){
        return $this->belongsTo(Village::class, 'village_id');
    }
    public function getVillageNameAttribute(){
        return $this->villageAppends->name ?? '-';
    }
}
