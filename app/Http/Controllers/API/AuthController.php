<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\People;
use App\Employee;
use App\PeopleDevice;
use App\SubDistrict;

class AuthController extends Controller
{
    public function login(Request $request){
        $people = People::where('phone',$request->phone)->first();
        if($people){
            if(Hash::check($request->password, $people->password)){
                $data['token'] = $people->createToken('nApp')->accessToken;
                $data['uid'] = $people->uid;
                $data['device_id'] = $request->device_id;
                if(!PeopleDevice::where('device_id',$request->device_id)->first())
                    PeopleDevice::create([
                        'people_uid' => $people->uid,
                        'device_id' => $request->device_id,
                    ]);
                return response()->json(['error' => false, 'message'=>'Success authenticated Data !', 'data' => $data],200);
            }else{
                return response()->json(['error' => true, 'message' => 'Passowrd doesn\'t match with email you\'ve entered !'],401);
            }
        }else{
            return response()->json(['error' => true, 'message'=>'Email not found in our system, we\'re sorry !!!'],401);
        }
    }
    public function register(Request $request){
        $requestData = $request->all();
        // Check if the locations are accepted
        if(!SubDistrict::find($request['sub-district-id'])) return response()->json(['error'=>true, 'Sub District might be out of our coverage area !'], 401);

        // Check for Redundant Data in Phone and NIK
        if(People::where('nik',$request->nik)->where('deleted_at',null)->first()) return response()->json(['error' => true,'message' => 'NIK already exist!'],200);
        if(People::where('phone',$request->phone)->where('deleted_at',null)->first()) return response()->json(['error' => true,'message' => 'Phone Number already exist!'],200);

        // Check if File is being attached
        if($request->hasFile('avatar')){
            if (!in_array($request->file('avatar')->getClientOriginalExtension(), array('jpg', 'jpeg', 'png'))) return response()->json(['error' => true, 'message' => 'File type is not supported, support only JPG, JPEG and PNG !'], 500);
            if($request->file('avatar')->getSize() > 2e+6) return response()->json(['error' => true, 'message' => 'File size is too big, Limited for 2 MB'], 500);
            
            // Store File attached to the Public Folder in this Root Directory
            $request->file('avatar')->move('people/','avatar-'.$request->file('avatar')->getClientOriginalName());
            $requestData['avatar'] = asset('people/avatar-'.$request->file('avatar')->getClientOriginalName());
        }

        
        $requestData['password'] = bcrypt($request->password);
        $insert = People::create($requestData);
        if($insert)
            return response()->json(['error' => false,'message' => 'Success registtred data !'],200);
        return response()->json(['error' => false,'message' => 'Failed registtred data, someting went wrong !'],500);
    }
    public function registerVerifyingSelfie(Request $request){
        $requestData = $request->all();
        if($request->hasFile('selfie')){
            if (!in_array($request->file('selfie')->getClientOriginalExtension(), array('jpg', 'jpeg', 'png'))) return response()->json(['error' => true, 'message' => 'File type is not supported, support only JPG, JPEG and PNG !'], 500);
            if($request->file('selfie')->getSize() > 5e+6) return response()->json(['error' => true, 'message' => 'File size is too big, Limited for 5 MB'], 500);
            // Store File attached to the Public Folder in this Root Directory
            $request->file('selfie')->move('people/','selfie-'.$request->uid.'-'.$request->file('selfie')->getClientOriginalName());
            $requestData['selfie'] = asset('people/selfie-'.$request->uid.'-'.$request->file('selfie')->getClientOriginalName());
            $people = People::find($request->uid);
            $people->selfie = $requestData['selfie'];
            if($people->save())
                return response()->json(['error'=>false,'message'=>'Success stored data !'],200);
            return response()->json(['error'=>true,'message'=>'Failed stored data, something went wrong !'],500);
        }else{
            return response()->json(['error' => true, 'message' => 'Image for KTP is requested'],500);
        }
    }
    public function registerVerifyingSelfieKtp(Request $request){
        $requestData = $request->all();
        if($request->hasFile('selfie-ktp')){
            if (!in_array($request->file('selfie-ktp')->getClientOriginalExtension(), array('jpg', 'jpeg', 'png'))) return response()->json(['error' => true, 'message' => 'File type is not supported, support only JPG, JPEG and PNG !'], 500);
            if($request->file('selfie-ktp')->getSize() > 5e+6) return response()->json(['error' => true, 'message' => 'File size is too big, Limited for 5 MB'], 500);
            // Store File attached to the Public Folder in this Root Directory
            $request->file('selfie-ktp')->move('people/','selfie-ktp-'.$request->uid.'-'.$request->file('selfie-ktp')->getClientOriginalName());
            $requestData['selfie-ktp'] = asset('people/selfie-ktp-'.$request->uid.'-'.$request->file('selfie-ktp')->getClientOriginalName());
            $people = People::find($request->uid);
            $people['selfie-ktp'] = $requestData['selfie-ktp'];
            if($people->save())
                return response()->json(['error'=>false,'message'=>'Success stored data !'],200);
            return response()->json(['error'=>true,'message'=>'Failed stored data, something went wrong !'],500);
        }else{
            return response()->json(['error' => true, 'message' => 'Image for selfie-ktp is requested'],500);
        }
    }
    public function UserData(Request $request){
        $data = People::find($request->uid);
        if($data)
            return response()->json(['error'=>false, 'message' => 'Success retrived data !', 'data' => $data], 200);
        return response()->json(['error'=>true, 'message' => 'Data not found !'], 401);
    }



    // Pegawai
    public function loginPegawai(Request $request){
        $data = Employee::where('username',$request->username)->first();
        if($data){
            if($request->password == $data['password']){
                if(!PeopleDevice::where('device_id',$request->device_id)->first())
                    PeopleDevice::create([
                        'people_uid' => $data->uid,
                        'device_id' => $request->device_id,
                    ]);
                return response()->json(['error'=>false,'message' => 'login success', 'data' => $data], 200);
            }
            return response()->json(['error'=>true,'message' => 'login failed, password is wrong'], 401);
        }
        return response()->json(['error'=>true,'message' => 'login failed, username is wrong'], 401);
    }
    public function logout(Request $request){
        $data =  PeopleDevice::where('people_uid', $request->uid)->get();
        foreach($data as $item){
            $item->delete();
        }
        return response()->json(['error'  => false, 'message' => 'Logout Success !'], 200);
    }
}
