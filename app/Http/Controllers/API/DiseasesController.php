<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Diseases;

class DiseasesController extends Controller
{
    public function getDiseases(){
        $data = Diseases::where('deleted_at',null)->get();
        return response()->json(['error' => false, 'message'=>'Success retrived Data !', 'data' => $data], 200);
    }
}
