<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\People;
use App\PeopleReport;
use App\PeopleDevice;
use App\Hospital;

class PeopleController extends Controller
{

    public static function sendGCM($title, $message, $device_id) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array (
                'registration_ids' => array (
                        $device_id
                ),
                'notification' => array(
                    "title" => $title,
                    "body"=> $message,
                    "sound" => "sirine",
                )
        );
        $fields = json_encode ( $fields );

        $headers = array (
                'Authorization: key=' . "AAAA10xTCKE:APA91bG1sk_5RCFuQxoc4yWnvcFihYR-6AGXwk5wNI4XhefNUpNltJ_DMQ6sdriKzYmOnViJMe-UBUDdpUKRzYxR2zg4ng7143dbcQAIJJxermQzoNM_PQvzC_jUEKqu-ZVdKPvXrE4q",
                'Content-Type: application/json'
        );
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        echo $result;
        curl_close ( $ch );
    }

    // People Report
    public function peopleReport(Request $request){
        // Validating
        $rules = [
            'report_type' => 'required',
            'longtitude' => 'required',
            'latitude' => 'required',
        ];
        $messages = [
            'report_type.required' => 'Cannot be Empty !',
            'longtitude.required' => 'Cannot be Empty !',
            'latitude.required' => 'Cannot be Empty !',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails())
            return response()->json(['error' => true, 'message' => $validator->errors()], 500);
        $requestData = $request->all();
        if($request->report_type != '3'){
            if($request->hasFile('prove_images1')){
                if (!in_array($request->file('prove_images1')->getClientOriginalExtension(), array('jpg', 'jpeg', 'png'))) return response()->json(['error' => true, 'message' => 'File type is not supported, support only JPG, JPEG and PNG !'], 500);
                if($request->file('prove_images1')->getSize() > 2e+6) return response()->json(['error' => true, 'message' => 'File size is too big, Limited for 2 MB'], 500);
                
                
                // if (!in_array($request->file('prove_images2')->getClientOriginalExtension(), array('jpg', 'jpeg', 'png'))) return response()->json(['error' => true, 'message' => 'File type is not supported, support only JPG, JPEG and PNG !'], 500);
                // if($request->file('prove_images2')->getSize() > 2e+6) return response()->json(['error' => true, 'message' => 'File size is too big, Limited for 2 MB'], 500);
                // $requestData['prove_images2'] = asset('people-report/prove_images2-'.$request->file('prove_images2')->getClientOriginalName());
                
                // Store File attached to the Public Folder in this Root Directory
                $request->file('prove_images1')->move('people-report/','prove_images1-'.$request->file('prove_images1')->getClientOriginalName());
                // $request->file('prove_images2')->move('people-report/','prove_images2-'.$request->file('prove_images2')->getClientOriginalName());

                // Image name will be stored in database
                $requestData['prove_images1'] = asset('people-report/prove_images1-'.$request->file('prove_images1')->getClientOriginalName());
            }else{
                return response()->json(['error' => true, 'message' => 'Prove Image Requested !'], 500);
            }
        }
        $requestData['tmp_created_date'] = Carbon::now()->format('Y-m-d').' 00:00:00';
        if(PeopleReport::create($requestData))
            return response()->json(['error' => false, 'message' => 'Data is stored !'], 200);
        return response()->json(['error' => true, 'message' => 'Data failed to be stored !'], 500);
    }

    // Report Status
    public function reportStatus(Request $request){
        $data = PeopleReport::where('people_uid',$request->uid)->where('rest_confirmed',null)->first();

        if($data){
            if($data['admin_confirmed'] == null && $data['rescuer_confirmed'] == null)
                return response()->json(['error' => false, 'message' => 'Menunggu konfirmasi admin !'],200);
            if($data['admin_confirmed'] != null && $data['rescuer_confirmed'] == null)
                return response()->json(['error' => false, 'message' => 'Sudah dikonfirmasi oleh admin !'],200);
            if($data['rescuer_confirmed'] != null && $data['rescuer_otw'] != null && $data['rescuer_arrived_people'] == null)
                return response()->json(['error' => false, 'message' => 'Ambulance sedang menuju lokasi anda !'],200);
            if($data['rescuer_confirmed'] != null && $data['rescuer_otw'] != null && $data['rescuer_arrived_people'] != null && $data['rescuer_arrived_hospital'] == null)
                return response()->json(['error' => false, 'message' => 'Ambulance tiba di lokasi anda !'],200);
            if($data['rescuer_confirmed'] != null && $data['rescuer_otw'] != null && $data['rescuer_arrived_people'] != null && $data['rescuer_arrived_hospital'] != null && $data['rest_confirmed'] == null)
                return response()->json(['error' => false, 'message' => 'Ambulance sedang menuju ke rumah sakit !'],200);
            if($data['rescuer_confirmed'] != null && $data['rescuer_otw'] != null && $data['rescuer_arrived_people'] != null && $data['rescuer_arrived_hospital'] != null && $data['rest_confirmed'] != null)
                return response()->json(['error' => false, 'message' => 'Ambulance telah tiba dirumah sakit !'],200);
        }else{
            return response()->json(['error' => false, 'message' => 'Belum ada laporan berjalan !'], 200);
        }

    }

    // Get Report Detail For Posko Employee
    public function reportData(Request $request){
        $data = PeopleReport::where('rescuer_id', $request->rescuer_id)->where('rest_confirmed',null)->get();
        if($data)
            return response()->json(['error' => false, 'message' => 'Success retrived data !', 'data' => $data], 200);
        return response()->json(['error' => true, 'message' => 'failed retrived data !', 'data' => $data], 401);
    }

    // Rescuer Confirmation
    public function confirmPickup(Request $request){
        $data = PeopleReport::find($request->report_id);
        $devices = PeopleDevice::where('people_uid',$data['people_uid'])->get();
        if($request->status == 'pickup'){
            $data->rescuer_confirmed = Carbon::now();
            $data->rescuer_otw = Carbon::now();
            foreach($devices as $device)
                Self::sendGCM('Ambulance akan datang !', 'Hore ! Ambulance sedang dalam perjalanan !', $device['device_id']);
        }
        else if($request->status == 'arrived_people')
            $data->rescuer_arrived_people = Carbon::now();
        else if($request->status == 'go_hospital')
            $data->rescuer_arrived_hospital = Carbon::now();
        else if($request->status == 'arrived_hospital')
            $data->rest_confirmed = Carbon::now();
        if($data->save())
            return response()->json(['error' => false, 'message' => 'Success changed status !'], 200);
        return response()->json(['error' => true, 'message' => 'Server error !'], 500);
    }

    // Report Log
    public function laporanLog(Request $request){
        if($request->status == "ongoing")
            $data = PeopleReport::where('people_uid',$request->people_uid)->where('admin_confirmed','<>',null)->where('rescuer_arrived_hospital',null)->get();
        if($request->status == "finished")
            $data = PeopleReport::where('people_uid',$request->people_uid)->where('rescuer_arrived_hospital','<>',null)->get();
        return response()->json(['error' => false, 'message' => 'Success retrived data !', 'data' => $data], 200);
    }
}
