<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SubDistrict;
use App\Village;

class TerritoryController extends Controller
{
    public function getSubDistrict(){
        $data = SubDistrict::where('deleted_at',null)->get();
        return response()->json(['error'=>false, 'message'=>'Success retrived data !', 'data' => $data], 200);
    }
    public function getVillageArea($id){
        $data = Village::where('sub-district-id',$id)->where('deleted_at',null)->get();
        return response()->json(['error' => false, 'message'=>'Success retriced data', 'data' => $data], 200);
    }
}
