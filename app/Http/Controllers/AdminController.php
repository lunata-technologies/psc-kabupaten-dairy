<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use Illuminate\Support\Facades\Hash;
use Session;

class AdminController extends Controller
{
    public function register(Request $request){
        $requestData = $request->all();
        $requestData['password'] = bcrypt($request->password);
        if(User::create($requestData))
            return redirect('admin')->with('success', $request->name. ' is registred !');
        return redirect(url('admin/register'))->with('failed', $request->name. ' has been failed to be registred, something went wrong !');
    }

    public function login(Request $request){
        if(!$request->all()){
            return view('auth.login');
        }else{
            $email = $request->email;
            $password = $request->password;

            $pegawai = User::where('email', $email)->first();

            if($pegawai){
                if(Hash::check($password, $pegawai->password)){
                    Session::put('nama_pegawai',$pegawai->name);
                    Session::put('email',$pegawai->email);
                    Session::put('uid',$pegawai->uid);
                    Session::put('login',TRUE);
                    return redirect('/');
                }else{
                    return redirect('login')->with('failed', 'Password Salah!');
                }
            }else{
                return redirect('login')->with('failed', 'Email tidak terdaftar!');
            }
        }
    }

    public function logout(){
        Session::flush();
        return redirect('login');
    }
}
