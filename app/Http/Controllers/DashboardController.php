<?php

namespace App\Http\Controllers;
use App\PeopleReport;
use App\People;
use App\Hospital;
use App\Diseases;
use App\User;
use App\SubDistrict;
use Carbon\Carbon;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function grafikLaporanPerJenisAduan($awal, $akhir){
        $awal = Carbon::create($awal)->format('Y-m-d');
        $akhir = Carbon::create($akhir)->format('Y-m-d');
        $range = Carbon::parse($awal)->diffInDays($akhir)+1;

        $tmpTanggal = [];
        $tmpBulan = [];
        $tmpTahun = [];
        $tmpDate = [];
        $tmpTanggalIndo = [];
        for($i = 0; $i < $range; $i++){
            $date = Carbon::parse($awal)->addDays($i)->toDateString();
            $tanggal = Carbon::create($date)->format('d');
            $bulan = Carbon::create($date)->format('m');
            $tahun = Carbon::create($date)->format('Y');
            $tmp = $tanggal . "-" . $bulan;
            $tmpTglIndo = $tahun . "-" . $bulan . "-" .$tanggal;

            array_push($tmpDate, $tmp);
            array_push($tmpTanggal, $tanggal);
            array_push($tmpBulan, $bulan);
            array_push($tmpTahun, $tahun);
            array_push($tmpTanggalIndo, $tmpTglIndo);
        }

        $statusName[0] = ['Diri Sendiri'];
        $statusName[1] = ['Orang Lain'];
        $statusName[2] = ['Panic Button'];

        $jenisPengaduan = [];
        $tmpJenisPengaduan = [];
        $k = 1;
        for($i = 0; $i < 3; $i++){
            $nilai = 0;
            $j = 0;
            for($l = 0; $l < count($tmpDate); $l++){
                $nilai = PeopleReport::where('report_type', $k)
                ->whereDay('created_at', $tmpTanggal[$l])
                ->whereMonth('created_at', $tmpBulan[$l])
                ->whereYear('created_at', $tmpTahun[$l])
                ->count();

                array_push($tmpJenisPengaduan, $nilai);
                $j++;
            }
            $k++;
            $jenisPengaduan[$i]['name'] = $statusName[$i];
            $jenisPengaduan[$i]['data'] = $tmpJenisPengaduan;
            $tmpJenisPengaduan = [];
        }

        return $jenisPengaduan;
    }

    public function grafikLaporanPerJenisPenyakit($awal, $akhir){
        $awal = Carbon::create($awal)->format('Y-m-d');
        $akhir = Carbon::create($akhir)->format('Y-m-d');

        $dataPenyakit = Diseases::orderBy('nama', 'asc')->get();
        $jenisPenyakit = [];
        $tmpPenyakit = [];
        $tmp = [];
        $j = 0;
        foreach ($dataPenyakit as $key => $dp2) {
            $tmpPenyakit['name'] = $dp2->nama;
            $tmpPenyakit['y'] = PeopleReport::where('diseases_id', $dp2->id)
            ->whereRaw(
                "(created_at >= ? AND created_at <= ?)",
                [$awal." 00:00:00", $akhir." 23:59:59"]
            )->count();
            $j++;
            array_push($jenisPenyakit, $tmpPenyakit);
        }
        return $jenisPenyakit;
    }

    public function grafikLaporanPerKecamatan($awal, $akhir){
        $awal = Carbon::create($awal)->format('Y-m-d');
        $akhir = Carbon::create($akhir)->format('Y-m-d');

        $statusName[0] = ['Diri Sendiri'];
        $statusName[1] = ['Orang Lain'];
        $statusName[2] = ['Panic Button'];

        $kecamatan = SubDistrict::orderBy('name', 'asc')->get();
        $tmpKecamatan = [];
        $aduanPerkecamatan = [];
        $tmpAduanPerkecamatan = [];
        $j = 0;

        for($i = 0; $i < 3; $i++){
            $aduanPerkecamatan[$i]['name'] = $statusName[$i];
        }

        foreach($kecamatan as $key => $value){
            $tmpKecamatan[$j] = $value->name;
            $j++;
        }

        $j = 1;
        for($i = 0; $i < 3; $i++){
            $nilai = 0;
            $orangLain = 0;
            $panicButton = 0;
            foreach($kecamatan as $key => $value){
                $nilai = PeopleReport::where([
                    'subdistrict_id' => $value->id,
                    'report_type' => $j
                ])
                ->whereRaw(
                    "(created_at >= ? AND created_at <= ?)",
                    [$awal." 00:00:00", $akhir." 23:59:59"]
                )->count();

                array_push($tmpAduanPerkecamatan, $nilai);

            }
            $j++;
            $aduanPerkecamatan[$i]['data'] = $tmpAduanPerkecamatan;
            $tmpAduanPerkecamatan = [];
        }

        return $aduanPerkecamatan;
    }

    public function dataKecamatan(){
        $kecamatan = SubDistrict::orderBy('name', 'asc')->get();
        $tmpKecamatan = [];
        $j = 0;
        foreach($kecamatan as $key => $value){
            $tmpKecamatan[$j] = $value->name;
            $j++;
        }
        return $tmpKecamatan;
    }

    public function dataTanggal($awal, $akhir){
        $awal = Carbon::create($awal)->format('Y-m-d');
        $akhir = Carbon::create($akhir)->format('Y-m-d');
        $range = Carbon::parse($awal)->diffInDays($akhir)+1;

        // $tmpTanggal = [];
        // $tmpBulan = [];
        // $tmpTahun = [];
        $tmpDate = [];
        $tmpTanggalIndo = [];
        for($i = 0; $i < $range; $i++){
            $date = Carbon::parse($awal)->addDays($i)->toDateString();
            $tanggal = Carbon::create($date)->format('d');
            $bulan = Carbon::create($date)->format('m');
            $tahun = Carbon::create($date)->format('Y');
            $tmp = $tanggal . "-" . $bulan . "-" . $tahun;
            // $tmpTglIndo = $tahun . "-" . $bulan . "-" .$tanggal;

            array_push($tmpDate, $tmp);
            // array_push($tmpTanggal, $tanggal);
            // array_push($tmpBulan, $bulan);
            // array_push($tmpTahun, $tahun);
            // array_push($tmpTanggalIndo, $tmpTglIndo);
        }

        return $tmpDate;
    }

    public function index(Request $request){
        if($request->awal == null || $request->akhir == null){
            $reqAwal = Carbon::now('Asia/Jakarta')->firstOfMonth()->toDateString();
            $reqAkhir = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        }else{
            $reqAwal = $request->awal;
            $reqAkhir = $request->akhir;
        }

        $awal = Carbon::create($reqAwal)->format('Y-m-d');
        $akhir = Carbon::create($reqAkhir)->format('Y-m-d');
        $range = Carbon::parse($awal)->diffInDays($akhir)+1;

        $countMember = People::count();
        $countSelfReport = PeopleReport::where('report_type', 1)->count();
        $countOtherReport = PeopleReport::where('report_type', 2)->count();
        $countPanicReport = PeopleReport::where('report_type', 3)->count();

        $jenisPengaduan = $this->grafikLaporanPerJenisAduan($awal, $akhir);
        $jenisPenyakit = $this->grafikLaporanPerJenisPenyakit($awal, $akhir);
        $aduanPerkecamatan = $this->grafikLaporanPerKecamatan($awal, $akhir);
        $tmpKecamatan = $this->dataKecamatan();
        $tmpDate = $this->dataTanggal($awal, $akhir);

        // $tmpTanggal = [];
        // $tmpBulan = [];
        // $tmpTahun = [];
        // $tmpDate = [];
        // $tmpTanggalIndo = [];
        // for($i = 0; $i < $range; $i++){
        //     $date = Carbon::parse($awal)->addDays($i)->toDateString();
        //     $tanggal = Carbon::create($date)->format('d');
        //     $bulan = Carbon::create($date)->format('m');
        //     $tahun = Carbon::create($date)->format('Y');
        //     $tmp = $tanggal . "-" . $bulan;
        //     $tmpTglIndo = $tahun . "-" . $bulan . "-" .$tanggal;

        //     array_push($tmpDate, $tmp);
        //     array_push($tmpTanggal, $tanggal);
        //     array_push($tmpBulan, $bulan);
        //     array_push($tmpTahun, $tahun);
        //     array_push($tmpTanggalIndo, $tmpTglIndo);
        // }

        // $dataPenyakit = Diseases::orderBy('nama', 'asc')->get();
        // $statusName[0] = ['Diri Sendiri'];
        // $statusName[1] = ['Orang Lain'];
        // $statusName[2] = ['Panic Button'];

        // $jenisPengaduan = [];
        // $tmpJenisPengaduan = [];
        // $k = 1;
        // for($i = 0; $i < 3; $i++){
        //     $nilai = 0;
        //     $j = 0;
        //     for($l = 0; $l < count($tmpDate); $l++){
        //         $nilai = PeopleReport::where('report_type', $k)
        //         ->whereDay('created_at', $tmpTanggal[$l])
        //         ->whereMonth('created_at', $tmpBulan[$l])
        //         ->whereYear('created_at', $tmpTahun[$l])
        //         ->count();

        //         array_push($tmpJenisPengaduan, $nilai);
        //         $j++;
        //     }
        //     $k++;
        //     $jenisPengaduan[$i]['name'] = $statusName[$i];
        //     $jenisPengaduan[$i]['data'] = $tmpJenisPengaduan;
        //     $tmpJenisPengaduan = [];
        // }

        // $jenisPenyakit = [];
        // $tmpPenyakit = [];
        // $tmp = [];
        // $j = 0;
        // foreach ($dataPenyakit as $key => $dp2) {
        //     $tmpPenyakit['name'] = $dp2->nama;
        //     $tmpPenyakit['y'] = PeopleReport::where('diseases_id', $dp2->id)
        //     ->whereRaw(
        //         "(created_at >= ? AND created_at <= ?)",
        //         [$awal." 00:00:00", $akhir." 23:59:59"]
        //     )->count();
        //     $j++;
        //     array_push($jenisPenyakit, $tmpPenyakit);
        // }

        // $kecamatan = SubDistrict::orderBy('name', 'asc')->get();
        // $tmpKecamatan = [];
        // $aduanPerkecamatan = [];
        // $tmpAduanPerkecamatan = [];
        // $j = 0;

        // for($i = 0; $i < 3; $i++){
        //     $aduanPerkecamatan[$i]['name'] = $statusName[$i];
        // }

        // foreach($kecamatan as $key => $value){
        //     $tmpKecamatan[$j] = $value->name;
        //     $j++;
        // }

        // $j = 1;
        // for($i = 0; $i < 3; $i++){
        //     $nilai = 0;
        //     $orangLain = 0;
        //     $panicButton = 0;
        //     foreach($kecamatan as $key => $value){
        //         $nilai = PeopleReport::where([
        //             'subdistrict_id' => $value->id,
        //             'report_type' => $j
        //         ])
        //         ->whereRaw(
        //             "(created_at >= ? AND created_at <= ?)",
        //             [$awal." 00:00:00", $akhir." 23:59:59"]
        //         )->count();

        //         array_push($tmpAduanPerkecamatan, $nilai);

        //     }
        //     $j++;
        //     $aduanPerkecamatan[$i]['data'] = $tmpAduanPerkecamatan;
        //     $tmpAduanPerkecamatan = [];
        // }

        return view('dashboard.index', compact('reqAwal', 'reqAkhir', 'tmpDate', 'countMember', 'countSelfReport', 'countOtherReport', 'countPanicReport', 'jenisPengaduan', 'jenisPenyakit', 'tmpKecamatan', 'aduanPerkecamatan'));
    }

    public function Notif(){
        $startNotif = Carbon::now('Asia/Jakarta')->subDay(3)->format('Y-m-d');
        $endNotif = Carbon::now('Asia/Jakarta')->format('Y-m-d');

        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->whereRaw(
            "(created_at >= ? AND created_at <= ?)",
            [$startNotif." 00:00:00", $endNotif." 23:59:59"]
        )->get();

        return $notif;
    }

    // ADmin
    public function admin(){
        $data = User::where('deleted_at',null)->get();
        return view('dashboard.admin', compact('data'));
    }
    public function deleteAdmin($uid){
        $data = User::find($uid);
        $data->delete();
        return redirect('admin')->with('success', 'Success deleted Data !');
    }
}
