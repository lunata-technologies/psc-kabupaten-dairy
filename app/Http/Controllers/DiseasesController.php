<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diseases;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Datatables;
use Carbon\Carbon;
use App\PeopleReport;

class DiseasesController extends Controller
{
    public function index(Request $request){
        if(!$request->all()){
            $notif = PeopleReport::orderBy('created_at', 'desc')
            ->get();
            return view('diseases.index', compact('notif'));
        }else{
            $rules = [
                'nama' => 'required'
            ];
        }

        $messages = [
            'nama.required' => 'Cannot be empty !',
        ];



        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return redirect('diseases')->with('failed', 'Gagal, Data Tidak Boleh Kosong !')->withErrors($validator)->withInput();
        }else{
            $penyakit = Diseases::insert([
                'nama' => $request->nama,
                'code' => $request->code,
                'created_at' => Carbon::now('Asia/Jakarta'),
                'updated_at' => Carbon::now('Asia/Jakarta')
            ]);

            if($penyakit){
                return redirect('diseases')->with('success', 'Sukses Menambahkan Data Baru !');
            }else{
                return redirect('diseases')->with('failed', 'Gagal Menambahkan Data Baru !');
            }
        }
    }

    public function penyakitDatatable(){
        $penyakit = DB::table('detail_diseases')
        // ->select('id', DB::raw('ROW_NUMBER() OVER(ORDER BY nama ASC) AS Row'), 'nama')
        ->orderBy('nama', 'asc')
        ->whereNull('deleted_at')
        ->get();

        return Datatables::of($penyakit)
        // ->addColumn('row',function($penyakit){
        //     $row = $penyakit->row;

        //     return $row;
        // })
        ->addColumn('nama',function($penyakit){
            $nama = $penyakit->nama;

            return $nama;
        })
        ->addColumn('action',function($penyakit){
            $link_delete = "'Hapus', 'delete-penyakit/".$penyakit->id."', 'Data akan dihapus, Yakin ?'";

            // $detail = '<span class="btn-sm btn-info py-1 px-2"><i class="fas fa-info-circle"></i></span>';
            $edit = '<span class="btn-sm btn-success py-1 px-2" data-toggle="modal" data-target="#update-penyakit" id="btn-update-detail" id_penyakit="'.$penyakit->id.'"><i class="fas fa-edit"></i></span>';
            $hapus = '<span title="Un Approve" class="btn-sm btn-danger py-1 px-2" onclick="confirm_me('.$link_delete.')"><i class="fas fa-trash"></i></span>';

            return $edit . ' ' . $hapus;
        })
        ->rawColumns(['nama', 'action'])
        ->toJson();
    }

    public function deletePenyakit($id){
        $delete = Diseases::where('id', $id)->delete();
        if($delete){
            return redirect(url('diseases'))->with('success', 'Data berhasil dihapus!');
        }else{
            return redirect(url('diseases'))->with('failed', 'Data gagal dihapus!');
        }
    }

    public function detailPenyakit($id){
        $detail = Diseases::where('id', $id)->first();
        return $detail;
    }

    public function updatePenyakit(Request $request){
        $rules = [
            'update_nama' => 'required'
        ];

        $messages = [
            'update_nama.required' => 'Cannot be empty !'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return redirect('diseases')->with('failed', 'Gagal, data tidak boleh kosong !')->withErrors($validator)->withInput();
        }else{
            $update = Diseases::where('id', $request->update_id)
            ->update([
                'nama' => $request->update_nama
            ]);

            if($update){
                return redirect('diseases')->with('success', 'Berhasil mengubah data !');
            }else{
                return redirect('diseases')->with('failed', 'Gagal mengubah data !');
            }
        }
    }
}
