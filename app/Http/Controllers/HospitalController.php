<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use App\HospitalSubdistrictCoverageArea;
use App\HospitalVillageCoverageArea;
use App\SubDistrict;
use App\Village;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\PoskoVillageCoverageArea;
use Datatables;
use App\Employee;
use Illuminate\Support\Facades\Validator;
use App\PeopleReport;

class HospitalController extends Controller
{
    public function index(Request $request){
        if(!$request->all()){
            $notif = PeopleReport::orderBy('created_at', 'desc')
            ->get();
            return view('hospitals.index', compact('notif'));
        }else{
            $rules = [
                'nama' => 'required',
                'alamat' => 'required',
                'latitude' => 'required',
                'longitude' => 'required'
            ];

            $messages = [
                'nama.required' => 'Tidak Boleh Kosong !',
                'alamat.required' => 'Tidak Boleh Kosong !',
                'latitude.required' => 'Tidak Boleh Kosong !',
                'longitude.required' => 'Tidak Boleh Kosong !'
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                return redirect('hospitals')->with('failed', 'Gagal, Data tidak boleh kosong !')->withErrors($validator)->withInput();
            }else{
                $hospital = Hospital::insert([
                    'name' => $request->nama,
                    'address' => $request->alamat,
                    'tipe' => 1,
                    'longtitude' => $request->longitude,
                    'latitude' => $request->latitude,
                    'created_at' => Carbon::now('Asia/Jakarta'),
                    'updated_at' => Carbon::now('Asia/Jakarta')
                ]);
                if($hospital){
                    return redirect(url('hospitals'))->with('success',' Rumah Sakit baru berhasil ditambahkan !');
                }else{
                    return redirect(url('hospitals'))->with('failed',' Rumah Sakit gagal ditambahkan !');
                }
            }
        }
    }

    public function hospitalDatatable(){
        $hospital = DB::table('hospitals')
        // ->select('id', DB::raw('ROW_NUMBER() OVER(ORDER BY name ASC) AS Row'), 'name', 'address')
        ->orderBy('name', 'asc')
        ->whereNull('deleted_at')
        ->where('tipe', 1)
        ->get();

        return Datatables::of($hospital)
        // ->addColumn('row',function($hospital){
        //     $row = $hospital->row;

        //     return $row;
        // })
        ->addColumn('nama',function($hospital){
            $nama = $hospital->name;

            return $nama;
        })
        ->addColumn('alamat',function($hospital){
            $alamat = $hospital->address;

            return $alamat;
        })
        ->addColumn('action',function($hospital){
            $link_delete = "'Hapus', 'delete-hospital/".$hospital->id."', 'Data akan dihapus, Yakin ?'";
            $link_info = url('detail-hospital/'.$hospital->id);
            // $detail = '<span class="btn-sm btn-info py-1 px-2"><i class="fas fa-info-circle"></i></span>';

            $info = '<span class="btn-sm btn-info py-1 px-2"><a href="'.$link_info.'"><i class="fas text-white fa-eye"></i></a></span>';
            $edit = '<span class="btn-sm need-auth btn-success py-1 px-2" data-toggle="modal" data-target="#update-rumah-sakit" id="btn-update-detail" id_rumah_sakit="'.$hospital->id.'"><i class="fas fa-edit"></i></span>';
            $hapus = '<span title="Un Approve" class="btn-sm need-auth btn-danger py-1 px-2" onclick="confirm_me('.$link_delete.')"><i class="fas fa-trash"></i></span>';

            return $info . ' ' . $edit . ' ' . $hapus;
        })
        ->rawColumns(['nama', 'alamat', 'action'])
        ->toJson();
    }

    public function deleteHospital(Request $request, $id){
        $delete = Hospital::where('id', $id)->delete();
        if($delete){
            return redirect(url('hospitals'))->with('success', 'Rumah Sakit berhasil dihapus!');
        }else{
            return redirect(url('hospitals'))->with('failed', 'Rumah Sakit gagal dihapus!');
        }
    }

    public function detailHospital(Request $request, $id){
        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->get();
        $data = Hospital::where('id', $id)->first();
        $employeeSupir = Employee::where('rest_id', $id)->where('type',3)->get();
        $employeePerawat = Employee::where('rest_id', $id)->where('type',2)->get();
        $employeeDokter = Employee::where('rest_id', $id)->where('type',1)->get();
        $poskoVillageCoverArea = HospitalVillageCoverageArea::where('hospital_id',$data['id'])->get();
        $subdistrict = SubDistrict::where('deleted_at',null)->get();
        if(!$request->all())
            return view('hospitals.hospital-detail',compact('data','employeeSupir','notif', 'employeePerawat', 'employeeDokter','poskoVillageCoverArea', 'subdistrict'));
        $requestData  = $request->all();
        $requestData['rest_id'] = $id;
        if(Employee::create($requestData))
            return redirect(url('detail-hospital/'.$id))->with('success','Sukses menambahkan pegawai baru !');
        return redirect(url('detail-hospital/'.$id))->with('failed','Gagal menambahkan pegawai baru ! Hubungi Developer');
    }

    public function updateHospital(Request $request){
        $rules = [
            'update_id' => 'required',
            'update_nama' => 'required',
            'update_alamat' => 'required',
            'update_latitude' => 'required',
            'update_longitude' => 'required'
        ];

        $messages = [
            'update_id.required' => 'Tidak boleh kosong !',
            'update_nama.required' => 'Tidak boleh kosong !',
            'update_alamat.required' => 'Tidak boleh kosong !',
            'update_latitude.required' => 'Tidak Boleh Kosong !',
            'update_longitude.required' => 'Tidak Boleh Kosong !'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return redirect('hospitals')->with('failed', 'Gagal, data tidak boleh kosong !')->withErrors($validator)->withInput();
        }else{
            $update = Hospital::where('id', $request->update_id)
            ->update([
                'name' => $request->update_nama,
                'address' => $request->update_alamat,
                'longtitude' => $request->update_longitude,
                'latitude' => $request->update_latitude
            ]);

            if($update){
                return redirect('hospitals')->with('success', 'Berhasil mengubah data !');
            }else{
                return redirect('hospitals')->with('failed', 'Gagal mengubah data !');
            }
        }
    }





    public function HospitalDetail($id){
        $subDistrict = SubDistrict::where('deleted_at',null)->get();
        $subDistrictCoverage = HospitalSubdistrictCoverageArea::where('hospital_id',$id)->get();
        $villageCoverage = HospitalVillageCoverageArea::where('hospital_id',$id)->get();
        $hospital = Hospital::find($id);
        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->get();
        return view('hospitals.hospital-detail',compact('hospital','subDistrict','subDistrictCoverage','villageCoverage','notif'));
    }

    public function HospitalCoverArea(Request $request){
        if(!$request->all){
            $existVillage = HospitalVillageCoverageArea::where([
                ['hospital_id',$request->id],
                ['village_id', $request->village_id],
            ])->first();
            if(!$existVillage){
                HospitalVillageCoverageArea::create([
                    'hospital_id' => $request->id,
                    'village_id' => $request->village_id,
                    'subdistrict_id' => $request->subdistrict_id,
                ]);
                return redirect('detail-hospital/'.$request->id)->with('success', 'Success added new Posko ! Coverage Area');
            }
        }else{
            $village = Village::where('sub-district-id',$request->subdistrict_id)->where('deleted_at',null)->get();
            foreach($village as $item){
                $exist = HospitalVillageCoverageArea::where('hospital_id',$request->id)->where('village_id',$item['id'])->first();
                if(!$exist){
                    HospitalVillageCoverageArea::create([
                        'hospital_id' => $request->id,
                        'village_id' => $item->id,
                        'subdistrict_id' => $request->subdistrict_id,
                    ]);
                }
            }
            return redirect('detail-hospital/'.$request->id)->with('success', 'Success added new Posko ! Coverage Area');
        }
        return redirect('detail-hospital/'.$request->id)->with('failed', 'Failed added new Posko ! Coverage Area, The coverage area might already registred !');
    }

    public function DeleteSubdistrictArea($id,$hospital_id){
        $villages = HospitalVillageCoverageArea::where([
            ['subdistrict_id', $id],
            ['hospital_id',$hospital_id],
        ])->get();
        $subdistricts = HospitalSubdistrictCoverageArea::where([
            ['subdistrict_id',$id],
            ['hospital_id',$hospital_id],
        ])->first()->delete();
        foreach($villages as $village){
            $village->delete();
        }
        return redirect(url('hospitals-detail/'.$hospital_id))->with('success','Success deleted coverage area');
    }

    public function DeleteVillageArea($id,$hospital_id){
        $thisVillage = HospitalVillageCoverageArea::where('village_id', $id)->first();
        $existVillage = HospitalVillageCoverageArea::where([
            ['subdistrict_id',$thisVillage['subdistrict_id']],
            ['hospital_id',$hospital_id],
        ])->get();
        if(count($existVillage)==1){
            $subDistrict = HospitalSubdistrictCoverageArea::where('subdistrict_id',$thisVillage['subdistrict_id'])->where('hospital_id',$hospital_id)->first()->delete();
        }
        $thisVillage->delete();
        return redirect(url('hospitals-detail/'.$hospital_id))->with('success','Success deleted Coverage Area');
    }
}
