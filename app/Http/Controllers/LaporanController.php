<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PeopleReport;
use App\People;
use App\Employee;
use App\PeopleDevice;
use App\Hospital;
use App\Diseases;
use App\ShiftHistory;
use App\HospitalVillageCoverageArea;
use App\PoskoVillageCoverageArea;
use Datatables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class LaporanController extends Controller
{
    public function GetDrivingDistance($latlong1, $latlong2){
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$latlong1."&destinations=".$latlong2."&key=AIzaSyDb3hUocjT4p9z45iwS-uiHQF6UpUFpSVU&mode=driving&language=pl-PL&mode=driving&language=pl-PL";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        if($response){
            $response_a = json_decode($response, true);
            $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

            return array('distance' => $dist, 'time' => $time);
        }
    }
    public static function sendGCM($title, $message, $device_id) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array (
                'registration_ids' => array (
                        $device_id
                ),
                'notification' => array(
                    "title" => $title,
                    "body"=> $message,
                    "sound" => "sirine",
                )
        );
        $fields = json_encode ( $fields );

        $headers = array (
                'Authorization: key=' . "AAAA10xTCKE:APA91bG1sk_5RCFuQxoc4yWnvcFihYR-6AGXwk5wNI4XhefNUpNltJ_DMQ6sdriKzYmOnViJMe-UBUDdpUKRzYxR2zg4ng7143dbcQAIJJxermQzoNM_PQvzC_jUEKqu-ZVdKPvXrE4q",
                'Content-Type: application/json'
        );
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        echo $result;
        curl_close ( $ch );
    }
    public function testNotif(){
        $data = PeopleDevice::all();
        foreach($data as $device)
            Self::sendGCM('Title Notif', 'Boody Notif', $device['device_id']);
        return "Selesai";
    }
    public function index(Request $request){
        if(!$request->all()){
            $tipeLaporan = 1;
        }else{
            $tipeLaporan = $request->tipeLaporan;
        }

        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->get();
        return view('laporan.index', compact('notif', 'tipeLaporan', 'arrKode'));
    }
    public function laporanDatatable($nilai){
        $report = PeopleReport::where('report_type', $nilai)
        ->orderBy('created_at', 'desc')
        ->get();

        return Datatables::of($report)
        ->addIndexColumn()
        ->addColumn('reporter',function($report){
            $nama = $report->reporter_name;

            return $nama;
        })
        ->addColumn('nama_penyakit',function($report){
            $penyakit = $report->diseases_name;

            return $penyakit;
        })
        ->addColumn('sub_district',function($report){
            $sub_district = $report->subdistrict_name;

            return $sub_district;
        })
        ->addColumn('village',function($report){
            $village = $report->village_name;

            return $village;
        })
        ->addColumn('posko',function($report){
            $posko = $report->posko_name;

            return $posko;
        })
        ->addColumn('status',function($report){
            if($report->status == 'ACCEPTED'){
                $nama = '<h4><span class="badge badge-success">'.$report->status.'</span></h4>';
            }elseif($report->status == 'DECLINE'){
                $nama = '<h4><span class="badge badge-danger">'.$report->status.'</span></h4>';
            }else{
                $nama = '<h4><span class="badge badge-warning"> PENDING </span></h4>';
            }

            return $nama;
        })
        ->addColumn('action',function($penyakit){
            $url = url('laporan-detail/'.$penyakit['id']);
            $message = "'"."Menuju halaman detail laporan mungkin akan memakan waktu setidaknya 30 detik, karena kami berusaha untuk memberikan Route terbaik !"."'";
            $detail = '<a href="'.$url.'" class="btn btn-sm btn-info"><i class="fas text-white bold fa-eye"></i></a>';

            return $detail;
        })
        ->rawColumns(['reporter', 'nama_penyakit', 'sub_district', 'village', 'posko', 'status', 'action'])
        ->toJson();
    }

    public function laporanDetail($id){
        $data = PeopleReport::find($id);
        $diseases = Diseases::where('deleted_at',null)->get();
        $latlong1 = $data['latitude'].','.$data['longtitude'];
        $posko = [];
        $hospital = [];
        $posko_coverage_default = [];
        $hospital_coverage_default = [];

        $get_posko_coverage_village = PoskoVillageCoverageArea::where('village_id',$data['village_id'])->get();
        foreach($get_posko_coverage_village as $item){
            $posko_coverage_query = Hospital::where('tipe',3)->where('deleted_at',null)->where('id',$item['hospital_id'])->first();
            if($posko_coverage_query){
                $posko_coverage_query['distance'] = Self::GetDrivingDistance($latlong1,$posko_coverage_query['latitude'].','.$posko_coverage_query['longtitude']) ?? 'Distance Unknown';
                $posko_coverage_default[] = $posko_coverage_query;
            }
        }

        $get_hospital_coverage_village = HospitalVillageCoverageArea::where('village_id',$data['village_id'])->get();
        foreach($get_hospital_coverage_village as $item){
            $hospital_coverage_query = Hospital::where('tipe',1)->orWhere('tipe',2)->where('deleted_at',null)->where('id',$item['hospital_id'])->first();
            if($hospital_coverage_query){
                $hospital_coverage_query['distance'] = Self::GetDrivingDistance($latlong1,$hospital_coverage_query['latitude'].','.$hospital_coverage_query['longtitude']) ?? 'Distance Unknown';
                $hospital_coverage_default[] = $hospital_coverage_query;
            }
        }
        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->get();




        if($data['rescuer_id'] == null || isset($_GET['posko'])){
            if($data['village_id'] != null){
                $posko_query = [];
                if(count($posko_coverage_default) > 0){
                    foreach($posko_coverage_default as $item){
                        $all_hospital = Hospital::where('tipe',3)->where('deleted_at',null)->where('id','<>',$item['id'])->get();
                        foreach($all_hospital as $posko_not_defaut)
                            $posko_query[] = $posko_not_defaut;
                    }
                }else{
                    $posko_query = Hospital::where('tipe',3)->where('deleted_at',null)->get();
                }
            }
            if($data['village_id'] == null)
                $posko_query = Hospital::where('tipe',3)->where('deleted_at',null)->get();
            foreach($posko_query as $item){
                $latlong2 = $item['latitude'].','.$item['longtitude'];
                $item['distance'] = Self::GetDrivingDistance($latlong1,$latlong2) ?? 'Distance Unknown';
                $posko[] = $item;
            }
        }

        if($data['rescuer_id'] != null || isset($_GET['hospital'])){
            if($data['village_id'] != null){
                $hospital_query = [];
                if(count($hospital_coverage_default) > 0){
                    foreach($hospital_coverage_default as $item){
                        $all_hospital = Hospital::where('tipe',1)->where('deleted_at',null)->where('id','<>',$item['id'])->get();
                        foreach($all_hospital as $hospital_not_defaut)
                            $hospital_query[] = $hospital_not_defaut;
                    }
                }else{
                    $hospital_query = Hospital::where('tipe',1)->orWhere('tipe',2)->where('deleted_at',null)->get();
                }
            }
            if($data['village_id'] == null)
                $hospital_query = Hospital::where('tipe',1)->orWhere('tipe',2)->where('deleted_at',null)->get();
            foreach($hospital_query as $item){
                $latlong2 = $item['latitude'].','.$item['longtitude'];
                $item['distance'] = Self::GetDrivingDistance($latlong1,$latlong2) ?? 'Distance Unknown';
                $hospital[] = $item;
            }
        }
        return view('laporan.laporan-detail', compact('data','posko','hospital','hospital_coverage_default','posko_coverage_default', 'notif', 'diseases'));
    }

    public function laporanCompleting(Request $request){
        $data = PeopleReport::find($request->id);
        $data->diseases_id = $request->diseases_id;
        $data->description = $request->description;
        if($data->save())
            return redirect(url('laporan-detail/'.$request->id))->with('success', 'Success mengisi keterangan !');
    }

    public function getAnotherPosko($id){
        $data = PeopleReport::find($id);
        $posko_query = Hospital::where('tipe',3)->where('deleted_at',null)->get();
        $latlong1 = $data['latitude'].','.$data['longtitude'];

        $get_posko_coverage_village = PoskoVillageCoverageArea::where('village_id',$data['village_id'])->get();
        foreach($get_posko_coverage_village as $item){
            $posko_coverage_query = Hospital::where('tipe',3)->where('deleted_at',null)->where('id',$item['hospital_id'])->first();
            if($posko_coverage_query){
                $posko_coverage_query['distance'] = Self::GetDrivingDistance($latlong1,$posko_coverage_query['latitude'].','.$posko_coverage_query['longtitude']);
                $posko_coverage_default[] = $posko_coverage_query;
            }
        }
        foreach($posko_coverage_default as $item){
            $all_hospital = Hospital::where('tipe',3)->where('deleted_at',null)->where('id','<>',$item['id'])->get();
            foreach($all_hospital as $posko_not_defaut){
                $latlong2 = $posko_not_defaut['latitude'].','.$posko_not_defaut['longtitude'];
                $posko_not_defaut['distance'] = Self::GetDrivingDistance($latlong1,$latlong2);
                $posko[] = $posko_not_defaut;
            }
        }
        return view('laporan.get-another-posko', compact('posko'));
    }

    public function laporanProcess(Request $request){
        $data = PeopleReport::find($request->id);
        $latlong1 = $data['latitude'].','.$data['longtitude'];
        $tmp_rescuer = $data['rescuer_id'] ?? $request->rescuer_id;
        $rescuer_status = $data['rescuer_id'];
        $shift_on = ShiftHistory::where('date',Carbon::now()->format('Y-m-d'))->where('start','<=',Carbon::now('Asia/Jakarta')->format('H:i'))->where('end','>',Carbon::now('Asia/Jakarta')->format('H:i'))->get();
        $pegawai_posko = [];
        foreach($shift_on as $item)
            $pegawai_posko[] = Employee::where('rest_id',$tmp_rescuer)->where('uid',$item['employee_uid'])->first();
        if($request->rescuer_id){
            $posko = Hospital::find($request->rescuer_id);
            $distance = Self::GetDrivingDistance($latlong1,$posko['latitude'].','.$posko['longtitude'])['distance'] ?? '-';
            $data->rescuer_id = $request->rescuer_id;
            $data->posko_distance = $distance;
        }
        if($request->rest_id){
            $hospital = Hospital::find($request->rest_id);
            $distance = Self::GetDrivingDistance($latlong1,$hospital['latitude'].','.$hospital['longtitude'])['distance'] ?? '-';
            $data->rest_id = $request->rest_id;
            $data->hospital_distance = $distance;
        }
        $data->admin_confirmed = Carbon::now();
        $data->status = 'ACCEPTED';
        if($data->save()){
            $devices = PeopleDevice::where('people_uid',$data['people_uid'])->get();
            foreach($pegawai_posko as  $pegawai)
                foreach(PeopleDevice::where('people_uid',$pegawai['uid'])->get() as $device){
                    if($request->rescuer_id != null && $request->rest_id != null){
                        Self::sendGCM('Laporan baru !', 'Hi, Segera ke lokasi, dan antar ke rumah sakit ini ya!', $device['device_id']);
                    }else{
                        if($rescuer_status == null)
                            Self::sendGCM('Laporan baru !', 'Hi, Segera ke lokasi, ada yang membutuhkan pertolonganmu !', $device['device_id']);
                        else
                            Self::sendGCM('Rumah sakit dipilih !', 'Hi, antarkan passien ke rumah sakit ini ya !', $device['device_id']);
                    }
                }
            foreach($devices as $device)
                Self::sendGCM('Permohonan Dikonformasi !', 'Hore ! Laporanmu sudah diteruskan, mohon menunggu bantuan akan segera datang ke lokasi !', $device['device_id']);
            return response()->json(['error'=>false, 'message' => 'Data sent !'], 200);
        }
    }

    public function laporanDecline(Request $request){
        $data = PeopleReport::find($request->id);
        $data->confirmed_description = $request->confirmed_description;
        $data->status = "DECLINE";
        if($data->save()){
            $devices = PeopleDevice::where('people_uid',$data['people_uid'])->get();
            foreach($devices as $device)
                Self::sendGCM('Laporan Ditolak !', $request->confirmed_description, $device['device_id']);
            return redirect(url('laporan-detail/'.$data->id))->with('success', 'Laporan telah ditolak !');
        }
        return redirect(url('laporan-detail/'.$data->id))->with('failed', 'Gagal menolak laporan, hubungi Developer !');
    }

    public function konfirmasiPengaduan(Request $request){
        $rules = [
            'status_id' => 'required',
            'status_konfirmasi' => 'required',
            'status_keterangan' => 'required'
        ];

        $messages = [
            'status_id.required' => 'Tidak Boleh Kosong !',
            'status_konfirmasi.required' => 'Tidak Boleh Kosong !',
            'status_keterangan.required' => 'Tidak Boleh Kosong !'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return redirect('laporan-detail/'.$request->status_id)->with('failed', 'Gagal, Data tidak boleh kosong !')->withErrors($validator)->withInput();
        }else{
            $id = $request->status_id;
            $status = $request->status_konfirmasi;
            $keterangan = $request->status_keterangan;

            $konfirm = PeopleReport::where('id', $id)
            ->update([
                'status' => $status,
                'confirmed_description' => $keterangan
            ]);

            if($status == 'Diterima'){
                return redirect('laporan-detail/'.$request->status_id)->with('success', 'Pengaduan Diterima !');
            }else{
                return redirect('laporan-detail/'.$request->status_id)->with('success', 'Pengaduan Ditolak !');
            }
        }
    }

    public function dataLaporan(Request $request){
        if($request->pengaduan_awal == null || $request->pengaduan_akhir == null){
            $awal = Carbon::now('Asia/Jakarta')->subDay(3)->format('Y-m-d');
            $akhir = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        }else{
            $awal = Carbon::parse($request->pengaduan_awal)->format('Y-m-d');
            $akhir = Carbon::parse($request->pengaduan_akhir)->format('Y-m-d');
        }

        $data = PeopleReport::orderBy('tmp_created_date', 'desc')
        ->whereRaw(
            "(tmp_created_date >= ? AND tmp_created_date <= ?)",
            [$awal." 00:00:00", $akhir." 23:59:59"]
        )
        ->distinct()
        ->get('tmp_created_date');
        $arrKode = ['0001', '0002', '0003', '0004', '0005', '0006', '0007', '0008', '0009', '0010'];

        return view('laporan.data-laporan', compact('data', 'awal', 'akhir', 'arrKode'));
    }

    public function dataPengaduan(Request $request){
        if($request->pengaduan_awal == null || $request->pengaduan_akhir == null){
            $awal = Carbon::now('Asia/Jakarta')->subDay(3)->format('Y-m-d');
            $akhir = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        }else{
            $awal = Carbon::parse($request->pengaduan_awal)->format('Y-m-d');
            $akhir = Carbon::parse($request->pengaduan_akhir)->format('Y-m-d');
        }


        return view('laporan.data-pengaduan', compact('awal', 'akhir'));
    }

    public function dataTableLaporanPengaduan(){
        $report = PeopleReport::orderBy('created_at', 'desc')
        ->get();

        return Datatables::of($report)
        ->addIndexColumn()
        ->addColumn('waktu',function($report){
            $waktu = $report->format_tanggal;

            return $waktu;
        })
        ->addColumn('pelapor',function($report){
            $pelapor = $report->reporter_name;

            return $pelapor;
        })
        ->addColumn('jenis_pengaduan',function($report){
            $jenis = $report->report_type;
            if($jenis == 1){
                $nama = "Diri Sendiri";
            }elseif($jenis == 2){
                $nama = "Orang Lain";
            }elseif($jenis == 3){
                $nama = "Panic Button";
            }else{
                $nama = "-";
            }

            return $nama;
        })
        ->addColumn('jarak_posko',function($report){
            $jarak = $report->hospital_distance;

            return $jarak . " km";
        })
        ->addColumn('jarak_faskes',function($report){
            $jarak = $report->posko_distance;

            return $jarak  . " km";
        })
        ->rawColumns(['waktu', 'pelapor', 'jenis_pengaduan', 'jarak_posko', 'jarak_faskes'])
        ->toJson();
    }

}
