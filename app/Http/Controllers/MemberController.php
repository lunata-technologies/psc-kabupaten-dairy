<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use Illuminate\Support\Facades\Validator;
use App\People;
use App\PeopleDevice;
use App\PeopleReport;
use App\Hospital;
use Carbon\Carbon;

class MemberController extends Controller
{
    public static function sendGCM($title, $message, $device_id) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array (
                'registration_ids' => array (
                        $device_id
                ),
                'notification' => array(
                    "title" => $title,
                    "body"=> $message,
                )
        );
        $fields = json_encode ( $fields );

        $headers = array (
                'Authorization: key=' . "AAAA10xTCKE:APA91bG1sk_5RCFuQxoc4yWnvcFihYR-6AGXwk5wNI4XhefNUpNltJ_DMQ6sdriKzYmOnViJMe-UBUDdpUKRzYxR2zg4ng7143dbcQAIJJxermQzoNM_PQvzC_jUEKqu-ZVdKPvXrE4q",
                'Content-Type: application/json'
        );
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        echo $result;
        curl_close ( $ch );
    }
    
    public function index(){
        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->get();
        return view('member.index', compact('notif'));
    }

    public function memberDatatable(){
        $member = People::orderBy('name', 'asc')->get();

        return Datatables::of($member)
        ->addIndexColumn()
        ->addColumn('nik',function($member){
            $nik = $member->nik;

            return $nik;
        })
        ->addColumn('nama',function($member){
            $foto = $member->avatar;
            $nama = $member->name;
            $link_detail = "'".url('member-detail/'.$member['uid'])."'";
            $cek = '<div class="media align-items-center">
                        <div class="media-body">
                            <span class="js-lists-values-employee-name" onclick="document.location.href='.$link_detail.'" onMouseOver="this.style.color='."'#0F0'".'" onMouseOut="this.style.color='."'#000'".'" style="cursor:pointer;">'. $nama .'</span>
                        </div>
                    </div>';

            return $cek;
        })
        ->addColumn('email',function($member){
            $email = $member->email;

            return $email;
        })
        ->addColumn('phone',function($member){
            $phone = $member->phone;

            return $phone;
        })
        ->addColumn('alamat',function($member){
            $alamat = $member->address;

            return $alamat;
        })
        ->addColumn('action',function($member){
            $link_approve = "'Approve', 'approve/member/".$member->uid."', 'Member Disetujui, Yakin ?'";
            if($member->approved == NULL){
                $approve = '<span title="Approve" style="cursor:pointer;" class="btn-sm btn-info py-1 px-2" onclick="confirm_me('.$link_approve.')"><i class="fas fa-check"></i></span>';
            }else{
                $approve = '<span title="Approve" style="cursor:pointer;" class="btn-sm btn-danger py-1 px-2" onclick="confirm_me('.$link_approve.')"><i class="fas fa-times"></i></span>';
            }

            $denied = '<span title="Tolak" style="cursor:pointer;" data-toggle="modal" data-target="#tolak-pengaduan" id="modal-tolak-member" uidMember="'.$member->uid.'" class="btn-sm btn-danger py-1 px-2"><i class="fas fa-trash"></i></span>';

            return $approve . $denied;
        })
        ->addColumn('status', function($member){
            if($member->approved != NULL){
                return '<span class="btn btn-success btn-sm py-1 px-2"> ACTIVE </span>';
            }else{
                return '<span class="btn btn-warning btn-sm py-1 px-2"> PENDING </span>';
            }
        })
        ->rawColumns(['nik', 'nama', 'email', 'phone', 'alamat', 'action', 'status'])
        ->toJson();
    }

    public function approveMember($uid){
        $approve = People::where('uid', $uid)
        ->update([
            'approved' => Carbon::now('Asia/Jakarta')
        ]);

        if($approve){
            return redirect('member')->with('success', 'Aktivasi member disetujui !');
        }else{
            return redirect('member')->with('failed', 'Aktivasi member tidak berhasil !');
        }
    }

    public function memberDetail($uid){
        $notif = PeopleReport::orderBy('created_at', 'desc')
            ->get();
        $data = People::find($uid);
        $report = PeopleReport::where('people_uid',$data['uid'])->get();
        return view('member.member-detail', compact('report','data','notif'));
    }

    public function map(){
        $data = Hospital::all();
        return view('member.map', compact('data'));
    }

    public function tolakMember(Request $request){
        $insert = People::where('uid', $request->uid_member)
        ->update([
            'statement_of_rejection' => $request->keterangan,
            'deleted_at' => Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'),
            'nik' => 'REJECTED-'.strval(count(People::all())+1),
            'email' => 'REJECTED-'.strval(count(People::all())+1),
        ]);

        if($insert){
            $devices = PeopleDevice::where('people_uid',$request->uid_member)->get();
            foreach($devices as $device)
                Self::sendGCM('Pendaftaran Ditolak !', $request->keterangan, $device['device_id']);
            return redirect('member')->with('success', 'Member berhasil di Blokir');
        }else{
            return redirect('member')->with('failed', 'Member gagal di blokir');
        }
    }
}
