<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use App\PublicHealth;
use App\Posko;
use App\HospitalSubdistrictCoverageArea;
use App\HospitalVillageCoverageArea;
use App\PoskoSubdistrictCoverageArea;
use App\PoskoVillageCoverageArea;
use App\SubDistrict;
use App\ShiftHistory;
use App\Village;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Datatables;
use Illuminate\Support\Facades\Validator;
use App\PeopleReport;

class PoskoController extends Controller
{
    public function index(Request $request){
        if(!$request->all()){
            $notif = PeopleReport::orderBy('created_at', 'desc')
            ->get();
            return view('posko.index', compact('notif'));
        }else{
            $rules = [
                'nama' => 'required',
                'alamat' => 'required',
                'latitude' => 'required',
                'longitude' => 'required'
            ];

            $messages = [
                'nama.required' => 'Tidak Boleh Kosong !',
                'alamat.required' => 'Tidak Boleh Kosong !',
                'latitude.required' => 'Tidak Boleh Kosong !',
                'longitude.required' => 'Tidak Boleh Kosong !'
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                return redirect('posko')->with('failed', 'Gagal, Data tidak boleh kosong !')->withErrors($validator)->withInput();
            }else{
                $hospital = Hospital::insert([
                    'name' => $request->nama,
                    'address' => $request->alamat,
                    'tipe' => 3,
                    'longtitude' => $request->longitude,
                    'latitude' => $request->latitude,
                    'created_at' => Carbon::now('Asia/Jakarta'),
                    'updated_at' => Carbon::now('Asia/Jakarta')
                ]);
                if($hospital){
                    return redirect(url('posko'))->with('success',' Berhasil Menambahkan Posko Baru !');
                }else{
                    return redirect(url('posko'))->with('failed',' Gagal Menambahkan Posko Baru !');
                }
            }
        }
    }

    public function poskoDatatable(){
        $posko = DB::table('hospitals')
        // ->select('id', DB::raw('ROW_NUMBER() OVER(ORDER BY name ASC) AS Row'), 'name', 'address')
        ->orderBy('name', 'asc')
        ->whereNull('deleted_at')
        ->where('tipe', 3)
        ->get();

        return Datatables::of($posko)
        // ->addColumn('row',function($posko){
        //     $row = $posko->row;

        //     return $row;
        // })
        ->addColumn('nama',function($posko){
            $nama = $posko->name;

            return $nama;
        })
        ->addColumn('alamat',function($posko){
            $alamat = $posko->address;

            return $alamat;
        })
        ->addColumn('action',function($posko){
            $link_delete = "'Hapus', 'delete-posko/".$posko->id."', 'Data akan dihapus, Yakin ?'";
            $link_info = url('detail-posko/'.$posko->id);

            $info = '<span class="btn-sm btn-info py-1 px-2"><a href="'.$link_info.'"><i class="fas text-white fa-eye"></i></a></span>';
            $edit = '<span class="btn-sm btn-success need-auth py-1 px-2" data-toggle="modal" data-target="#update-posko" id="btn-update-detail" id_posko="'.$posko->id.'"><i class="fas fa-edit"></i></span>';
            $hapus = '<span title="Un Approve" class="btn-sm need-auth btn-danger py-1 px-2" onclick="confirm_me('.$link_delete.')"><i class="fas fa-trash"></i></span>';

            return $info . ' ' . $edit . ' ' . $hapus;
        })
        ->rawColumns(['nama', 'alamat', 'action'])
        ->toJson();
    }

    public function deletePosko($id){
        $delete = Hospital::where('id', $id)->delete();
        if($delete){
            return redirect(url('posko'))->with('success', 'Posko berhasil dihapus!');
        }else{
            return redirect(url('posko'))->with('failed', 'Posko gagal dihapus!');
        }
    }

    public function detailPosko(Request $request, $id){
        $today = Carbon::now('Asia/Jakarta')->format('Y-m-d h:m');
        $time = Carbon::now('Asia/Jakarta')->format('h:i');
        $notif = PeopleReport::orderBy('created_at', 'desc')->get();
        $shiftViewButton = [];
        $shift = [];
        foreach(ShiftHistory::where('date','>=', Carbon::parse(Carbon::now())->format('Y-m-d'))->orderBy('date','ASC')->get() as $item){
            $item['date_name'] = Carbon::parse($item['date'])->format('d F Y');
            $shift[] = $item;
        }
        // $shiftViewButton[0] = Carbon::parse($shift[0]['date'])->format('d F Y');

        if(count($shift)>0){
            array_push($shiftViewButton, Carbon::parse($shift[0]['date'])->format('d F Y'));
            foreach($shift as $item){
                $sudah_ada = false;
                for($i=0; $i<count($shiftViewButton); $i++){
                    if($shiftViewButton[$i] == Carbon::parse($item['date'])->format('d F Y')){
                        $sudah_ada = true;
                        break;
                    }
                }
                if($sudah_ada == false){
                    $shiftViewButton[] = Carbon::parse($item['date'])->format('d F Y');
                }
            }
        }


        $data = Hospital::where('id', $id)->first();
        $employee = Employee::where('rest_id', $id)->get();
        $employeeSupir = Employee::where('rest_id', $id)->where('type',3)->get();
        $employeePerawat = Employee::where('rest_id', $id)->where('type',2)->get();
        $employeeDokter = Employee::where('rest_id', $id)->where('type',1)->get();
        $poskoVillageCoverArea = PoskoVillageCoverageArea::where('hospital_id',$data['id'])->get();
        $subdistrict = SubDistrict::where('deleted_at',null)->get();
        if(!$request->all())
            return view('posko.posko-detail',compact('data','shift','shiftViewButton','today','time','employee','employeeSupir', 'employeePerawat', 'employeeDokter','poskoVillageCoverArea', 'subdistrict','notif'));
        $requestData  = $request->all();
        $requestData['rest_id'] = $id;
        if(Employee::create($requestData))
            return redirect(url('detail-posko/'.$id))->with('success','Sukses menambahkan pegawai baru !');
        return redirect(url('detail-posko/'.$id))->with('failed','Gagal menambahkan pegawai baru ! Hubungi Developer');
    }

    public function updateDetailPosko($id){
        $data = Hospital::where('id', $id)->first();
        return $data;
    }

    public function updatePosko(Request $request){
        $rules = [
            'update_id' => 'required',
            'update_nama' => 'required',
            'update_alamat' => 'required',
            'update_latitude' => 'required',
            'update_longitude' => 'required'
        ];

        $messages = [
            'update_id.required' => 'Tidak Boleh Kosong !',
            'update_nama.required' => 'Tidak Boleh Kosong !',
            'update_alamat.required' => 'Tidak Boleh Kosong !',
            'update_latitude.required' => 'Tidak Boleh Kosong !',
            'update_longitude.required' => 'Tidak Boleh Kosong !'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return redirect('posko')->with('failed', 'Gagal, Data tidak boleh kosong !')->withErrors($validator)->withInput();
        }else{
            $update = Hospital::where('id', $request->update_id)
            ->update([
                'name' => $request->update_nama,
                'address' => $request->update_alamat,
                'longtitude' => $request->update_longitude,
                'latitude' => $request->update_latitude
            ]);

            if($update){
                return redirect('posko')->with('success', 'Berhasil mengubah data !');
            }else{
                return redirect('posko')->with('failed', 'Gagal mengubah data !');
            }
        }
    }

    public function ajaxEditPegawaiPosko($uid){
        $data = Employee::find($uid);
        return view('posko.ajax-edit-pegawai-posko', compact('data'));
    }

    public function editPegawaiPosko(Request $request){
        $data = Employee::find($request->uid);
        $data->name = $request->name;
        $data->username = $request->username;
        $data->password = $request->password;
        $data->phone = $request->phone;
        $data->type = $request->type;
        if($data->save())
            return redirect(url('detail-posko/'.$data['rest_id']))->with('success','Sukses mengubah data pegawai !');
        return redirect(url('detail-posko/'.$data['rest_id']))->with('failed','Gagal mengubah data pegawai ! Hubungi Developer');
    }

    public function PoskoShift(Request $request){
        if(!$request->employee)
            return redirect(url('detail-posko/'.$request->rest_id))->with('failed', 'Gagal menambahkan Shift Baru, pilih setidaknya 1 pegawai ');
        $exist = false;
        $exist_name = '';
        foreach($request->employee as $item){
            $data = ShiftHistory::where('date', Carbon::parse($request->date))->where('employee_uid',$item)->first();
            if($data){
                $exist = true;
                $exist_name .= ', ' .Employee::find($data['employee_uid'])['name'];
            }
        }
        if($exist == true)
            return redirect(url('detail-posko/'.$request->rest_id))->with('failed', 'Gagal menambahkan Shift Baru, data telah ada untuk '.$exist_name);
        foreach($request->employee as $item){
            ShiftHistory::create([
                'rest_id' => $request->rest_id,
                'date' => $request->date,
                'start' => $request->start,
                'end' => $request->end,
                'employee_uid' => $item,
            ]);
        }
        return redirect(url('detail-posko/'.$request->rest_id))->with('success', 'Berhasil menambahkan Shift Baru');
    }

    public function deleteShift($id,$status){
        $data = ShiftHistory::find($id);
        $tmp = $data;
        $data->delete();
        if($status == 'posko')
            return redirect(url('detail-posko/'.$tmp['rest_id']))->with('success','Success menghapus shift !');
    }


    // Old

    public function PoskoDetail($id){
        $hospital_belonged_to = [];
        $subDistrict = SubDistrict::where('deleted_at',null)->get();
        $subDistrictCoverage = PoskoSubdistrictCoverageArea::where('posko_id',$id)->get();
        $villageCoverage = PoskoVillageCoverageArea::where('posko_id',$id)->get();

        // Get Hospital Belonged to this POSKO
        foreach($villageCoverage as $village){
            $hospital_village_coverage = HospitalVillageCoverageArea::where('village_id',$village['village_id'])->get();
            foreach($hospital_village_coverage as $hospital){
                $hospital_belonged_to[] = Hospital::find($hospital['hospital_id'])->first();
            }
        }
        $posko = Posko::find($id);
        return view('posko.posko-detail',compact('posko','subDistrict','subDistrictCoverage','villageCoverage','hospital_belonged_to'));
    }
    public function PoskoCoverArea(Request $request){
        if(!$request->all){
            $existVillage = PoskoVillageCoverageArea::where([
                ['hospital_id',$request->id],
                ['village_id', $request->village_id],
            ])->first();
            if(!$existVillage){
                PoskoVillageCoverageArea::create([
                    'hospital_id' => $request->id,
                    'village_id' => $request->village_id,
                    'subdistrict_id' => $request->subdistrict_id,
                ]);
                return redirect('detail-posko/'.$request->id)->with('success', 'Success added new Posko ! Coverage Area');
            }
        }else{
            $village = Village::where('sub-district-id',$request->subdistrict_id)->where('deleted_at',null)->get();
            foreach($village as $item){
                $exist = PoskoVillageCoverageArea::where('hospital_id',$request->id)->where('village_id',$item['id'])->first();
                if(!$exist){
                    PoskoVillageCoverageArea::create([
                        'hospital_id' => $request->id,
                        'village_id' => $item->id,
                        'subdistrict_id' => $request->subdistrict_id,
                    ]);
                }
            }
            return redirect('detail-posko/'.$request->id)->with('success', 'Success added new Posko ! Coverage Area');
        }
        return redirect('detail-posko/'.$request->id)->with('failed', 'Failed added new Posko ! Coverage Area, The coverage area might already registred !');
    }
    public function DeleteSubdistrictArea($id,$posko_id){
        $villages = PoskoVillageCoverageArea::where([
            ['subdistrict_id', $id],
            ['posko_id',$posko_id],
        ])->get();
        $subdistricts = PoskoSubdistrictCoverageArea::where([
            ['subdistrict_id',$id],
            ['posko_id',$posko_id],
        ])->first()->delete();
        foreach($villages as $village){
            $village->delete();
        }
        return redirect(url('posko-detail/'.$posko_id))->with('success','Success deleted coverage area');
    }
    public function DeleteVillageArea($id,$posko_id){
        $thisVillage = PoskoVillageCoverageArea::where('village_id', $id)->first();
        $existVillage = PoskoVillageCoverageArea::where([
            ['subdistrict_id',$thisVillage['subdistrict_id']],
            ['posko_id',$posko_id],
        ])->get();
        if(count($existVillage)==1){
            $subDistrict = PoskoSubdistrictCoverageArea::where('subdistrict_id',$thisVillage['subdistrict_id'])->where('posko_id',$posko_id)->first()->delete();
        }
        $thisVillage->delete();
        return redirect(url('posko-detail/'.$posko_id))->with('success','Success deleted Coverage Area');
    }
}
