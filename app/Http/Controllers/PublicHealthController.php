<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use App\PublicHealth;
use App\PublichealthSubdistrictCoverageArea;
use App\PublichealthVillageCoverageArea;
use App\PuskesmasVillageCoverageArea;
use App\SubDistrict;
use App\Village;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Datatables;
use App\PeopleReport;


class PublicHealthController extends Controller
{
    public function index(Request $request){
        if(!$request->all()){
            $notif = PeopleReport::orderBy('created_at', 'desc')
            ->get();
            return view('publichealth.index', compact('notif'));
        }else{
            $rules = [
                'nama' => 'required',
                'alamat' => 'required',
                'latitude' => 'required',
                'longitude' => 'required'
            ];

            $messages = [
                'nama.required' => 'Tidak Boleh Kosong !',
                'alamat.required' => 'Tidak Boleh Kosong !',
                'latitude.required' => 'Tidak Boleh Kosong !',
                'longitude.required' => 'Tidak Boleh Kosong !'
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                return redirect('publichealth')->with('failed', 'Gagal, Data tidak boleh kosong !')->withErrors($validator)->withInput();
            }else{
                $hospital = Hospital::insert([
                    'name' => $request->nama,
                    'address' => $request->alamat,
                    'tipe' => 2,
                    'longtitude' => $request->longitude,
                    'latitude' => $request->latitude,
                    'created_at' => Carbon::now('Asia/Jakarta'),
                    'updated_at' => Carbon::now('Asia/Jakarta')
                ]);
                if($hospital){
                    return redirect(url('publichealth'))->with('success',' Berhasil menambahkan Puskesmas baru !');
                }else{
                    return redirect(url('publichealth'))->with('failed',' Gagal Menambahkan Puskesmas baru !');
                }
            }
        }
    }

    public function puskesmasDatatable(){
        $puskesmas = DB::table('hospitals')
        // ->select('id', DB::raw('ROW_NUMBER() OVER(ORDER BY name ASC) AS Row'), 'name', 'address')
        ->orderBy('name', 'asc')
        ->whereNull('deleted_at')
        ->where('tipe', 2)
        ->get();

        return Datatables::of($puskesmas)
        // ->addColumn('row',function($puskesmas){
        //     $row = $puskesmas->row;

        //     return $row;
        // })
        ->addColumn('nama',function($puskesmas){
            $nama = $puskesmas->name;

            return $nama;
        })
        ->addColumn('alamat',function($puskesmas){
            $alamat = $puskesmas->address;

            return $alamat;
        })
        ->addColumn('action',function($puskesmas){
            $link_delete = "'Hapus', 'delete-puskesmas/".$puskesmas->id."', 'Data akan dihapus, Yakin ?'";
            $link_info = url('detail-puskesmas/'.$puskesmas->id);

            $info = '<span class="btn-sm btn-info py-1 px-2" data-toggle="modal" data-target="#update-posko"><a href="'.$link_info.'"><i class="fas text-white fa-eye"></i></a></span>';
            $edit = '<span class="btn-sm need-auth btn-success py-1 px-2" data-toggle="modal" data-target="#update-puskesmas" id="btn-update-detail" id_puskesmas="'.$puskesmas->id.'"><i class="fas fa-edit"></i></span>';
            $hapus = '<span title="Un Approve" class="btn-sm need-auth btn-danger py-1 px-2" onclick="confirm_me('.$link_delete.')"><i class="fas fa-trash"></i></span>';

            return $info . ' ' . $edit . ' ' . $hapus;
        })
        ->rawColumns(['nama', 'alamat', 'action'])
        ->toJson();
    }

    public function deletePuskesmas($id){
        $delete = Hospital::where('id', $id)->delete();
        if($delete){
            return redirect(url('publichealth'))->with('success', 'Puskesmas berhasil dihapus!');
        }else{
            return redirect(url('publichealth'))->with('failed', 'Puskesmas gagal dihapus!');
        }
    }

    public function updateDetailPuskesmas($id){
        $data = Hospital::where('id', $id)->first();
        return $data;
    }

    public function detailPuskesmas(Request $request, $id){
        $data = Hospital::where('id', $id)->first();
        $employeeSupir = Employee::where('rest_id', $id)->where('type',3)->get();
        $employeePerawat = Employee::where('rest_id', $id)->where('type',2)->get();
        $employeeDokter = Employee::where('rest_id', $id)->where('type',1)->get();
        $poskoVillageCoverArea = PuskesmasVillageCoverageArea::where('hospital_id',$data['id'])->get();
        $subdistrict = SubDistrict::where('deleted_at',null)->get();
        if(!$request->all())
            return view('publichealth.publichealth-detail',compact('data','employeeSupir', 'employeePerawat', 'employeeDokter','poskoVillageCoverArea', 'subdistrict'));
        $requestData  = $request->all();
        $requestData['rest_id'] = $id;
        if(Employee::create($requestData))
            return redirect(url('detail-puskesmas/'.$id))->with('success','Sukses menambahkan pegawai baru !');
        return redirect(url('detail-puskesmas/'.$id))->with('failed','Gagal menambahkan pegawai baru ! Hubungi Developer');
    }

    public function ajaxEditPegawaiPublicHealth($uid){
        $data = Employee::find($uid);
        return view('publichealth.ajax-edit-pegawai-publichealth', compact('data'));
    }

    public function updatePuskesmas(Request $request){
        $rules = [
            'update_id' => 'required',
            'update_nama' => 'required',
            'update_alamat' => 'required',
            'update_latitude' => 'required',
            'update_longitude' => 'required'
        ];

        $messages = [
            'update_id.required' => 'Tidak Boleh Kosong !',
            'update_nama.required' => 'Tidak Boleh Kosong !',
            'update_alamat.required' => 'Tidak Boleh Kosong !',
            'update_latitude.required' => 'Tidak Boleh Kosong !',
            'update_longitude.required' => 'Tidak Boleh Kosong !'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return redirect('publichealth')->with('failed', 'Gagal, data tidak boleh kosong !')->withErrors($validator)->withInput();
        }else{
            $update = Hospital::where('id', $request->update_id)
            ->update([
                'name' => $request->update_nama,
                'address' => $request->update_alamat,
                'longtitude' => $request->update_longitude,
                'latitude' => $request->update_latitude
            ]);

            if($update){
                return redirect('publichealth')->with('success', 'Berhasil mengubah data !');
            }else{
                return redirect('publichealth')->with('failed', 'Gagal mengubah data !');
            }
        }
    }




    public function PublicHealthDetail($id){
        return $data = Hospital::where('id', $id)->first();
        $employeeSupir = Employee::where('rest_id', $id)->where('type',3)->get();
        $employeePerawat = Employee::where('rest_id', $id)->where('type',2)->get();
        $employeeDokter = Employee::where('rest_id', $id)->where('type',1)->get();
        $poskoVillageCoverArea = PoskoVillageCoverageArea::where('hospital_id',$data['id'])->get();
        $subdistrict = SubDistrict::where('deleted_at',null)->get();
        if(!$request->all())
            return view('publichealth.publichealth-detail',compact('data','employeeSupir', 'employeePerawat', 'employeeDokter','poskoVillageCoverArea', 'subdistrict'));
        $requestData  = $request->all();
        $requestData['rest_id'] = $id;
        if(Employee::create($requestData))
            return redirect(url('detail-puskesmas/'.$id))->with('success','Sukses menambahkan pegawai baru !');
        return redirect(url('detail-puskesmas/'.$id))->with('failed','Gagal menambahkan pegawai baru ! Hubungi Developer');
    }
    public function PuskesmasCoverArea(Request $request){
        if(!$request->all){
            $existVillage = PuskesmasVillageCoverageArea::where([
                ['hospital_id',$request->id],
                ['village_id', $request->village_id],
            ])->first();
            if(!$existVillage){
                PuskesmasVillageCoverageArea::create([
                    'hospital_id' => $request->id,
                    'village_id' => $request->village_id,
                    'subdistrict_id' => $request->subdistrict_id,
                ]);
                return redirect('detail-puskesmas/'.$request->id)->with('success', 'Success added new Posko ! Coverage Area');
            }
        }else{
            $village = Village::where('sub-district-id',$request->subdistrict_id)->where('deleted_at',null)->get();
            foreach($village as $item){
                $exist = PuskesmasVillageCoverageArea::where('hospital_id',$request->id)->where('village_id',$item['id'])->first();
                if(!$exist){
                    PuskesmasVillageCoverageArea::create([
                        'hospital_id' => $request->id,
                        'village_id' => $item->id,
                        'subdistrict_id' => $request->subdistrict_id,
                    ]);
                }
            }
            return redirect('detail-puskesmas/'.$request->id)->with('success', 'Success added new Posko ! Coverage Area');
        }
        return redirect('detail-puskesmas/'.$request->id)->with('failed', 'Failed added new Posko ! Coverage Area, The coverage area might already registred !');
    }
    public function DeleteSubdistrictArea($id,$publichealth_id){
        $villages = PublichealthVillageCoverageArea::where([
            ['subdistrict_id', $id],
            ['publichealth_id',$publichealth_id],
        ])->get();
        $subdistricts = PublichealthSubdistrictCoverageArea::where([
            ['subdistrict_id',$id],
            ['publichealth_id',$publichealth_id],
        ])->first()->delete();
        foreach($villages as $village){
            $village->delete();
        }
        return redirect(url('publichealth-detail/'.$publichealth_id))->with('success','Success deleted coverage area');
    }
    public function DeleteVillageArea($id,$publichealth_id){
        $thisVillage = PublichealthVillageCoverageArea::where('village_id', $id)->first();
        $existVillage = PublichealthVillageCoverageArea::where([
            ['subdistrict_id',$thisVillage['subdistrict_id']],
            ['publichealth_id',$publichealth_id],
        ])->get();
        if(count($existVillage)==1){
            $subDistrict = PublichealthSubdistrictCoverageArea::where('subdistrict_id',$thisVillage['subdistrict_id'])->where('publichealth_id',$publichealth_id)->first()->delete();
        }
        $thisVillage->delete();
        return redirect(url('publichealth-detail/'.$publichealth_id))->with('success','Success deleted Coverage Area');
    }
}
