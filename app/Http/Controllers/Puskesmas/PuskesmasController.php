<?php

namespace App\Http\Controllers\Puskesmas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Session;

class PuskesmasController extends Controller
{
    public function login(Request $request){
        if(!$request->all()){
            return view('puskesmas.login');
        }else{
            $email = $request->email;
            $password = $request->password;

            $pegawai = User::where('email', $email)->first();

            if($pegawai){
                if(Hash::check($password, $pegawai->password)){
                    Session::put('nama_pegawai',$pegawai->nama);
                    Session::put('email',$pegawai->email);
                    Session::put('uid',$pegawai->uid);
                    Session::put('login',TRUE);
                    return redirect('puskesmas');
                }else{
                    return redirect('puskesmas/login')->with('failed', 'Password Salah!');
                }
            }else{
                return redirect('puskesmas/login')->with('failed', 'Email tidak terdaftar!');
            }
        }
    }

    public function logout(){
        Session::flush();
        return redirect('puskesmas/login')->with('success', 'Logout berhasil');
    }
}
