<?php

namespace App\Http\Controllers\RumahSakit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RumahSakit;
use App\SubDistrict;
use App\PeopleReport;
use Carbon\Carbon;
use Session;

class RSDashboardController extends Controller
{
    public function dataKecamatan(){
        $kecamatan = SubDistrict::orderBy('name', 'asc')->get();
        $tmpKecamatan = [];
        $j = 0;
        foreach($kecamatan as $key => $value){
            $tmpKecamatan[$j] = $value->name;
            $j++;
        }
        return $tmpKecamatan;
    }

    public function grafikLaporanPerKecamatan($awal, $akhir){
        $awal = Carbon::create($awal)->format('Y-m-d');
        $akhir = Carbon::create($akhir)->format('Y-m-d');

        $statusName[0] = ['Diri Sendiri'];
        $statusName[1] = ['Orang Lain'];
        $statusName[2] = ['Panic Button'];

        $kecamatan = SubDistrict::orderBy('name', 'asc')->get();
        $tmpKecamatan = [];
        $aduanPerkecamatan = [];
        $tmpAduanPerkecamatan = [];
        $j = 0;

        for($i = 0; $i < 3; $i++){
            $aduanPerkecamatan[$i]['name'] = $statusName[$i];
        }

        foreach($kecamatan as $key => $value){
            $tmpKecamatan[$j] = $value->name;
            $j++;
        }

        $j = 1;
        for($i = 0; $i < 3; $i++){
            $nilai = 0;
            $orangLain = 0;
            $panicButton = 0;
            foreach($kecamatan as $key => $value){
                $nilai = PeopleReport::where([
                    'subdistrict_id' => $value->id,
                    'report_type' => $j,
                    'rest_id' => Session::get('rest_id')
                ])
                ->whereRaw(
                    "(created_at >= ? AND created_at <= ?)",
                    [$awal." 00:00:00", $akhir." 23:59:59"]
                )->count();

                array_push($tmpAduanPerkecamatan, $nilai);

            }
            $j++;
            $aduanPerkecamatan[$i]['data'] = $tmpAduanPerkecamatan;
            $tmpAduanPerkecamatan = [];
        }

        return $aduanPerkecamatan;
    }

    public function index(Request $request){
        if($request->awal == null || $request->akhir == null){
            $reqAwal = Carbon::now('Asia/Jakarta')->firstOfMonth()->toDateString();
            $reqAkhir = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        }else{
            $reqAwal = $request->awal;
            $reqAkhir = $request->akhir;
        }

        $awal = Carbon::create($reqAwal)->format('Y-m-d');
        $akhir = Carbon::create($reqAkhir)->format('Y-m-d');
        $range = Carbon::parse($awal)->diffInDays($akhir)+1;

        $awal = Carbon::create($awal)->format('Y-m-d');
        $akhir = Carbon::create($akhir)->format('Y-m-d');

        $aduanPerkecamatan = $this->grafikLaporanPerKecamatan($awal, $akhir);
        $tmpKecamatan = $this->dataKecamatan();

        return view('rumah-sakit.dashboard.index', compact('aduanPerkecamatan', 'tmpKecamatan', 'reqAwal', 'reqAkhir'));
    }

    public function notifRumahSakit(){
        $startNotif = Carbon::now('Asia/Jakarta')->subDay(28)->format('Y-m-d');
        $endNotif = Carbon::now('Asia/Jakarta')->format('Y-m-d');

        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->where('rest_id', Session::get('rest_id'))
        ->whereRaw(
            "(created_at >= ? AND created_at <= ?)",
            [$startNotif." 00:00:00", $endNotif." 23:59:59"]
        )->get();

        return $notif;
    }
}
