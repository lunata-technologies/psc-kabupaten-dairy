<?php

namespace App\Http\Controllers\RumahSakit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PeopleReport;
use Datatables;
use App\Diseases;
use App\PoskoVillageCoverageArea;
use App\HospitalVillageCoverageArea;
use App\Hospital;
use Session;
use Carbon\Carbon;

class RSPengaduanController extends Controller
{
    public function index(){
        return view('rumah-sakit.pengaduan.index');
    }

    public function pengaduanDatatable(){
        $rest_id = Session::get('rest_id');
        $report = PeopleReport::orderBy('created_at', 'desc')->where('rest_id', $rest_id)->get();

        return Datatables::of($report)
        ->addColumn('reporter',function($report){
            $nama = $report->reporter_name;

            return $nama;
        })
        ->addColumn('nama_penyakit',function($report){
            $penyakit = $report->diseases_name;

            return $penyakit;
        })
        ->addColumn('sub_district',function($report){
            $sub_district = $report->subdistrict_name;

            return $sub_district;
        })
        ->addColumn('village',function($report){
            $village = $report->village_name;

            return $village;
        })
        ->addColumn('posko',function($report){
            $posko = $report->posko_name;

            return $posko;
        })
        ->addColumn('status',function($report){
            if($report->status == 'ACCEPTED'){
                $nama = '<h4><span class="badge badge-success">'.$report->status.'</span></h4>';
            }elseif($report->status == 'DECLINE'){
                $nama = '<h4><span class="badge badge-danger">'.$report->status.'</span></h4>';
            }else{
                $nama = '<h4><span class="badge badge-warning"> PENDING </span></h4>';
            }

            return $nama;
        })
        ->addColumn('aksi',function($report){
            $url = 'detail-laporan/' . $report->id;
            $terima = '<a href="'.$url.'" class="btn btn-sm btn-info"><i class="fas fa-info"></i></a>';

            return $terima;
        })
        ->rawColumns(['reporter', 'nama_penyakit', 'sub_district', 'village', 'posko', 'status', 'aksi'])
        ->toJson();
    }

    public function detailPengaduan($id){
        $data = PeopleReport::find($id);

        return view('rumah-sakit.pengaduan.detail-pengaduan', compact('data'));
    }

    public function terimaPasien($id){
        $data = PeopleReport::where('id', $id)->update([
            'rest_accept_date' => Carbon::now('Asia/Jakarta')
        ]);

        if($data){
            return redirect('rumah-sakit/detail-laporan/'.$id)->with('success', 'Pasien Diterima');
        }else{
            return redirect('rumah-sakit/detail-laporan/'.$id)->with('failed', 'Proses gagal !');
        }
    }
}
