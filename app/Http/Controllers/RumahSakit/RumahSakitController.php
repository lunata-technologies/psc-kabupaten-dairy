<?php

namespace App\Http\Controllers\RumahSakit;
use App\Employee;
use Hash;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RumahSakitController extends Controller
{
    public function login(Request $request){
        if(!$request->all()){
            return view('rumah-sakit.login');
        }else{
            $username = $request->username;
            $password = $request->password;

            $pegawai = Employee::where('username', $username)->first();

            if($pegawai){
                // if(Hash::check($password, $pegawai->password)){
                if($password == $pegawai->password){
                    Session::put('nama_pegawai',$pegawai->name);
                    Session::put('username',$pegawai->username);
                    Session::put('rest_id',$pegawai->rest_id);
                    Session::put('uid',$pegawai->uid);
                    Session::put('faskes',$pegawai->faskes);
                    Session::put('faskes_detail',$pegawai->faskes_detail);
                    Session::put('login',TRUE);
                    return redirect('rumah-sakit');
                }else{
                    return redirect('rumah-sakit/login')->with('failed', 'Password Salah!');
                }
            }else{
                return redirect('rumah-sakit/login')->with('failed', 'Username tidak terdaftar!');
            }
        }
    }

    public function logout(){
        Session::flush();
        return redirect('rumah-sakit/login')->with('success', 'Logout berhasil');
    }
}
