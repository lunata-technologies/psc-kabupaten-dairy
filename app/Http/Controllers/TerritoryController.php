<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\SubDistrict;
use App\Village;
use App\HospitalSubdistrictCoverageArea;
use App\HospitalVillageCoverageArea;
use App\PoskoVillageCoverageArea;
use App\PublichealthVillageCoverageArea;
use App\PeopleReport;

class TerritoryController extends Controller
{
    // Sub District ======================================================================================================================================================
    public function SubDistrict(Request $request){
        $subDistrict = SubDistrict::where('deleted_at',null)->get();
        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->get();
        if(!$request->all())
            return view('Territory.SubDistrict', compact('subDistrict', 'notif'));
        if(SubDistrict::create($request->all()))
            return redirect(url('sub-district'))->with('success','Success added new Sub District');
        return redirect(url('sub-district'))->with('failed','Failed added new Sub District');
    }
    public function SubDistrictDetail($id){
        $subDistrict = SubDistrict::find($id);
        $village = Village::where('sub-district-id', $subDistrict['id'])->where('deleted_at',null)->get();
        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->get();
        return view('Territory.SubdistrictDetail', compact('subDistrict','village', 'notif'));
    }
    public function SubDistrictEdit(Request $request){
        $data = SubDistrict::find($request->id);
        $data->name = $request->name;
        $data->longtitude = $request->longtitude;
        $data->latitude = $request->latitude;
        if($data->save())
            return redirect(url('sub-district'))->with('success','Success edited Sub District');
        return redirect(url('sub-district'))->with('failed','Failed edited Sub District');
    }
    public function SubDistrictDelete($id){
        $data = SubDistrict::find($id)->destroy();
        if($data)
            return redirect(url('sub-district'))->with('success','Success deleted Sub District');
        return redirect(url('sub-district'))->with('failed','Failed deleted Sub District');
    }


    // Village ======================================================================================================================================================
    public function Village(Request $request){
        $subDistrict = SubDistrict::where('deleted_at',null)->get();
        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->get();
        $village = Village::where('deleted_at',null)->get();
        if(!$request->all())
            return view('Territory.Village', compact('subDistrict','village', 'notif'));
        if(Village::create([
            'name' => $request->name,
            'sub-district-id' => $request['sub-district-id'],
        ]))
            if(!$request['from_page_sub_district_detail']){
                return redirect(url('village'))->with('success','Success added new Village Area');
            }else{
                return redirect(url('sub-district-detail/'.$request['sub-district-id']))->with('success','Success added new Village Area');
            }
    }
    public function VillageSubdistrict(Request $request){
        $village = Village::where('sub-district-id',$request->subdistrict_id)->where('deleted_at',null)->get();
        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->get();
        return view('Territory.ajax-village-subdistrict', compact('village', 'notif'));
    }
    public function VillageEdit(Request $request, $id){
        $village = Village::find($id);
        $notif = PeopleReport::orderBy('created_at', 'desc')
        ->get();
        if(!$request->all())
            return view('Territory.ajax-village-edit', compact('village', 'notif'));
        $village->name = $request->name;
        if($village->save())
            return redirect(url('sub-district-detail/'.$village['sub-district-id']))->with('success','Success edited Village area detail !');
        return redirect(url('sub-district-detail/'.$village['sub-district-id']))->with('failed','Failed edit Village area detail !');
    }
    public function VillageDelete($id){
        $village = Village::find($id);
        $hospitalCoverArea = HospitalVillageCoverageArea::where('village_id',$id)->get();
        $poskoCoverArea = PoskoVillageCoverageArea::where('village_id',$id)->get();
        $publicHealthCoverArea = PublichealthVillageCoverageArea::where('village_id',$id)->get();
        foreach($hospitalCoverArea as $item)
            $item->delete();
        foreach($poskoCoverArea as $item)
            $item->delete();
        foreach($publicHealthCoverArea as $item)
            $item->delete();
        $village->deleted_at = Carbon::now();
        $village->save();
        return redirect(url('sub-district-detail/'.$village['sub-district-id']))->with('success','Success deleted !');
    }
}
