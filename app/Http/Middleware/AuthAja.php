<?php

namespace App\Http\Middleware;
use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

use Closure;

class AuthAja
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $email = Session::get('email');
        $user = User::where('email', $email)->first();
        if (!$user) {
            return redirect('login')->with('failed', 'Login terlebih dahulu! :(');
        }
        return $next($request);
    }
}
