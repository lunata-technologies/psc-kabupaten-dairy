<?php

namespace App\Http\Middleware;
use App\Employee;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

use Closure;

class AuthRumahSakit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $username = Session::get('username');
        $user = Employee::where('username', $username)->first();
        if (!$user) {
            return redirect('rumah-sakit/login')->with('failed', 'Login terlebih dahulu! :(');
        }

        return $next($request);
    }
}
