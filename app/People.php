<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class People extends Authenticatable
{
    use HasApiTokens;
    protected $table = "people";
    protected $primaryKey = "uid";
    public $incrementing = false;
    protected $hidden = ['password'];
    protected $guarded = [];
    use SoftDeletes;

    protected $appends = ['subdistrict_area','village_area'];

    // Sub District Area
    public function subdistrict_appends(){
        return $this->belongsTo(SubDistrict::class, 'sub-district-id');
    }
    public function getSubdistrictAreaAttribute(){
        return $this->subdistrict_appends->name ?? '-';
    }

    // Village Area
    public function village_appends(){
        return $this->belongsTo(Village::class, 'village-id');
    }
    public function getVillageAreaAttribute(){
        return $this->village_appends->name ?? '-';
    }
}
