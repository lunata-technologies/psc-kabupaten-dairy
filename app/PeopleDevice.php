<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeopleDevice extends Model
{
    protected $table = "people_device";
    protected $guarded = [];
}
