<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class PeopleReport extends Model
{
    protected $table = "people_report";
    protected $guarded = [];
    protected $appends = [
        'reporter_name',
        'reporter_foto',
        'diseases_name',
        'subdistrict_name',
        'village_name',
        'posko_name',
        'rest_name',
        'estimasi',
        'link',
        'link_laporan',
        'link_laporan_rumah_sakit',
        'total_diri_sendiri',
        'total_orang_lain',
        'total_panic_button',
        'format_tanggal',
        'count_diri_sendiri_1',
        'count_diri_sendiri_2',
        'count_diri_sendiri_3',
        'count_diri_sendiri_4',
        'count_diri_sendiri_5',
        'count_diri_sendiri_6',
        'count_diri_sendiri_7',
        'count_diri_sendiri_8',
        'count_diri_sendiri_9',
        'count_diri_sendiri_10',
        'count_orang_lain_1',
        'count_orang_lain_2',
        'count_orang_lain_3',
        'count_orang_lain_4',
        'count_orang_lain_5',
        'count_orang_lain_6',
        'count_orang_lain_7',
        'count_orang_lain_8',
        'count_orang_lain_9',
        'count_orang_lain_10',
        'count_panic_button_1',
        'count_panic_button_2',
        'count_panic_button_3',
        'count_panic_button_4',
        'count_panic_button_5',
        'count_panic_button_6',
        'count_panic_button_7',
        'count_panic_button_8',
        'count_panic_button_9',
        'count_panic_button_10'
    ];
    use SoftDeletes;

    // People ORM
    public function people_appends(){
        return $this->belongsTo(People::class, 'people_uid');
    }
    public function getReporterNameAttribute(){
        return $this->people_appends->name ?? '-';
    }
    public function getReporterFotoAttribute(){
        return $this->people_appends->selfie ?? '-';
    }

    // Diseases ORM
    public function diseases_appends(){
        return $this->belongsTo(Diseases::class, 'diseases_id');
    }
    public function getDiseasesNameAttribute(){
        return $this->diseases_appends->nama ?? '-';
    }

    // Subdistrict
    public function subdistrict_appends(){
        return $this->belongsTo(SubDistrict::class, 'subdistrict_id');
    }

    public function getSubdistrictNameAttribute(){
        return $this->subdistrict_appends->name ?? '-';
    }

    // Village
    public function village_appends(){
        return $this->belongsTo(Village::class, 'village_id');
    }

    public function getVillageNameAttribute(){
        return $this->village_appends->name ?? '-';
    }

    // Posko
    public function posko_appends(){
        return $this->belongsTo(Hospital::class, 'rescuer_id');
    }

    public function getPoskoNameAttribute(){
        return $this->posko_appends->name ?? '-';
    }

    // Rest
    public function rest_appends(){
        return $this->belongsTo(Hospital::class, 'rest_id');
    }

    public function getRestNameAttribute(){
        return $this->rest_appends->name ?? '-';
    }

    public function getEstimasiAttribute(){
        date_default_timezone_set('Asia/Jakarta');
        $date1 = date_create($this->created_at);
        $date2 = date_create("now");

        $diff = date_diff($date1,$date2);

        $detik = $diff->format('%s');
        $menit = $diff->format('%i');
        $jam = $diff->format('%h');
        $hari = $diff->format('%d');
        $status = '';

        if($hari >= 1){
            $status = $hari . ' hari yang lalu';
        }
        if($jam >= 1 && $hari < 1){
            $status = $jam . ' jam yang lalu';
        }
        if($menit >= 1 && $jam < 1){
            $status = $menit . ' menit yang lalu';
        }
        if($detik >= 1 && $menit < 1){
            $status = $detik . ' detik yang lalu';
        }

        return $status;
    }

    public function getLinkAttribute(){
        return url('/');
    }

    public function getLinkLaporanAttribute(){
        return url('laporan-detail/');
    }

    public function getLinkLaporanRumahSakitAttribute(){
        return url('rumah-sakit/detail-laporan/');
    }

    public function getTotalDiriSendiriAttribute(){
        $jumlah = Self::where('report_type', 1)->where('tmp_created_date', $this->tmp_created_date)->count();
        return $jumlah ?? 0;
    }

    public function getTotalOrangLainAttribute(){
        $jumlah = Self::where('report_type', 2)->where('tmp_created_date', $this->tmp_created_date)->count();
        return $jumlah ?? 0;
    }

    public function getTotalPanicButtonAttribute(){
        $jumlah = Self::where('report_type', 3)->where('tmp_created_date', $this->tmp_created_date)->count();
        return $jumlah ?? 0;
    }

    public function getFormatTanggalAttribute(){
        $tanggal = Carbon::parse($this->tmp_created_date)->format('d-m-Y');
        return $tanggal;
    }

    public function getCountDiriSendiri1Attribute(){
        $jumlah = Self::where('report_type', 1)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 1
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountDiriSendiri2Attribute(){
        $jumlah = Self::where('report_type', 1)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 2
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountDiriSendiri3Attribute(){
        $jumlah = Self::where('report_type', 1)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 3
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountDiriSendiri4Attribute(){
        $jumlah = Self::where('report_type', 1)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 4
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountDiriSendiri5Attribute(){
        $jumlah = Self::where('report_type', 1)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 5
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountDiriSendiri6Attribute(){
        $jumlah = Self::where('report_type', 1)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 6
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountDiriSendiri7Attribute(){
        $jumlah = Self::where('report_type', 1)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 7
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountDiriSendiri8Attribute(){
        $jumlah = Self::where('report_type', 1)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 8
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountDiriSendiri9Attribute(){
        $jumlah = Self::where('report_type', 1)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 9
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountDiriSendiri10Attribute(){
        $jumlah = Self::where('report_type', 1)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 10
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountOrangLain1Attribute(){
        $jumlah = Self::where('report_type', 2)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 1
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountOrangLain2Attribute(){
        $jumlah = Self::where('report_type', 2)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 2
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountOrangLain3Attribute(){
        $jumlah = Self::where('report_type', 2)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 3
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountOrangLain4Attribute(){
        $jumlah = Self::where('report_type', 2)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 4
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountOrangLain5Attribute(){
        $jumlah = Self::where('report_type', 2)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 5
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountOrangLain6Attribute(){
        $jumlah = Self::where('report_type', 2)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 6
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountOrangLain7Attribute(){
        $jumlah = Self::where('report_type', 2)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 7
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountOrangLain8Attribute(){
        $jumlah = Self::where('report_type', 2)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 8
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountOrangLain9Attribute(){
        $jumlah = Self::where('report_type', 2)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 9
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountOrangLain10Attribute(){
        $jumlah = Self::where('report_type', 2)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 10
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountPanicButton1Attribute(){
        $jumlah = Self::where('report_type', 3)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 1
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountPanicButton2Attribute(){
        $jumlah = Self::where('report_type', 3)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 2
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountPanicButton3Attribute(){
        $jumlah = Self::where('report_type', 3)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 3
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountPanicButton4Attribute(){
        $jumlah = Self::where('report_type', 3)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 4
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountPanicButton5Attribute(){
        $jumlah = Self::where('report_type', 3)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 5
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountPanicButton6Attribute(){
        $jumlah = Self::where('report_type', 3)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 6
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountPanicButton7Attribute(){
        $jumlah = Self::where('report_type', 3)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 7
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountPanicButton8Attribute(){
        $jumlah = Self::where('report_type', 3)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 8
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountPanicButton9Attribute(){
        $jumlah = Self::where('report_type', 3)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 9
        ])->count();
        return $jumlah ?? 0;
    }

    public function getCountPanicButton10Attribute(){
        $jumlah = Self::where('report_type', 3)->where([
            'tmp_created_date' => $this->tmp_created_date,
            'diseases_id' => 10
        ])->count();
        return $jumlah ?? 0;
    }



}
