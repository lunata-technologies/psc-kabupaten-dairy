<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoskoVillageCoverageArea extends Model
{
    protected $table = 'posko_coverage_village_area';
    protected $guarded = [];
    protected $appends = ['village_name'];

    public function villageAppends(){
        return $this->belongsTo(Village::class, 'village_id');
    }
    public function getVillageNameAttribute(){
        return $this->villageAppends->name ?? '-';
    }
}
