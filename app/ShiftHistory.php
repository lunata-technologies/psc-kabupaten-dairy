<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftHistory extends Model
{
    protected $table = "shift_history";
    protected $guarded = [];
    protected $appends = ['name'];

    public function employee_appends(){
        return $this->belongsTo(Employee::class, 'employee_uid', 'uid');
    }
    public function getNameAttribute(){
        return $this->employee_appends->name ?? '-';
    }
}
