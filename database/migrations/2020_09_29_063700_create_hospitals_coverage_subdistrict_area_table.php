<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalsCoverageSubdistrictAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('hospitals_coverage_subdistrict_area')){
            Schema::create('hospitals_coverage_subdistrict_area', function (Blueprint $table) {
                $table->id();
                $table->integer('hospital_id');
                $table->integer('subdistrict_id');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitals_coverage_subdistrict_area');
    }
}
