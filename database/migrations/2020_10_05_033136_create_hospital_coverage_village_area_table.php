<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalCoverageVillageAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('hospital_coverage_village_area')){
            Schema::create('hospital_coverage_village_area', function (Blueprint $table) {
                $table->id();
                $table->integer('hospital_id');
                $table->integer('village_id');
                $table->integer('subdistrict_id');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital_coverage_village_area');
    }
}
