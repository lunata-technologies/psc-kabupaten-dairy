<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('people')){
            Schema::create('people', function (Blueprint $table) {
                if(config('database.default') == 'pgsql')
                    $table->uuid('uid')->default(DB::raw('gen_uuid()'))->primary();
                else if(config('database.default') == 'mysql')
                    $table->uuid('uid')->default(DB::raw('uuid()'))->primary();
                $table->integer('sub-district-id')->nullable();
                $table->integer('village-id')->nullable();
                $table->string('nik')->unique();
                $table->string('name');
                $table->string('email')->unique()->nullable();
                $table->string('password');
                $table->string('phone');
                $table->integer('age');
                $table->string('gender');
                $table->dateTime('approved')->nullable();
                $table->string('avatar')->nullable();
                $table->string('selfie')->nullable();
                $table->string('selfie-ktp')->nullable();
                $table->string('address');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
