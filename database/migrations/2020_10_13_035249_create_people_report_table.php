<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('people_report')){
            Schema::create('people_report', function (Blueprint $table) {
                $table->id();
                $table->uuid('people_uid'); // Hit From ANDROID
                $table->integer('diseases_id')->nullable(); // Hit From ANDROID
                $table->integer('report_type'); // Hit From ANDROID 1. Own Self 2. Other People 3. Panic Button
                $table->string('longtitude'); // Hit From ANDROID
                $table->string('latitude'); // Hit From ANDROID
                $table->string('prove_images1')->nullable(); // Hit From ANDROID
                $table->string('prove_images2')->nullable(); // Hit From ANDROID
                $table->text('description')->nullable(); // Hit From ANDROID
                $table->integer('subdistrict_id')->nullable(); // Hit From ANDROID
                $table->integer('village_id')->nullable(); // Hit From ANDROID
                $table->text('address')->nullable(); // Hit From ANDROID
                $table->text('other_people_name')->nullable(); // Hit From ANDROID
                $table->integer('other_people_age')->nullable(); // Hit From ANDROID
                $table->string('other_people_gender')->nullable(); // Hit From ANDROID
                $table->integer('rescuer_id')->nullable(); // Hit From PSC ADMIN
                $table->integer('rest_id')->nullable(); // Hit From PSC ADMIN
                $table->dateTime('rest_accept_date')->nullable(); // Hit From PSC ADMIN
                $table->dateTime('admin_confirmed')->nullable(); // Hit From PSC ADMIN
                $table->dateTime('rescuer_confirmed')->nullable(); // Hit From ANDROID
                $table->dateTime('rescuer_otw')->nullable(); // Hit From ANDROID
                $table->dateTime('rescuer_arrived_people')->nullable(); // Hit From ANDROID
                $table->dateTime('rescuer_arrived_hospital')->nullable(); // Hit From ANDROID
                $table->dateTime('rest_confirmed')->nullable(); // Hit From Hospital/Rest ADMIN
                $table->string('status')->nullable();
                $table->string('confirmed_description')->nullable();
                $table->dateTime('tmp_created_date');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people_report');
    }
}
