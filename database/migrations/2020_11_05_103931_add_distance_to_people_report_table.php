<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDistanceToPeopleReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('people_report', function (Blueprint $table) {
            $table->string('posko_distance')->after('latitude')->nullable();
            $table->string('hospital_distance')->after('posko_distance')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('people_report', function (Blueprint $table) {
            $table->dropColumn('posko_distance');
            $table->dropColumn('hospital_distance');
        });
    }
}
