<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Table User
        DB::table('users')
        ->insert([
            'uid' => '2aa892ba-23ac-67cb-c196-f96ac85cd987',
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'phone_number' => '0800 0000 0000',
            'password' => bcrypt('admin')
        ]);

        // Table Employee
        DB::table('employee')
        ->insert([
            'uid' => '2aa892ba-23ac-67cb-c196-f96ac85cd987',
            'name' => 'Pegawai Rumah Sakit',
            'username' => 'pegawai 123',
            'phone' => '0800 0000 0000',
            // 'password' => bcrypt('pegawai123'),
            'password' => 'pegawai123',
            'type' => 1,
            'rest_id' => 1
        ]);

        // Create People Data
        DB::table('people')->insert([
            'uid' => '59cac328-2634-58be-ecc4-56987c699b38',
            'nik' => '1207261911990013',
            'name' => 'Orang Bego',
            'email' => 'orangbego@gmail.com',
            'password' => bcrypt('admin123'),
            'address' => 'Jl. Jalan aja No. 250 D',
            'phone' => '+6221 7863 8759',
            'age' => '18',
            'gender' => 'M',
        ]);

        DB::table('people_report')->insert([
            [
                'people_uid' => '59cac328-2634-58be-ecc4-56987c699b38',
                'diseases_id' => 1,
                'report_type' => 1,
                'longtitude' => '98.6406589',
                'latitude' => '3.5673368',
                'prove_images1' => 'people-report/prove_images1-tabrakan.jpg',
                'prove_images2' => 'image2',
                'description' => 'Test report',
                'subdistrict_id' => 1,
                'village_id' => 1,
                'address' => 'Test Alamat',
                'other_people_name' => 'Cek name',
                'other_people_age' => 1,
                'other_people_gender' => 'M',
                'status' => 'Pending',
                'rest_id' => 1,
                'rest_accept_date' => Carbon::now('Asia/Jakarta'),
                'tmp_created_date' => Carbon::now('Asia/Jakarta'),
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'people_uid' => '59cac328-2634-58be-ecc4-56987c699b38',
                'diseases_id' => 2,
                'report_type' => 2,
                'longtitude' => '98.6406589',
                'latitude' => '3.5673368',
                'prove_images1' => 'people-report/prove_images1-tabrakan.jpg',
                'prove_images2' => 'image2',
                'description' => 'Test report',
                'subdistrict_id' => 2,
                'village_id' => 1,
                'address' => 'Test Alamat',
                'other_people_name' => 'Cek name',
                'other_people_age' => 1,
                'other_people_gender' => 'M',
                'status' => 'Pending',
                'rest_id' => 2,
                'rest_accept_date' => '2020-11-04 05:30:23',
                'tmp_created_date' => '2020-11-04 05:30:23',
                'created_at' => '2020-11-04 05:30:23',
            ],
            [
                'people_uid' => '59cac328-2634-58be-ecc4-56987c699b38',
                'diseases_id' => 3,
                'report_type' => 3,
                'longtitude' => '98.6406589',
                'latitude' => '3.5673368',
                'prove_images1' => 'people-report/prove_images1-tabrakan.jpg',
                'prove_images2' => 'image2',
                'description' => 'Test report',
                'subdistrict_id' => 3,
                'village_id' => 1,
                'address' => 'Test Alamat',
                'other_people_name' => 'Cek name',
                'other_people_age' => 1,
                'other_people_gender' => 'M',
                'status' => 'Pending',
                'rest_id' => 3,
                'rest_accept_date' => '2020-10-30 11:23:11',
                'tmp_created_date' => '2020-10-30 11:23:11',
                'created_at' => '2020-10-30 11:23:11'
            ]
        ]);

        for($i = 1; $i < 12; $i++){
            DB::table('sub_district')->insert([
                'name' => 'Distrik ' . $i,
                'longtitude' => '98.234234',
                'latitude' => '3.12312321'
            ]);
        }


        for($i = 1; $i < 12; $i++){
            DB::table('village')->insert([
                'sub-district-id' => $i,
                'name' => 'Village ' . $i,
                'longtitude' => '3.2343242',
                'latitude' => '89.234243'
            ]);
        }


        // Create Diseases
        // $diseases = ['Ringan', 'Sedang', 'Kronis'];

        for($i = 0; $i < 12; $i++){
            DB::table('detail_diseases')->insert([
                'nama' => 'Penyakit ' . $i,
            ]);
        }

        // Create Hospital, Posko, Puskesmas








        for($i=0; $i<10; $i++){
            DB::table('hospitals')->insert([
                'name' => 'Hospital - ' .$i,
                'tipe' => 1,
                'address' => 'Jl. Jalan aja',
                'longtitude' => '98.6270736',
                'latitude' => '3.5709019'
            ]);
        };

        for($i=0; $i<10; $i++){
            DB::table('hospitals')->insert([
                'name' => 'Puskesmas - ' .$i,
                'tipe' => 2,
                'address' => 'Jl. Jalan aja',
                'longtitude' => '98.6270736',
                'latitude' => '3.5709019'
            ]);
        };

        for($i=0; $i<10; $i++){
            DB::table('hospitals')->insert([
                'name' => 'Posko - ' .$i,
                'tipe' => 3,
                'address' => 'Jl. Jalan aja',
                'longtitude' => '98.6270736',
                'latitude' => '3.5709019'
            ]);
        };
    }
}
