@extends('app-layout.master', [
    'mainMenuLink' => url('sub-district'),
    'mainMenu' => 'Territory',
    'currentPage' => 'Sub District',
])

@section('content')
<div class="container-fluid">
    <div class="row second-chart-list third-news-update">
        <div class="col-md-12 col-lg-4">
            <form action="{{url('sub-district')}}" method="POST">@csrf
                <div class="card">
                    <div class="card-header border-bottom">
                        <h5>Add new Sub District</h5>
                    </div>                
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <label for="">Name</label>
                                <input type="text" name="name" class="form-control">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12">
                                <button class="btn float-right btn-info btn-block">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12 col-lg-8">
            @if(count($subDistrict)>0)
                <div class="row">
                @foreach ($subDistrict as $item)
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="ml-2">{{$item['name']}}</span>                            
                                <a class="btn btn-success btn-sm float-right ml-2" href="{{url('sub-district-detail/'.$item['id'])}}"><i class="fas fa-eye" style="font-size:18px;"></i></a>
                                <a class="btn btn-info btn-sm float-right" href="#"> <i class="fas fa-edit" style="font-size:18px;"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            @else
            <div class="card alert alert-soft-info">
                <div class="card-body">
                    <strong>Data not found !</strong> <br>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Necessitatibus voluptatem praesentium voluptatibus nam eius illo expedita aspernatur similique repellat blanditiis delectus sed voluptatum, pariatur excepturi nostrum reprehenderit quaerat quas quis?
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection