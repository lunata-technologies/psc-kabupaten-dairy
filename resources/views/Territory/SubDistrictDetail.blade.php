@extends('app-layout.master', [
    'mainMenuLink' => url('subdistrict/'),
    'mainMenu' => 'Territory',
    'currentPage' => 'Sub District Detail',
])

@section('content')
<div class="row p-2">
    <div class="col-12">
        <i class="fas fa-map-marker-alt"></i>
        <span class="ml-2">{{$subDistrict['name']}}</span>
    </div>
    <div class="col-12 mt-2">
        <div class="card">
            <div class="card-header bg-info pb-0 text-white">
                <h6><i class="fas fa-check"></i> <span class="ml-2 text-white">Here detail information about {{$subDistrict['name']}}</span></h6>
            </div>
            <div class="card-body">
                <div class="row mt-3">
                    <div class="col-12">
                        <h3>Village area in this <span class="text-info bold">{{$subDistrict['name']}}</span> :</h3>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12 pb-4">
                        <a class="btn btn-info float-right" href="#"  data-toggle="modal" data-target="#newVillage">
                            <i class="fas fa-plus"></i>
                            <span class="ml-2">Add New Village Area</span>
                        </a>
                    </div>
                    @foreach ($village as $item)
                        <div class="col-6">
                            <div class="alert alert-info">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="ml-2">{{$item['name']}}</span>
                                <a class="btn btn-danger btn-sm float-right" href="#" onclick="confirm_me('Delete this, means delete all relation to this Village Coverage Area', '{{url('village-delete/'.$item['id'])}}')"> <i class="fas fa-trash" style="font-size:18px;"></i></a>
                                <a class="btn btn-info btn-sm float-right" href="#" onclick="load('{{url('village-edit/'.$item['id'])}}','village-edit')" data-toggle="modal" data-target="#editVillage"> <i class="fas fa-edit" style="font-size:18px;"></i></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Edit Village -->
<div class="modal fade" id="editVillage" tabindex="-1" aria-labelledby="editVillageLabel" aria-hidden="true">
    <div class="modal-dialog" id="village-edit">
        
    </div>
</div>
<!-- Modal -->
<form action="{{url('village')}}" method="POST">@csrf
<div class="modal fade" id="newVillage" tabindex="-1" aria-labelledby="newVillageLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="newVillageLabel">New Village</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="from_page_sub_district_detail" value="true">
            <input type="hidden" name="sub-district-id" value="{{$subDistrict['id']}}">
            <div class="row">
                <div class="col-12">
                    <label for="">Village Area Name :</label>
                    <input type="text" name="name" class="form-control">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </div>
    </div>
</div>
</form>
@endsection