@extends('app-layout.master', [
    'mainMenuLink' => url('village'),
    'mainMenu' => 'Territory',
    'currentPage' => 'Village Area',
])

@section('content')
<div class="container-fluid">
    <div class="row second-chart-list third-news-update">
        <div class="col-md-12 col-lg-4">
            <form action="{{url('village')}}" method="POST">@csrf
                <div class="card">
                    <div class="card-header border-bottom">
                        <h5>Add new Village</h5>
                    </div>                
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <label for="">Name</label>
                                <input type="text" name="name" class="form-control">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12">
                                <label for="">Sub District</label>
                                <select name="sub-district-id" id="" class="form-control">
                                    <option value="" hidden>Choose Sub District</option>
                                    @foreach ($subDistrict as $item)
                                        <option value="{{$item['id']}}">{{$item['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12">
                                <button class="btn float-right btn-info btn-block">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12 col-lg-8">
            @if(count($village)>0)
                @foreach ($village as $item)
                    <div class="card">
                        <div class="card-body">
                            <i class="fas fa-map-marker-alt"></i>
                            <span class="ml-2">{{$item['name']}}</span>
        
                            <a href="#"> <i class="fas fa-edit float-right"></i></a>
                        </div>
                    </div>
                @endforeach
            @else
            <div class="card alert alert-soft-info">
                <div class="card-body">
                    <strong>Data not found !</strong> <br>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Necessitatibus voluptatem praesentium voluptatibus nam eius illo expedita aspernatur similique repellat blanditiis delectus sed voluptatum, pariatur excepturi nostrum reprehenderit quaerat quas quis?
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection