<form action="{{url('village-edit/'.$village['id'])}}" method="POST">@csrf
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="editVillageLabel">Edit Village Detail</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" >
            <div class="row">
                <div class="col-12">
                    <label for="">Village Area Name :</label>
                    <input type="text" name="name" value="{{$village['name']}}" class="form-control">
                </div>
            </div>    
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </div>
</form>