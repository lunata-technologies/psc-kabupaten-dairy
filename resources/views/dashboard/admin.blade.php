@extends('app-layout.master', [
    'mainMenuLink' => url('admin'),
    'mainMenu' => 'Admin',
    'currentPage' => null,
])

@section('content')

<div class="col-sm-6 col-md-6 col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="col-12 pb-4">
                <button class="btn btn-info float-right " data-toggle="modal" data-target="#admin">Tambah Admin</button>
            </div>
            <div class="container mt-4">
                <div class="table-responsive-lg table-responsive-md table-responsive-sm">
                    <table class="table table-striped table-bordered text-center" cellspacing="0" id="table_hospital" style="width: 100%" style="width: 100%">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Email</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                @if($item['uid'] != Session::get('uid'))
                                    <td>{{$loop->iteration}}</td>
                                    <td class="text-left">{{$item['name']}}</td>
                                    <td class="text-left">{{$item['email']}}</td>
                                    <td class="text-center"><a href="{{url('admin-delete/'.$item['uid'])}}" class="btn btn-danger text-white"><i class="fas fa-trash"></i></a></td>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="admin" tabindex="-1" aria-labelledby="adminLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="adminLabel">Tambah Admin</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin/register')}}" method="POST">@csrf
                    <div class="form-group">
                        <label class="text-label" for="name_2">Name:</label>
                        <div class="input-group input-group-merge">
                            <input required id="name_2" name="name" type="text" required="" class="form-control form-control-prepended" placeholder="Rian Gho">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="far fa-user"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="text-label" for="email_2">Email Address:</label>
                        <div class="input-group input-group-merge">
                            <input required id="email_2" name="email" type="email" required="" class="form-control form-control-prepended" placeholder="riangho@mikroskil.ac.id">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="far fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="text-label" for="password_2">Password:</label>
                        <div class="input-group input-group-merge">
                            <input required id="password_2" name="password" type="password" required="" class="form-control form-control-prepended" placeholder="Enter your password">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="fas fa-key"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-5">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" checked="" class="custom-control-input" id="terms" />
                            <label class="custom-control-label" for="terms">I accept <a href="#">Terms and Conditions</a></label>
                        </div>
                    </div>
                    @if(session('failed'))
                    <div class="form-group mb-5">
                        <div class="alert alert-danger">
                            <i class="fas fa-exclamation"></i>
                            <span class="ml-2">{{session('failed')}}</span>
                        </div>
                    </div>
                    @endif
                    <div class="form-group text-center">
                        <button class="btn btn-primary mb-2" type="submit">Create Account</button><br>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </div>
@endsection