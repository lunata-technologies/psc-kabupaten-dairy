@extends('app-layout.master', [
    'mainMenuLink' => url('/'),
    'mainMenu' => 'Dashboard',
    'currentPage' => null,
])

@section('title', 'Dashboard')

@section('content')
    <div class="row card-group-row">
        <div class="col-lg-3 col-md-3 col-sm-3 card-group-row__col">
            <div class="card card-group-row__card card-body card-body-x-lg flex-row align-items-center bg-info">
                <div class="flex">
                    <div class="card-header__title text-white mb-2">Member</div>
                    <div class="h3 text-white">{{$countMember}}</div>
                </div>
                <div><i class="text-white material-icons icon-danger icon-40pt ml-3">account_circle</i></div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 card-group-row__col">
            <div class="card card-group-row__card card-body card-body-x-lg flex-row align-items-center bg-warning">
                <div class="flex">
                    <div class="card-header__title text-white mb-2">Diri Sendiri</div>
                    <div class="h3 text-white">{{$countSelfReport}}</div>
                </div>
                <div><i class="text-white material-icons icon-40pt ml-3">report</i></div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 card-group-row__col">
            <div class="card card-group-row__card card-body card-body-x-lg flex-row align-items-center" style="background: #fd7e14;">
                <div class="flex">
                    <div class="card-header__title text-white mb-2">Orang Lain</div>
                    <div class="h3 text-white">{{$countOtherReport}}</div>
                </div>
                <div><i class="text-white material-icons icon-40pt ml-3">report</i></div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 card-group-row__col">
            <div class="card card-group-row__card card-body card-body-x-lg flex-row align-items-center bg-danger">
                <div class="flex">
                    <div class="card-header__title text-white mb-2">Panic Button</div>
                    <div class="h3 text-white">{{$countPanicReport}}</div>
                </div>
                <div><i class="text-white material-icons icon-40pt ml-3">report</i></div>
            </div>
        </div>
    </div>

    <form method="" action="">
        <div class="card card-form d-flex flex-column flex-sm-row">
            <div class="card-form__body card-body-form-group flex">
                <div class="row">
                    <div class="col-sm-auto mx-auto">
                        <div class="form-group" style="width: 200px;">
                            <label for="flatpickrSample01"></label>
                            <input id="flatpickrSample01" name="awal" type="text" class="form-control awal" placeholder="Flatpickr example" data-toggle="flatpickr" value="{{$reqAwal}}">
                        </div>
                    </div>
                    <div class="col-sm-auto">
                        <div class="form-group">
                            <label for="filter_stock"></label>
                            <div class="custom-control custom-checkbox mt-sm-2">
                                <label class="" for="filter_stock">TO</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-auto mx-auto">
                        <div class="form-group" style="width: 200px;">
                            <label for="flatpickrSample01"></label>
                            <input id="flatpickrSample01" name="akhir" type="text" class="form-control akhir" placeholder="Flatpickr example" data-toggle="flatpickr" value="{{$reqAkhir}}">
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn bg-white border-left border-top border-top-sm-0 rounded-top-0 rounded-top-sm rounded-left-sm-0"><i class="material-icons text-primary icon-20pt">refresh</i></button>
        </div>
    </form>

    <div class="row card-group-row">
        <div class="col-lg-7 col-md-7 col-sm-7">
            <figure class="highcharts-figure">
                <div id="container-line"></div>
            </figure>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5">
            <figure class="highcharts-figure">
                <div id="container-pie"></div>
            </figure>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <figure class="highcharts-figure">
                    <div id="container-bar"></div>
                </figure>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function(){
            Highcharts.chart('container-line', {
                colors: ['#ffc107','#6610f2','#dc3545','#36abd7'],
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'JENIS PENGADUAN'
                },
                subtitle: {
                    // text: 'Source: WorldClimate.com'
                },
                xAxis: {
                    categories: {!! json_encode($tmpDate) !!}
                },
                yAxis: {
                    title: {
                        text: 'Pengaduan (Jumlah)'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series:
                {!! json_encode($jenisPengaduan) !!}
            });
        });

        $(function(){
            Highcharts.chart('container-pie', {
                colors: ['#5e1bcb','#7ae34c','#e14655','#d7b44b','#2a3cbb','#bb6d2a','#772571','#ca3169','#214646','#309c2e','#aed026','#9a681d','#d8208e','#30e210','#e25d10'],
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'JENIS PENYAKIT'
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    data: {!! json_encode($jenisPenyakit) !!}
                }]

            });
        });

        Highcharts.chart('container-bar', {
            colors: ['#ffc107','#fd7e14','#dc3545','#36abd7'],
            chart: {
                type: 'column'
            },
            title: {
                text: 'AREA KECAMATAN'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: {!! json_encode($tmpKecamatan) !!},
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Pengaduan / Kecamatan'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: {!! json_encode($aduanPerkecamatan) !!},
        });
    </script>
@endsection
