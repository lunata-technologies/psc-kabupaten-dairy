@extends('app-layout.master', [
    'mainMenuLink' => url('laporan/'),
    'mainMenu' => 'Laporan',
    'currentPage' => null,
])

@section('title', 'Laporan')

@section('content')
<style>
    table{
        border: 1px solid black !important;
    }

    tr{
        border: 1px solid black !important;
    }

    th{
        border: 1px solid black !important;
    }

    td{
        border: 1px solid black !important;
    }

    @media print{
        @page {
            size: landscape;
        }

        table {
            transform: scale(0.9);
            margin : 0 0 0 -5.05%;
        }

        #cetak-laporan tr th{
            padding: 0.2rem;
            font-size: 12px;
        }

        #cetak-laporan tr td{
            padding: 0.2rem;
            font-size: 12px;
        }
    }
</style>
    <div class="container hidden-after">
        <form method="" action="">
            <div class="card card-form d-flex flex-column flex-sm-row">
                <div class="card-form__body card-body-form-group flex">
                    <div class="row">
                        <div class="col-sm-auto mx-auto">
                            <div class="form-group" style="width: 200px;">
                                <label for="flatpickrSample01"></label>
                                <input id="flatpickrSample01" name="pengaduan_awal" type="text" class="form-control" placeholder="Flatpickr example" data-toggle="flatpickr" value="{{$awal}}">
                            </div>
                        </div>
                        <div class="col-sm-auto">
                            <div class="form-group">
                                <label for="filter_stock"></label>
                                <div class="custom-control custom-checkbox mt-sm-2">
                                    <label class="" for="filter_stock">TO</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-auto mx-auto">
                            <div class="form-group" style="width: 200px;">
                                <label for="flatpickrSample01"></label>
                                <input id="flatpickrSample01" name="pengaduan_akhir" type="text" class="form-control" placeholder="Flatpickr example" data-toggle="flatpickr" value="{{$akhir}}">
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn bg-white border-left border-top border-top-sm-0
                rounded-top-0 rounded-top-sm rounded-left-sm-0"><i class="material-icons text-primary icon-20pt">refresh</i></button>
                <button type="button" class="btn bg-white border-left border-top border-top-sm-0
                rounded-top-0 rounded-top-sm rounded-left-sm-0" id="cetak-rekap-laporan"><i class="material-icons text-primary icon-20pt">print</i></button>
            </div>
        </form>
    </div>
    <div id="cetak-rekap">
        <div class="row p-2 d-flex justify-content-between mx-auto">
            <div class="col-sm-5 col-md-5 col-lg-5">
                <i class="fas fa-building"></i>
                <span class="ml-2">Rekap Laporan</span>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3">
                <span class="mx-auto hidden">
                    <i class="fas fa-home"></i>
                    <span class="ml-2">PSC DINKES</span>
                </span>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <span class="float-right hidden">
                    <i class="fas fa-calendar"></i>
                    <span class="ml-2">{{Date('d-m-Y')}}</span>
                </span>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12">
            {{-- <div class="card">
                <div class="card-body"> --}}
                    <div class="container">
                        <div class="">
                            <div class="table-responsive" width="100%" style="overflow: scroll;">
                                <table class="table table-fixed text-center" id="cetak-laporan" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Tanggal</th>
                                            <th scope="col" colspan="10">Diri Sendiri</th>
                                            <th scope="col" colspan="10">Orang Lain</th>
                                            <th scope="col" colspan="10">Panic Button</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>/</td>
                                            @for($i = 0; $i < 3; $i++)
                                                @foreach($arrKode as $key => $value)
                                                    <td>{{$value}}</td>
                                                @endforeach
                                            @endfor

                                        </tr>
                                        @foreach($data as $key => $val2)
                                            <tr>
                                                <td>{{$val2->format_tanggal}}</td>

                                                <td>{{$val2->count_diri_sendiri_1}}</td>
                                                <td>{{$val2->count_diri_sendiri_2}}</td>
                                                <td>{{$val2->count_diri_sendiri_3}}</td>
                                                <td>{{$val2->count_diri_sendiri_4}}</td>
                                                <td>{{$val2->count_diri_sendiri_5}}</td>
                                                <td>{{$val2->count_diri_sendiri_6}}</td>
                                                <td>{{$val2->count_diri_sendiri_7}}</td>
                                                <td>{{$val2->count_diri_sendiri_8}}</td>
                                                <td>{{$val2->count_diri_sendiri_9}}</td>
                                                <td>{{$val2->count_diri_sendiri_10}}</td>

                                                <td>{{$val2->count_orang_lain_1}}</td>
                                                <td>{{$val2->count_orang_lain_2}}</td>
                                                <td>{{$val2->count_orang_lain_3}}</td>
                                                <td>{{$val2->count_orang_lain_4}}</td>
                                                <td>{{$val2->count_orang_lain_5}}</td>
                                                <td>{{$val2->count_orang_lain_6}}</td>
                                                <td>{{$val2->count_orang_lain_7}}</td>
                                                <td>{{$val2->count_orang_lain_8}}</td>
                                                <td>{{$val2->count_orang_lain_9}}</td>
                                                <td>{{$val2->count_orang_lain_10}}</td>

                                                <td>{{$val2->count_panic_button_1}}</td>
                                                <td>{{$val2->count_panic_button_2}}</td>
                                                <td>{{$val2->count_panic_button_3}}</td>
                                                <td>{{$val2->count_panic_button_4}}</td>
                                                <td>{{$val2->count_panic_button_5}}</td>
                                                <td>{{$val2->count_panic_button_6}}</td>
                                                <td>{{$val2->count_panic_button_7}}</td>
                                                <td>{{$val2->count_panic_button_8}}</td>
                                                <td>{{$val2->count_panic_button_9}}</td>
                                                <td>{{$val2->count_panic_button_10}}</td>

                                            </tr>
                                        @endforeach
                                        {{-- @foreach($data as $key => $value)
                                            <tr>
                                                <td>{{$value->format_tanggal}}</td>
                                                <td scope="col">{{$value->total_diri_sendiri}}</td>
                                                <td scope="col">{{$value->total_orang_lain}}</td>
                                                <td scope="col">{{$value->total_panic_button}}</td>
                                            </tr>
                                        @endforeach --}}
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                {{-- </div>
            </div> --}}
        </div>
    </div>
@endsection

@section('modal')

@endsection

@section('script')
    <script>
        $(document).on('click', '#cetak-rekap-laporan', function(){
            window.print('#table-hospital');
        });


    </script>
@endsection
