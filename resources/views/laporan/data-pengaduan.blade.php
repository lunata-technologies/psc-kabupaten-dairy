@extends('app-layout.master', [
    'mainMenuLink' => url('laporan/'),
    'mainMenu' => 'Laporan',
    'currentPage' => null,
])

@section('title', 'Laporan')

@section('content')

    <div class="container hidden-after">
        <form method="" action="">
            <div class="card card-form d-flex flex-column flex-sm-row">
                <div class="card-form__body card-body-form-group flex">
                    <div class="row">
                        <div class="col-sm-auto mx-auto">
                            <div class="form-group" style="width: 200px;">
                                <label for="flatpickrSample01"></label>
                                <input id="flatpickrSample01" name="pengaduan_awal" type="text" class="form-control" placeholder="Flatpickr example" data-toggle="flatpickr" value="{{$awal}}">
                            </div>
                        </div>
                        <div class="col-sm-auto">
                            <div class="form-group">
                                <label for="filter_stock"></label>
                                <div class="custom-control custom-checkbox mt-sm-2">
                                    <label class="" for="filter_stock">TO</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-auto mx-auto">
                            <div class="form-group" style="width: 200px;">
                                <label for="flatpickrSample01"></label>
                                <input id="flatpickrSample01" name="pengaduan_akhir" type="text" class="form-control" placeholder="Flatpickr example" data-toggle="flatpickr" value="{{$akhir}}">
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn bg-white border-left border-top border-top-sm-0
                rounded-top-0 rounded-top-sm rounded-left-sm-0"><i class="material-icons text-primary icon-20pt">refresh</i></button>
                <button type="button" class="btn bg-white border-left border-top border-top-sm-0
                rounded-top-0 rounded-top-sm rounded-left-sm-0" id="cetak-rekap-pengaduan"><i class="material-icons text-primary icon-20pt">print</i></button>
            </div>
        </form>
    </div>
    <div id="cetak-rekap">
        <div class="row p-2 d-flex justify-content-between mx-auto">
            <div class="col-sm-5 col-md-5 col-lg-5">
                <i class="fas fa-building"></i>
                <span class="ml-2">Rekap Laporan</span>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3">
                <span class="mx-auto hidden">
                    <i class="fas fa-home"></i>
                    <span class="ml-2">PSC DINKES</span>
                </span>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <span class="float-right hidden">
                    <i class="fas fa-calendar"></i>
                    <span class="ml-2">{{Date('d-m-Y')}}</span>
                </span>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="container">
                        <div class="table-responsive-lg table-responsive-md table-responsive-sm">
                            <table class="table table-striped table-bordered text-center" cellspacing="0" id="table_rekap_pegaduan" style="width: 100%" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th scope="col">Waktu</th>
                                        <th scope="col">Nama Pelapor</th>
                                        <th scope="col">Jenis Pengaduan</th>
                                        <th scope="col">Jarak Posko Ke Lokasi Kejadian</th>
                                        <th scope="col">Jarak Lokasi Kejadian Ke Faskes</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection

@section('script')
    <script>
        $(document).on('click', '#cetak-rekap-pengaduan', function(){
            window.print('#table_rekap_pegaduan');
        });

        $(function(){
            var link = '{{ url('laporan-pengaduan-datatable') }}/';

            var table = $('#table_rekap_pegaduan').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: link,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'waktu', name: 'waktu'},
                    {data: 'pelapor', name: 'pelapor'},
                    {data: 'jenis_pengaduan', name: 'jenis_pengaduan'},
                    {data: 'jarak_posko', name: 'jarak_posko'},
                    {data: 'jarak_faskes', name: 'jarak_faskes'}
                ],
                "language": {
                    "infoEmpty": "No records available",
                    "emptyTable": "No Data Available",
                },
                "paging": true,
                "ordering": false,
                "info": true,
                "select": true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        });


    </script>
@endsection

