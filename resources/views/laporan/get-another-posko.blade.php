
<ul class="list-group mt-3">
    @php $i=0; @endphp
    @foreach($posko as $item)
    <li class="list-group-item"  >
        <div class="row">
            <div class="col-7">
                <span class="bold" style="font-size:20px;"> {{$item['name']}} </span> <p></p> <span> <i class="fas fa-map-marker-alt mr-2"></i> {{$item['distance']['distance']}} dari Lokasi</span>
            </div>
            <div class="col-5">
                <button class="btn btn-light float-right" key="{{$item['id']}}" number="{{$i}}" id="rescuers-point" style="height: 100%;">Pilih</button>
            </div>
        </div>
    </li>
    @php $i++; @endphp
    @endforeach
</ul>