@extends('app-layout.master', [
    'mainMenuLink' => url('laporan/'),
    'mainMenu' => 'Pengaduan',
    'currentPage' => null,
])

@section('title', 'Pengaduan')

@section('content')
    <div class="form-group">
        <form method="get" action="">
            <label for="fname">Tipe Laporan</label><br />
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-light active">
                    <input type="radio" name="tipeLaporan" id="tipeLaporan" value="1" @if($tipeLaporan == 1)
                    checked
                    @endif> Diri Sendiri
                </label>
                <label class="btn btn-light">
                    <input type="radio" name="tipeLaporan" id="tipeLaporan" value="2" @if($tipeLaporan == 2)
                    checked
                    @endif> Orang Lain
                </label>
                <label class="btn btn-light">
                    <input type="radio" name="tipeLaporan" id="tipeLaporan" value="3" @if($tipeLaporan == 3)
                    checked
                    @endif> Panic Button
                </label>
            </div>
            <button type="submit" class="d-none" id="filter-laporan">
        </form>
        <input type="hidden" id="jenisLaporan" value="{{$tipeLaporan}}">
    </div>

<div class="row p-2">
    <div class="col-12">
        <i class="fas fa-building"></i>
        <span class="ml-2">Daftar Pengaduan</span>
    </div>
</div>
<div class="row p-2">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="container">
                    <div class="table-responsive-lg table-responsive-md table-responsive-sm">
                        <table class="table table-striped table-bordered text-center" cellspacing="0" id="table_laporan" style="width: 100%" style="width: 100%">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Penyakit</th>
                                    <th scope="col">Kota</th>
                                    <th scope="col">Desa</th>
                                    <th scope="col">Posko</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<audio id="myaudio" src="{{('http://psc-fullstack.devs/assets/audio/sirene_ambulance.mp3')}}"></audio>
{{-- <button onclick="settime()">Set time to zero</button> --}}

{{-- <audio> --}}
    {{-- <source src="{{url('assets/audio/sirene_ambulance.ogg')}}" type="audio/ogg">
    <source src="{{url('assets/audio/sirene_ambulance.mp3')}}" type="audio/mpeg">
</audio> --}}

{{-- <button id="click_sirene" type="button">Play </button> --}}
@endsection

@section('modal')
    {{-- Modal --}}
    <div id="approve-laporan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form method="post" action="{{url('konfirmasi-pengaduan')}}">
                @csrf
                    <div class="modal-body">
                        <div class="px-3">
                            <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                                <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                                    <img class="navbar-brand-icon" src="assets/images/stack-logo-blue.svg" width="25" alt="Stack">
                                    <span>Konfirmasi Pengaduan</span>
                                </a>
                            </div>
                            <input class="form-control @error('status_id') is-invalid @enderror" type="hidden" name="status_id" id="status_id" />
                            <div class="form-group">
                                <label for="select02">Status Pengaduan</label>
                                <select id="select02" data-toggle="select" data-minimum-results-for-search="-1" class="form-control @error('status_konfirmasi') is-invalid @enderror" name="status_konfirmasi">
                                    <option value="Diterima">Diterima</option>
                                    <option value="Ditolak">Ditolak</option>
                                </select>
                                @error('status_konfirmasi')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status_keterangan">Keterangan:</label>
                                <input class="form-control @error('status_keterangan') is-invalid @enderror" type="text" name="status_keterangan" id="status_keterangan" />
                                @error('status_keterangan')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group text-center">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>
                    </div> <!-- // END .modal-body -->
                </form>
            </div> <!-- // END .modal-content -->
        </div> <!-- // END .modal-dialog -->
    </div> <!-- // END .modal -->
@endsection

@section('script')
    <script>
        // $(document).ready(function(){
        //     $(".audio_sirine").prop('muted', true);
        //     $(".audio_sirine")[0].play();
        // });

        // function playSound()
        // {
        //     var audio = new Audio('http://psc-fullstack.devs/assets/audio/sirene_ambulance.mp3');
        //     audio.play();
        // }

        // // setInterval(function() {
        // //     playSound();
        // //     console.log('cek interval');
        // // },2000);

        // function settime(){
        //     var audio= document.getElementById("myaudio");
        //     audio.currentTime=10;
        //     audio.play();
        //     console.log(audio.currentTime);
        //     console.log("Ok!");
        //     setInterval(function(){
        //         console.log("Ok!");
        //         if(audio.currentTime>15){

        //         audio.pause();
        //         }
        //     },1000);
        // }
        $(document).on('click', '#tipeLaporan', function() {
            var nilai = $('input:radio[name="tipeLaporan"]:checked').val();
            $( "#filter-laporan" ).trigger( "click" );
        });

        $(function(){
            var nilai = $('input:radio[name="tipeLaporan"]:checked').val();
            var link = '{{ url('pengaduan-datatable') }}/' + nilai;
            console.log(link);
            var table = $('#table_laporan').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: link,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'reporter', name: 'reporter'},
                    {data: 'nama_penyakit', name: 'nama_penyakit'},
                    {data: 'sub_district', name: 'sub_district'},
                    {data: 'village', name: 'village'},
                    {data: 'posko', name: 'posko'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'},
                ],
                "language": {
                    "infoEmpty": "No records available",
                    "emptyTable": "No Data Available",
                },
                "paging": true,
                "ordering": false,
                "info": true,
                "select": true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        });

        $(document).on('click', '.konfirmasi', function() {
            const id = $(this).attr('id_konfirmasi');
            console.log(id);
            $('#status_id').val(id);
        });

        //var myMusic= document.getElementById("sirene");
        //let timerId = setTimeout(function tick() {
            //console.log('Cek');
          //  document.getElementById("sirene").autoplay;
            //timerId = setTimeout(tick, 2000); // (*)
        //}, 2000);

        //var sound = document.createElement('audio')
        //sound.id = 'audio'
        //sound.controls = 'controls'
        //sound.src = 'http://psc-fullstack.devs/assets/audio/sirene_ambulance.mp3'
        //sound.type = 'audio/mp3'
        //document.body.appendChild(sound)

        //function playAudio() {
          //  console.log('Cek');
            //document.getElementById('sirene').play();
        //}

        //setTimeout("playAudio()", 3000);


    </script>
@endsection
