@extends('app-layout.master', [
    'mainMenuLink' => url('laporan/'),
    'mainMenu' => 'Laporan',
    'currentPage' => null,
])

@section('content')
<div class="row p-2">
    <div class="col-12">
        <i class="fas fa-building"></i>
        <span class="ml-2">Detail Laporan</span>
    </div>
</div>
<div class="row p-2">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header bg-danger">
                <h3 class="text-white">
                    <i class="fas fa-user"></i>
                    <span class="ml-3">{{$data['reporter_name']}}</span>
                </h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-lg-4">
                        <div class="card">
                            <img src="{{$data['reporter_foto']}}" class="card-img-top img-thumbnail" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{$data['reporter_name']}}</h6>
                            </div>
                        </div>
                        <div class="col">
                            <div id="mymap" style="width: 100%; height:300px; overflow:hidden;"></div>
                        </div>                        
                        @if($data['rescuer_id'] == null && $data['status'] != 'DECLINE')
                        <div class="col mt-4">
                            <button class="btn btn-sm btn-block text-white btn-danger konfirmasi" id="button-tolak-laporan">
                                <h4 class="text-white">
                                    <i class="fas fa-times"></i>
                                    <span class="ml-2">Tolak Laporan</span>
                                </h6>
                            </button>
                        </div>
                        @endif
                    </div>
                    
                        <div class="col-md-12 col-lg-8 d-none" id="tolak-laporan">
                            <div class="col">
                                <h6><i class="fas fa-times"></i> <span class="ml-2 bold">Tolak Laporan</span></h6>
                                <p style="text-secondary"><h6>Berikan keterangan kenapa anda menolak laporan ini ! Perhatian, mungkin saja laporan ini sangat mendesak,  mungkin anda dapat mempertimbangkannya lagi !</h6></p>
                                <form action="{{url('laporan-decline')}}" method="POST"> @csrf
                                    <input type="hidden" name="id" value="{{$data['id']}}">
                                    <textarea class="form-control mt-2" name="confirmed_description" name="" id="" cols="30" rows="10"></textarea>
                                    <button type="submit" class="btn btn-danger float-right mt-4">Setuju membatalkan laporan</button>
                                    <button class="btn btn-warning float-right mt-4 mr-2" id="batal-tolak-laporan">Tutup</button>
                                </form>
                            </div>
                        </div>
                        @if($data['diseases_id'] != null)
                            @if($data['rescuer_id'] == null || $data['rest_id'] == null || isset($_GET['edit']))
                                <div class="col-md-12 col-lg-8" id="lanjut-laporan">
                                    @if($data['rescuer_id'] != null && $data['rest_id'] == null)
                                        <div class="alert alert-warning">
                                            <i class="fas fa-check"></i>
                                            <span class="ml-2">Laporan ini telah ditindak lanjuti oleh pihak Posko Ambulance, Silahkan pilih rumah sakit atau puskesmas rujukan !</span>
                                        </div>
                                    @endif
                                    @if($data['status'] == 'DECLINE')
                                        <div class="alert alert-warning">
                                            <i class="fas fa-check"></i>
                                            <span class="ml-2">Laporan ini telah di Tolak</span>
                                        </div>
                                    @endif
                                    <div class="col">
                                        <h6><i class="fas fa-laptop-code"></i> <span class="ml-2 bold">Keterangan Laporan</span></h6>
                                        <p style="text-secondary"><h6>{!!$data['description']!!}</h6></p>
                                        <hr>
                                    </div>
                                    <div class="col mt-4">
                                        <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Jenis Laporan</span></h6>
                                        <p style="text-secondary"><h6>@if($data['report_type'] == 1) Diri Sendiri @elseif($data['report_type']==2) Orang Lain <p></p> <li> Nama : {{$data['other_people_name']}}</li> <li>Usia : {{$data['other_people_age']}}</li> @else Panic Button @endif</h6></p>
                                        <hr>
                                    </div>
                                    <div class="col mt-4">
                                        <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Jenis Penyakit</span></h6>
                                        <p style="text-secondary"><h6>{{$data['diseases_name']}}</h6></p>
                                        <hr>
                                    </div>
                                    <div class="col mt-4">
                                        <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Bukti Laporan</span></h6>
                                        <p style="text-secondary"><img  width="100" style="cursor: zoom-in" class="sample-image" src="{{$data['prove_images1']}}" /></p>
                                        <span>Klik untuk memperbesar gambar !</span>
                                        <hr>
                                    </div>
                                    @if($data['status'] != 'DECLINE')
                                        @if($data['rescuer_id'] == null || isset($_GET['posko']))
                                            @if($data['village_id'] != null)
                                                <div class="col mt-4">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h6><i class="fas fa-ambulance"></i> <span class="ml-2 bold">Posko Sesuai Dengan Desa Laporan</span></h6>
                                                        </div>
                                                        <div class="card-body">
                                                            @if(count($posko_coverage_default ?? '')>0)
                                                                @php $i=-1; $end_posko_default = 0; @endphp
                                                                    @foreach ($posko_coverage_default ?? '' as $item)                                        
                                                                        <ul class="list-group @if($i==-1) mt-3 @endif">
                                                                            <li class="list-group-item"key="{{$item['id']}}" number="{{$i}}" id="rescuers-point" >
                                                                                <div class="row">
                                                                                    <div class="col-7">
                                                                                        <span class="bold" style="font-size:20px;"> {{$item['name']}} </span> <p></p> <span> <i class="fas fa-map-marker-alt mr-2"></i> {{$item['distance']['distance'] ?? 'Unknown'}} dari Lokasi | Perkiraan waktu {{$item['time']['duration'] ?? 'Unknown'}} </span>
                                                                                    </div>
                                                                                    <div class="col-5">
                                                                                        <button class="btn btn-light float-right"  style="height: 100%;">Pilih</button>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    @php $i++; $end_posko_default++; @endphp
                                                                @endforeach
                                                            @else
                                                                <div class="alert alert-warning">
                                                                    Tidak ada posko yang sesuai dengan Desa Laporan !
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                                <div class="col mt-4">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h6><i class="fas fa-ambulance"></i> <span class="ml-2 bold">Posko Diluar dari Desa Laporan</span></h6>
                                                        </div>
                                                        <div class="card-body"> 
                                                            @if(count($posko)>0)                                               
                                                                <ul class="list-group mt-3 ">
                                                                    @php $i=0; @endphp
                                                                    @foreach($posko as $item)
                                                                    <li class="list-group-item" key="{{$item['id']}}" number="{{$i}}" id="rescuers-point" >
                                                                        <div class="row">
                                                                            <div class="col-7">
                                                                                <span class="bold" style="font-size:20px;"> {{$item['name']}} </span> <p></p> <span> <i class="fas fa-map-marker-alt mr-2"></i> {{$item['distance']['distance'] ?? 'Unknown'}} dari Lokasi | Perkiraan waktu {{$item['time']['duration'] ?? 'Unknown'}}</span>
                                                                            </div>
                                                                            <div class="col-5">
                                                                                <button class="btn btn-light float-right"  style="height: 100%;">Pilih</button>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    @php $i++; @endphp
                                                                    @endforeach
                                                                </ul>
                                                            @else
                                                                <div class="alert alert-warning">
                                                                    Tidak ada posko diluar Desa Laporan !
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                        @endif
                                        @if($data['rescuer_id'] != null && $data['rest_id'] == null || isset($_GET['hospital']))
                                            @if($data['village_id'] != null)
                                            <div class="col mt-4">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h6><i class="fas fa-hospital"></i> <span class="ml-2 bold">Rumah Sakit / Puskesmas Sesuai Dengan Desa Laporan </span></h6>
                                                    </div>
                                                    <div class="card-body">   
                                                        @if(count($hospital_coverage_default) > 0)
                                                            @php $i=-1; $end_hospital_default = 0; @endphp
                                                            @foreach ($hospital_coverage_default as $item)                                        
                                                                <ul class="list-group @if($i==-1) mt-3 @endif">
                                                                    <li class="list-group-item" key="{{$item['id']}}" number="{{$i}}" id="rest-point" style="cursor: pointer;">
                                                                        {{-- {{$item['name']}} <span class="float-right"> <i class="fas fa-map-marker-alt mr-2"></i> {{$item['distance']['distance'] ?? 'Unknown'}} dari Lokasi</span> --}}
                                                                        <div class="row">
                                                                            <div class="col-7">
                                                                                <span class="bold" style="font-size:20px;"> {{$item['name']}} </span> <p></p> <span> <i class="fas fa-map-marker-alt mr-2"></i> {{$item['distance']['distance'] ?? 'Unknown'}} dari Lokasi | Perkiraan waktu {{$item['time']['duration'] ?? 'Unknown'}}</span>
                                                                            </div>
                                                                            <div class="col-5">
                                                                                <button class="btn btn-light float-right"  style="height: 100%;">Pilih</button>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    
                                                                </ul>
                                                            @php $i++; $end_hospital_default++; @endphp
                                                            @endforeach
                                                        @else
                                                            <div class="alert alert-warning">
                                                                Tidak ada Hospital yang sesuai dengan Desa Laporan !
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col mt-4">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h6><i class="fas fa-hospital"></i> <span class="ml-2 bold">Rumah Sakit / Puskesmas Diluar dari Desa Laporan </span></h6>
                                                    </div>
                                                    <div class="card-body">
                                                        @if(count($hospital)>0)
                                                            <ul class="list-group mt-4">
                                                                @php if(count($hospital_coverage_default)>0) $i=$end_hospital_default; else $i=0 @endphp
                                                                @foreach($hospital as $item)
                                                                <li class="list-group-item" key="{{$item['id']}}" number="{{$i}}" id="rest-point" style="cursor: pointer;">
                                                                    {{-- {{$item['name']}} <span class="float-right"> <i class="fas fa-map-marker-alt mr-2"></i> {{$item['distance']['distance'] ?? 'Unknown'}} dari Lokasi</span> --}}
                                                                    <div class="row">
                                                                        <div class="col-7">
                                                                            <span class="bold" style="font-size:20px;"> {{$item['name']}} </span> <p></p> <span> <i class="fas fa-map-marker-alt mr-2"></i> {{$item['distance']['distance'] ?? 'Unknown'}} dari Lokasi | Perkiraan waktu {{$item['time']['duration'] ?? 'Unknown'}}</span>
                                                                        </div>
                                                                        <div class="col-5">
                                                                            <button class="btn btn-light float-right"  style="height: 100%;">Pilih</button>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @php $i++; @endphp
                                                                @endforeach
                                                            </ul>
                                                        @else
                                                            <div class="alert alert-warning">
                                                                Tidak ada Hospital diluar Desa Laporan !
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif                            
                                        @if($data['rescuer_id'] == null || $data['rest_id'] == null || isset($_GET['edit']))
                                        <div class="col mt-4">
                                            <button class="btn btn-success btn-block" id="process-laporan">
                                                <i class="fas fa-check"></i>
                                                <span class="ml-2">Proses Laporan @if($data['rescuer_id'] == null) dan Lanjut Ke Pemilihan Rumah Sakit atau Puskesmas @endif</span>
                                            </button>
                                        </div>
                                        @endif
                                    @endif
                                </div>
                                <div class="col-md-12 col-lg-8 d-none text-center" id="loading-process-laporan">
                                    <img src="https://i.pinimg.com/originals/78/e8/26/78e826ca1b9351214dfdd5e47f7e2024.gif" style="width: 500px; height:500px;" alt="" srcset=""><br>
                                    <h2>Memproses Laporan ...</h2>
                                </div>
                            @endif
                        @else            
                            <div class="col-md-12 col-lg-8" id="lanjut-laporan">  
                                <div class="col">
                                    <div class="alert alert-danger">
                                        <span class="">Laporan <b>PANIC BUTTON</b>, untuk memilih <b>POSKO</b> atau <b>RUMAH SAKIT</b> anda harus memilih jenis penyakit dan mengisi keterangan terlebih dahulu !</span>
                                    </div>
                                </div>      
                                <form action="{{url('laporan-completing')}}" method="POST">@csrf  
                                    <input type="hidden" name="id" value="{{$data['id']}}">      
                                    <div class="col mt-3">
                                        <h6><i class="fas fa-laptop-code"></i> <span class="ml-2 bold">Keterangan Laporan</span></h6>
                                        <p style="text-secondary">
                                            <textarea name="description" placeholder="Isi keterangan laporan disini ..." id="" cols="30" rows="10" class="form-control"></textarea>
                                        </p>
                                        <hr>
                                    </div>
                                    <div class="col mt-4">
                                        <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Jenis Laporan</span></h6>
                                        <p style="text-secondary"><h6>@if($data['report_type'] == 1) Diri Sendiri @elseif($data['report_type']==2) Orang Lain <p></p> <li> Nama : {{$data['other_people_name']}}</li> <li>Usia : {{$data['other_people_age']}}</li> @else Panic Button @endif</h6></p>
                                        <hr>
                                    </div>
                                    <div class="col mt-4">
                                        <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Jenis Penyakit</span></h6>
                                        <p style="text-secondary">
                                            <select name="diseases_id" id="" class="form-control">
                                                <option value="" hidden>Pilih Jenis Penyakit</option>
                                                @foreach ($diseases as $item)
                                                    <option value="{{$item['id']}}">{{$item['nama']}}</option>
                                                @endforeach
                                            </select>
                                        </p>
                                    </div>
                                    <div class="col mt-4">
                                        <button type="submit" class="btn btn-danger btn-block">
                                            Proses dan Lanjutkan Memilih Posko 
                                        </button>
                                    </div>
                                </form>
                            </div>
                        @endif
                    @if($data['rescuer_id'] != null && $data['rest_id'] != null && !isset($_GET['edit']))
                        <div class="col-md-12 col-lg-8">
                            <div class="alert alert-success">
                                <i class="fas fa-check"></i>
                                <span class="ml-2">Laporan ini telah ditindak lanjuti, terimakasih !</span>
                            </div>
                            <div class="col">
                                <h6><i class="fas fa-laptop-code"></i> <span class="ml-2 bold">Keterangan Laporan</span></h6>
                            <p style="text-secondary"><h6>{!!$data['description']!!}</h6></p>
                                <hr>
                            </div>
                            <div class="col mt-4">
                                <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Jenis Laporan</span></h6>
                                <p style="text-secondary"><h6>@if($data['report_type'] == 1) Diri Sendiri @elseif($data['report_type']==2) Orang Lain @else Panic Button @endif</h6></p>
                                <hr>
                            </div>
                            <div class="col mt-4">
                                <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Jenis Penyakit</span></h6>
                                <p style="text-secondary"><h6>{{$data['diseases_name']}}</h6></p>
                                <hr>
                            </div>
                            <div class="col mt-4">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="card text-white bg-danger mb-3">
                                            <div class="card-header bg-danger">
                                                <i class="fas fa-ambulance"></i> <span class="ml-2">Ambulance</span>
                                                <a href="{{url('laporan-detail/'.$data['id'].'?edit=true&posko=true')}}"><i class="fas fa-edit text-white float-right" style="cursor:pointer;"></i></a>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-white">{{$data['posko_name']}}</h6>
                                                <p class="card-text">{{$data['posko_name']}} telah diminta untuk menangani laporan ini !</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="card text-white bg-success mb-3">
                                            <div class="card-header bg-success">
                                                <i class="fas fa-building"></i> <span class="ml-2">Hospital</span>
                                                <a href="{{url('laporan-detail/'.$data['id'].'?edit=true&hospital=true')}}"><i class="fas fa-edit text-white float-right" style="cursor:pointer;"></i></a>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-white">{{$data['rest_name']}}</h6>
                                                <p class="card-text">{{$data['rest_name']}} telah diminta untuk menjadi lokasi rujukan laporan ini !</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col mt-4">
                                <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Progress Pelayanan</span></h6><p></p>
                                @if($data['admin_confirmed'] != null)
                                    <p style="text-secondary"><i class="far fa-circle" style="font-size:9px"></i><span class="ml-2">Dikonfirmasi oleh admin PSC</span> <span class="float-right">{{$data['admin_confirmed']}}</span></p>
                                @endif
                                @if($data['rescuer_confirmed'] != null)
                                    <p style="text-secondary"><i class="far fa-circle" style="font-size:9px"></i><span class="ml-2">Dikonfirmasi oleh pihak Posko</span> <span class="float-right">{{$data['rescuer_confirmed']}}</span></p>
                                @endif
                                @if($data['rescuer_otw'] != null)
                                    <p style="text-secondary"><i class="far fa-circle" style="font-size:9px"></i><span class="ml-2">Ambulan menuju lokasi</span> <span class="float-right">{{$data['rescuer_otw']}}</span></p>
                                @endif
                                @if($data['rescuer_arrived_people'] != null)
                                    <p style="text-secondary"><i class="far fa-circle" style="font-size:9px"></i><span class="ml-2">Ambulance menuju rumah sakit</span> <span class="float-right">{{$data['rescuer_arrived_people']}}</span></p>
                                @endif
                                @if($data['rescuer_arrived_hospital'] != null)
                                    <p style="text-secondary"><i class="far fa-circle" style="font-size:9px"></i><span class="ml-2">Ambulance tiba di rumah sakit</span> <span class="float-right">{{$data['rescuer_arrived_hospital']}}</span></p>
                                @endif
                                <hr>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    {{-- Modal --}}
    <div id="approve-laporan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form method="post" action="{{url('konfirmasi-pengaduan')}}">
                @csrf
                    <div class="modal-body">
                        <div class="px-3">
                            <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                                <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                                    <img class="navbar-brand-icon" src="{{url('assets/images/stack-logo-blue.svg')}}" width="25" alt="Stack">
                                    <span>Konfirmasi Pengaduan</span>
                                </a>
                            </div>
                            <input class="form-control @error('status_id') is-invalid @enderror" type="hidden" name="status_id" id="status_id" />
                            <div class="form-group">
                                <label for="select02">Status Pengaduan</label>
                                <select id="select02" data-toggle="select" data-minimum-results-for-search="-1" class="form-control @error('status_konfirmasi') is-invalid @enderror" name="status_konfirmasi">
                                    <option value="Diterima">Diterima</option>
                                    <option value="Ditolak">Ditolak</option>
                                </select>
                                @error('status_konfirmasi')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status_keterangan">Keterangan:</label>
                                <input class="form-control @error('status_keterangan') is-invalid @enderror" type="text" name="status_keterangan" id="status_keterangan" />
                                @error('status_keterangan')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group text-center">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>
                    </div> <!-- // END .modal-body -->
                </form>
            </div> <!-- // END .modal-content -->
        </div> <!-- // END .modal-dialog -->
    </div> <!-- // END .modal -->
@endsection

@section('script')
    <script>
        $(document).on('click', '.konfirmasi', function() {
            const id = $(this).attr('id_konfirmasi');
            console.log(id);
            $('#status_id').val(id);
        });

        // Key to be sent to API
        var rescuer_id = null, rest_id = null, id='{{$data['id']}}', people_uid='{{$data['people_uid']}}'
        // Rescuers
        let rescuers = document.querySelectorAll('#rescuers-point')
        $(rescuers).click(function(){
            $(this).addClass('active')
            rescuer_id = $(this).attr('key');
            for(let i=0; i<rescuers.length; i++){
                if($(this).attr('number') != $(rescuers[i]).attr('number'))
                    $(rescuers[i]).removeClass('active')
            }
        })

        // Rest Point
        let rest = document.querySelectorAll('#rest-point')
        $(rest).click(function(){
            $(this).addClass('active')
            rest_id = $(this).attr('key');
            for(let i=0; i<rest.length; i++){
                if($(this).attr('number') != $(rest[i]).attr('number'))
                    $(rest[i]).removeClass('active')
            }
        })

        // Tolak Laporan
        $('#button-tolak-laporan').click(function(){
            $('#lanjut-laporan').addClass('d-none')
            $('#tolak-laporan').removeClass('d-none')
        })
        $('#batal-tolak-laporan').click(function(){
            $('#tolak-laporan').addClass('d-none')
            $('#lanjut-laporan').removeClass('d-none')
        })

        // Processing Laporan
        $('#process-laporan').click(function(){
            if(rescuer_id != null || rest_id != null){
                $('#loading-process-laporan').removeClass('d-none')
                $('#lanjut-laporan').addClass('d-none')
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    url : '{{url('laporan-process')}}',
                    type: "POST",
                    data: {
                        id :  id,
                        rescuer_id :  rescuer_id,
                        rest_id :  rest_id,
                    },
                    success: function(response){
                        document.location.href = '{{url('laporan-detail')}}/{{$data['id']}}'
                    },
                });
                setTimeout(function(){ document.location.href = '{{url('laporan-detail')}}/{{$data['id']}}' }, 2000)

            }else{
                swal({
                    title: "Maaf!",
                    text: "Harus memilih Posko !",
                    icon: "warning",
                    button: "Aww yiss!",
                    });
            }
        })

        // Image Zoom
        $('img[data-enlargeable]').addClass('img-enlargeable').click(function(){
            var src = $(this).attr('src');
            alert(src)
            var modal;
            function removeModal(){ modal.remove(); $('body').off('keyup.modal-close'); }
            modal = $('<div>').css({
                // 'background' : 'url('+src+') no-repeat center',
                backgroundSize: 'contain',
                width:'100%', height:'100%',
                position:'fixed',
                zIndex:'10000',
                top:'0', left:'0',
                cursor: 'zoom-out',
            }).click(function(){
                removeModal();
            }).appendTo('body');
            //handling ESC
            $('body').on('keyup.modal-close', function(e){
                if(e.key==='Escape'){ removeModal(); } 
            });
        });

        document.querySelector('img.sample-image').addEventListener('click', function() {
            this.classList.toggle('sample-image-large');
        });
    </script>


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDb3hUocjT4p9z45iwS-uiHQF6UpUFpSVU"></script>
    <script>
        var locations = <?php print_r(json_encode($data)) ?>;

        var mymap = new GMaps({
            el: '#mymap',
            lat: 3.554514,
            lng: 98.879572,
            zoom:6
        });


        $.each( locations, function( index, value ){
            mymap.addMarker({
            lat: value.latitude,
            lng: value.longtitude,
            title: value.name,
            click: function(e) {
                alert('Rumah Sakit '+value.name);
            }
            });
        });

    </script>
    <script type="text/javascript">
        document.getElementById('reset').onclick= function() {
            var field1= document.getElementById('lng');
            var field2= document.getElementById('lat');
            field1.value= field1.defaultValue;
            field2.value= field2.defaultValue;
        };
    </script>

    <script type="text/javascript">
        function updateMarkerPosition(latLng) {
            document.getElementById('lat').value = [latLng.lat()];
            document.getElementById('lng').value = [latLng.lng()];
        }

        var myOptions = {
        zoom: 12,
            scaleControl: true,
        center:  new google.maps.LatLng({{$data['latitude']}},{{$data['longtitude']}}),
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };


        var map = new google.maps.Map(document.getElementById("mymap"),
            myOptions);

        var marker1 = new google.maps.Marker({
            position : new google.maps.LatLng({{$data['latitude']}},{{$data['longtitude']}}),
            title : 'lokasi',
            map : map,
            draggable : true
        });

        //updateMarkerPosition(latLng);

        google.maps.event.addListener(marker1, 'drag', function() {
            updateMarkerPosition(marker1.getPosition());
        });
    </script>
    <script type="text/javascript">
        document.getElementById('reset').onclick= function() {
            var field1= document.getElementById('lng');
            var field2= document.getElementById('lat');
            field1.value= field1.defaultValue;
            field2.value= field2.defaultValue;
        };
    </script>
    <script type="text/javascript">
        function updateMarkerPosition(latLng) {
            document.getElementById('lat').value = [latLng.lat()];
            document.getElementById('lng').value = [latLng.lng()];
        }

        var myOptions = {
        zoom: 12,
            scaleControl: true,
            center:  new google.maps.LatLng({{$data['latitude']}},{{$data['longtitude']}}),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };


        var map = new google.maps.Map(document.getElementById("map"),
            myOptions);

        var marker1 = new google.maps.Marker({
            position : new google.maps.LatLng({{$data['latitude']}},{{$data['longtitude']}}),
            title : 'lokasi',
            map : map,
            draggable : true
        });

        //updateMarkerPosition(latLng);

        google.maps.event.addListener(marker1, 'drag', function() {
            updateMarkerPosition(marker1.getPosition());
        });
    </script>
@endsection
