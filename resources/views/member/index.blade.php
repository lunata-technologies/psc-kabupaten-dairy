@extends('app-layout.master', [
    'mainMenuLink' => url('member/'),
    'mainMenu' => 'Member',
    'currentPage' => null,
])

@section('title', 'Member')

@section('content')
<div class="row p-2">
    <div class="col-12">
        <i class="fas fa-building"></i>
        <span class="ml-2">Daftar Member</span>
    </div>
</div>
<div class="row p-2">
    {{-- <div class="col-md-12 col-lg-4">
        <form action="" method="POST">@csrf
            <div class="card">
                <div class="card-header border-bottom">
                    <h5>Tambah Member Baru</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <label for="">Nama:</label>
                            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror">
                        </div>
                        @error('nama')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="row mt-2">
                        <div class="col-12">
                            <button type="submit" class="btn btn-info btn-block">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div> --}}
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="container">
                    <div class="table-responsive-lg table-responsive-md table-responsive-sm">
                        <table class="table table-striped table-bordered text-center" cellspacing="0" id="table_member" style="width: 100%" style="width: 100%">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Nik</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Handphone</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal --}}
<div id="update-penyakit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="post" action="{{url('update-penyakit')}}">
            @csrf
                <div class="modal-body">
                    <div class="px-3">
                        <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                            <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                                <img class="navbar-brand-icon" src="assets/images/stack-logo-blue.svg" width="25" alt="Stack">
                                <span>Data Penyakit</span>
                            </a>
                        </div>
                        <input class="form-control" type="hidden" name="update_id" id="update_id" />
                        <div class="form-group">
                            <label for="update_nama">Nama:</label>
                            <input class="form-control @error('update_nama') is-invalid @enderror" type="text" name="update_nama" id="update_nama" />
                            @error('update_nama')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </div> <!-- // END .modal-body -->
            </form>
        </div> <!-- // END .modal-content -->
    </div> <!-- // END .modal-dialog -->
</div> <!-- // END .modal -->
@endsection

@section('script')
    <script>
        $(function(){
            var table = $('#table_member').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: '{{ url('member-datatable') }}',
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'nama', name: 'nama'},
                    {data: 'nik', name: 'nik'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'alamat', name: 'alamat'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'},
                ],
                "language": {
                    "infoEmpty": "No records available",
                    "emptyTable": "No Data Available",
                },
                columnDefs:[
                    {
                        "targets" : [1,2,3,4,5],
                        "className": "text-left"
                    },
                ],
                "paging": true,
                "ordering": false,
                "info": true,
                "select": true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        });

        $(document).on('click', '#modal-tolak-member', function() {
            var uid = $(this).attr('uidMember');
            $('#uid_member').val(uid);
        });

        $(document).on('click', '#btn-update-detail', function() {
            const id = $(this).attr('id_penyakit');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "get",
                url: "detail-penyakit/" + id,
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    $("#update_id").val(data.id);
                    $("#update_nama").val(data.nama);
                },
                error: function(data) {
                    console.log('Error : ', data);
                }
            });
        });
    </script>
@endsection

@section('modal')
<div id="tolak-pengaduan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="post" action="{{url('update-member')}}">
            @csrf
                <div class="modal-body">
                    <div class="px-3">
                        <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                            <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                                <img class="navbar-brand-icon" src="assets/images/stack-logo-blue.svg" width="25" alt="Stack">
                                <span>Suspend Member</span>
                            </a>
                        </div>
                        <input class="form-control" type="hidden" name="uid_member" id="uid_member" />
                        <div class="form-group">
                            <label for="update_nama">Keterangan:</label>
                            <textarea class="form-control mt-2" name="keterangan" cols="30" rows="10"></textarea>
                            @error('keterangan')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
