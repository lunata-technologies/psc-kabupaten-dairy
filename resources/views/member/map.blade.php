@extends('app-layout.master', [
    'mainMenuLink' => url('maps/'),
    'mainMenu' => 'Map',
    'currentPage' => null,
])

@section('title', 'Map')

@section('content')
    <div id="mymap"></div>
    <input type="text" name="lat" id="lat">
    <input type="text" name="lng" id="lng">
@endsection

@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDb3hUocjT4p9z45iwS-uiHQF6UpUFpSVU"></script>
    <script>
        var locations = <?php print_r(json_encode($data)) ?>;

        var mymap = new GMaps({
            el: '#mymap',
            lat: 3.554514,
            lng: 98.879572,
            zoom:6
        });


        $.each( locations, function( index, value ){
            mymap.addMarker({
            lat: value.latitude,
            lng: value.longtitude,
            title: value.name,
            click: function(e) {
                alert('Rumah Sakit '+value.name);
            }
            });
        });

    </script>
    <script type="text/javascript">
        document.getElementById('reset').onclick= function() {
            var field1= document.getElementById('lng');
            var field2= document.getElementById('lat');
            field1.value= field1.defaultValue;
            field2.value= field2.defaultValue;
        };
    </script>

    <script type="text/javascript">
        function updateMarkerPosition(latLng) {
            document.getElementById('lat').value = [latLng.lat()];
            document.getElementById('lng').value = [latLng.lng()];
        }

        var myOptions = {
        zoom: 12,
            scaleControl: true,
        center:  new google.maps.LatLng(3.563838,98.878698),
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };


        var map = new google.maps.Map(document.getElementById("mymap"),
            myOptions);

        var marker1 = new google.maps.Marker({
            position : new google.maps.LatLng(3.563838,98.878698),
            title : 'lokasi',
            map : map,
            draggable : true
        });

        //updateMarkerPosition(latLng);

        google.maps.event.addListener(marker1, 'drag', function() {
            updateMarkerPosition(marker1.getPosition());
        });
    </script>
    <script type="text/javascript">
        document.getElementById('reset').onclick= function() {
            var field1= document.getElementById('lng');
            var field2= document.getElementById('lat');
            field1.value= field1.defaultValue;
            field2.value= field2.defaultValue;
        };
    </script>
    <script type="text/javascript">
        function updateMarkerPosition(latLng) {
            document.getElementById('lat').value = [latLng.lat()];
            document.getElementById('lng').value = [latLng.lng()];
        }

        var myOptions = {
        zoom: 12,
            scaleControl: true,
            center:  new google.maps.LatLng(3.637603,98.666044),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };


        var map = new google.maps.Map(document.getElementById("map"),
            myOptions);

        var marker1 = new google.maps.Marker({
            position : new google.maps.LatLng(3.637603,98.666044),
            title : 'lokasi',
            map : map,
            draggable : true
        });

        //updateMarkerPosition(latLng);

        google.maps.event.addListener(marker1, 'drag', function() {
            updateMarkerPosition(marker1.getPosition());
        });
    </script>
@endsection

