@extends('app-layout.master', [
    'mainMenuLink' => url('member/'),
    'mainMenu' => 'Member',
    'currentPage' => 'Member Detail',
])

@section('title', 'Member Detail')

@section('content')
<div class="row p-2">
    <div class="col-12">
        <i class="fas fa-user"></i>
        <span class="ml-2">Member Detail</span>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card pt-4">
            <div class="card-header bg-white">
                <h4>Informasi untuk <b class="bg-info text-white pl-3 pr-3">{{$data['name']}}</b></h4>
                <span>Anda bisa malakukan managemenisasi terhadap member ini !</span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-lg-4">
                        <div class="card" style="width: 18rem;">
                            <img src="{{$data['selfie']}}" style="height: 200px; width=100%;" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{$data['name']}}</h5>
                                <p class="card-text">{{$data['address']}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-8">                        
                        <div class="col mt-2">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation" id="informasi-button">
                                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home"  aria-selected="true">Informasi Pribadi</a>
                                </li>
                                <li class="nav-item" role="presentation" id="history-button">
                                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile"  aria-selected="false">History Pengaduan</a>
                                </li>
                              </ul>
                        </div>
                        <div class="col mt-2  " id="box-informasi">
                            <div class="row pt-4">
                                <div class="col-12 pb-4">
                                    <i class="alert alert-warning">
                                        <span class="text-secondary">
                                            <i class="fas fa-info"></i>
                                            <span class="ml-2">Hi, bisa saja informasi bersifat rahasia, mohon untuk dijaga !</span>
                                        </span>
                                    </i>
                                </div>
                                <div class="col-6 mt-3">
                                    <div class="col form-group">
                                        <i class="fas fa-envelope"></i>
                                        <span class="ml-2">{{$data['email'] ?? 'Tidak ada email' }}</span>
                                    </div>
                                </div>
                                <div class="col-6 mt-3">
                                    <div class="col form-group">
                                        <i class="fas fa-phone"></i>
                                        <span class="ml-2">{{$data['phone'] ?? 'Tidak ada nomor hp' }}</span>
                                    </div>
                                </div>
                                <div class="col-6 mt-3">
                                    <div class="col form-group">
                                        <i class="fas fa-building"></i>
                                        <span class="ml-2">{{$data['address'] ?? 'Tidak ada alamat' }}</span>
                                    </div>
                                </div>
                                <div class="col-6 mt-3">
                                    <div class="col form-group">
                                        <i class="fas fa-qrcode"></i>
                                        <span class="ml-2">{{$data['nik'] ?? 'Tidak ada NIK' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <div class="card">
                                        <img src="{{$data['selfie']}}" style="height: 200px; width=100%;" class="card-img-top" alt="...">
                                        <div class="card-body">
                                          <p class="card-text badge badge-info">Selfie</p>
                                        </div>
                                      </div>
                                </div>
                                <div class="col-6">
                                    <div class="card">
                                        <img src="{{$data['selfie-ktp']}}" style="height: 200px; width=100%;" class="card-img-top" alt="...">
                                        <div class="card-body">
                                          <p class="card-text badge badge-info">KTP</p>
                                        </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                        <div class="col mt-2 d-none" id="box-history">
                            @foreach ($report as $item)                                    
                            <div class="alert @if($item['rescuer_arrived_hospital'] != null ) alert-success @else alert-warning @endif" style="cursor: pointer;" onclick="document.location.href='{{url('laporan-detail/'.$item['id'])}}'">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-bullhorn"></i>
                                    </div>
                                    <div class="col-11 pl-0">
                                        {{$item['description']}} <br>
                                        <i>@if($item['report_type'] == 1) Laporan Diri Sendiri @elseif($item['report_type'] == 2) Laporan untuk orang lain @else Laporan Panic Button @endif</i>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $('#informasi-button').click(function(){
            $('#box-informasi').removeClass('d-none')
            $('#box-history').addClass('d-none')
        })
        $('#history-button').click(function(){
            $('#box-informasi').addClass('d-none')
            $('#box-history').removeClass('d-none')
        })
    </script>
@endsection