<form method="post" action="{{url('edit-pegawai-posko')}}">
    @csrf
        <div class="modal-body">
            <div class="px-3">
                <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                    <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                        {{-- <img class="navbar-brand-icon" src="{{asset('assets/images/stack-logo-blue.svg')}}" width="25" alt="Stack"> --}}
                        <span>Edit Shift</span>
                    </a>
                </div>
                <div class="row mt-3">
                    <div class="col-6">
                        <label for="">Tanggal</label>
                        <input id="flatpickrSample01" name="date" data-toggle="flatpickr" type="text" class="form-control" value="">
                    </div>
                    <div class="col-3">
                        <label for="">Shift Mulai</label>
                        <input id="flatpickrSample01" name="start" data-toggle="flatpickr" data-flatpickr-enable-time="true" data-flatpickr-no-calendar="true" data-flatpickr-alt-format="H:i" data-flatpickr-date-format="H:i" value="" type="text" class="form-control">
                    </div>
                    <div class="col-3">
                        <label for="">Shift Akhir</label>
                        <input type="text" name="end" data-toggle="flatpickr" data-flatpickr-enable-time="true" data-flatpickr-no-calendar="true" data-flatpickr-alt-format="H:i" data-flatpickr-date-format="H:i" value="" type="text" class="form-control">
                    </div>
                </div>
                <div class="form-group text-center">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </div>
        </div> <!-- // END .modal-body -->
    </form>