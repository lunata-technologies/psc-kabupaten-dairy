@extends('app-layout.master', [
    'mainMenuLink' => url('hospitals/'),
    'mainMenu' => 'Posko',
    'currentPage' => 'Posko Ambulance',
])

@section('title', 'Ambulan Spot')

@section('content')
<div class="row p-2">
    <div class="col-12">
        <i class="fas fa-ambulance"></i>
        <span class="ml-2">Atur Posko</span>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <div class="card">
                            <img src="https://www.vectorjunky.com/wp-content/uploads/2017/02/Pr%20122-%20TRI%20-%2025_02_11%20-%20006.jpg" class="card-img-top img-thumbnail" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{$data['name']}}</h5>
                                <p class="card-text">{{$data['address']}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12">
                        <div class="col">
                            <button class="btn btn-danger" data-toggle="modal" data-target="#tambah-pegawai">
                                <i class="fas fa-plus"></i>
                                <span class="ml-2">Tambah Pegawai</span>
                            </button>
                        </div>
                        <div class="col mt-2">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation" id="supir-button">
                                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home"  aria-selected="true">Supir</a>
                                </li>
                                <li class="nav-item" role="presentation" id="perawat-button">
                                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile"  aria-selected="false">Perawat</a>
                                </li>
                                <li class="nav-item" role="presentation" id="dokter-button">
                                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact"  aria-selected="false">Dokter</a>
                                </li>
                                <li class="nav-item" role="presentation" id="village-button">
                                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact"  aria-selected="false">Village Coverage Area</a>
                                </li>
                                <li class="nav-item" role="presentation" id="shift-button">
                                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact"  aria-selected="false">Shift Kerja</a>
                                </li>
                              </ul>
                        </div>
                        <div class="col mt-2">
                            <ul class="list-group " id="supir">
                                @if(count($employeeSupir)>0) 
                                    @foreach ($employeeSupir as $item)
                                            <li class="list-group-item position-relative">
                                                {{$item['name']}}
                                                <button class="btn btn-info float-right p-1" data-toggle="modal" data-target="#edit-pegawai" onclick="load('{{url('ajax-edit-pegawai-posko/'.$item['uid'])}}', 'modal-edit')">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </li>               
                                    @endforeach
                                @else
                                    <li class="list-group-item position-relative">
                                        <div class="alert alert-info">
                                            <i class="fas fa-warning"></i>
                                            <span class="ml-2">Data not found !</span>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                            <ul class="list-group d-none" id="perawat">
                                @if(count($employeePerawat)>0)          
                                    @foreach ($employeePerawat as $item)
                                            <li class="list-group-item position-relative">
                                                {{$item['name']}}
                                                <button class="btn btn-info float-right p-1" data-toggle="modal" data-target="#edit-pegawai" onclick="load('{{url('ajax-edit-pegawai-posko/'.$item['uid'])}}', 'modal-edit')">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </li>                 
                                    @endforeach
                                @else
                                    <li class="list-group-item position-relative">
                                        <div class="alert alert-info">
                                            <i class="fas fa-warning"></i>
                                            <span class="ml-2">Data not found !</span>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                            <ul class="list-group d-none" id="dokter">
                                @if(count($employeeDokter)>0)
                                    @foreach ($employeeDokter as $item)
                                            <li class="list-group-item position-relative">
                                                {{$item['name']}}
                                                <button class="btn btn-info float-right p-1" data-toggle="modal" data-target="#edit-pegawai" onclick="load('{{url('ajax-edit-pegawai-posko/'.$item['uid'])}}', 'modal-edit')">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </li>                 
                                    @endforeach
                                @else
                                    <li class="list-group-item position-relative">
                                        <div class="alert alert-info">
                                            <i class="fas fa-warning"></i>
                                            <span class="ml-2">Data not found !</span>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                            <ul class="list-group d-none" id="box-village">
                                <li class="pb-4" style="list-style: none"><button class="btn btn-danger float-right" data-toggle="modal" data-target="#tambah-desa">Add new</button></li>
                                @foreach ($poskoVillageCoverArea as $item)
                                    <li class="list-group-item position-relative">{{$item['village_name']}} <span class="float-right"><i class="fas text-danger fa-trash" style="cursor: pointer"></i></span></li>
                                @endforeach
                            </ul>
                            <ul class="list-group d-none" id="box-shift">
                                <form action="{{url('posko-shift')}}" method="POST">@csrf
                                    <input type="hidden" name="rest_id" value="{{$data['id']}}">
                                    <li class="pb-4" style="list-style: none">
                                        <div class="row mt-3">
                                            <div class="col-6">
                                                <label for="">Tanggal</label>
                                                <input id="flatpickrSample01" name="date" data-toggle="flatpickr" type="text" class="form-control" value="{{$today}}">
                                            </div>
                                            <div class="col-3">
                                                <label for="">Shift Mulai</label>
                                                <input id="flatpickrSample01" name="start" data-toggle="flatpickr" data-flatpickr-enable-time="true" data-flatpickr-no-calendar="true" data-flatpickr-alt-format="H:i" data-flatpickr-date-format="H:i" value="{{$time}}" type="text" class="form-control">
                                            </div>
                                            <div class="col-3">
                                                <label for="">Shift Akhir</label>
                                                <input type="text" name="end" data-toggle="flatpickr" data-flatpickr-enable-time="true" data-flatpickr-no-calendar="true" data-flatpickr-alt-format="H:i" data-flatpickr-date-format="H:i" value="{{$time}}" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-12">
                                                @foreach ($employee as $item)                                                
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                            <input type="checkbox" name="employee[]" value="{{$item['uid']}}" aria-label="Checkbox for following text input">
                                                            </div>
                                                        </div>
                                                        <input type="text" value="{{$item['name']}}" disabled class="form-control" aria-label="Text input with checkbox">
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-danger float-right">Tambah Shift</button>
                                            </div>
                                        </div>

                                        <div class="row mt-4 p-2">
                                            @if(count($shiftViewButton) != 0)
                                                <div class="col-12 mt-4">
                                                    <div class="alert alert-info">
                                                        <i class="fas fa-exclamation"></i>
                                                        <span class="ml-2">Menampilkan data shift yang telah dibuat, silahkan klik Tombol dibawah ini !</span>
                                                    </div>
                                                </div>
                                            @endif
                                            <section class="row p-4">
                                            @foreach($shiftViewButton as $item)
                                                <div class="col-3 p-0">
                                                    {{-- <div class="col"> --}}
                                                        <button type="button" number="{{$loop->iteration}}" id="button-shift-date" class="btn btn-block btn-light">{{$item}}</button>
                                                    {{-- </div> --}}
                                                </div>
                                            @endforeach
                                            </section>
                                            @foreach ($shiftViewButton as $item)                                                
                                                <div class="col-12 mt-4 d-none" id="box-shift-view" number="{{$loop->iteration}}">
                                                    <table class="table table-striped">
                                                        <thead class="thead-light">
                                                            <tr class="text-center">
                                                                <th>Nama</th>
                                                                <th>Tanggal</th>
                                                                <th>Jam Mulai</th>
                                                                <th>Jam Selesai</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($shift as $row)
                                                                @if($row['date_name'] == $item)
                                                                    <tr class="text-center">
                                                                        <td>{{$row['name']}}</td>
                                                                        <td>{{$row['date_name']}}</td>
                                                                        <td>{{$row['start']}}</td>
                                                                        <td>{{$row['end']}}</td>
                                                                        <td class="text-center"><a href="{{url('delete-shift/'.$row['id'].'/'.'posko')}}" class="btn btn-danger p-1" ><i class="fas fa-trash text-white"  style="cursor: pointer"></i></a></td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            @endforeach
                                        </div>
                                    </li>
                                </form>
                            </ul>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('modal')
{{-- Modal --}}
<div id="tambah-pegawai" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="post" action="">
            @csrf
                <div class="modal-body">
                    <div class="px-3">
                        <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                            <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                                <img class="navbar-brand-icon" src="{{asset('assets/images/stack-logo-blue.svg')}}" width="25" alt="Stack">
                                <span>Posko</span>
                            </a>
                        </div>
                        <div class="form-group">
                            <label for="update_nama">Nama:</label>
                            <input class="form-control" type="text" name="name" required id="update_nama" />
                        </div>
                        <div class="form-group">
                            <label for="update_nama">Username:</label>
                            <input class="form-control" type="text" name="username" required id="update_nama" />
                        </div>
                        <div class="form-group">
                            <label for="update_alamat">Password:</label>
                            <input class="form-control" type="text" name="password" required id="update_alamat" />
                        </div>
                        <div class="form-group">
                            <label for="update_alamat">No Hp:</label>
                            <input class="form-control" type="number" name="phone" required id="update_alamat" />
                        </div>
                        <div class="form-group">
                            <label for="">Jabatan</label>
                            <select name="type" id="" class="form-control">
                                <option value="" hidden>Pilih Jabatan</option>
                                <option value="1">Dokter</option>
                                <option value="2">Perawat</option>
                                <option value="3">Sopir Ambulance</option>
                            </select>
                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </div> <!-- // END .modal-body -->
            </form>
        </div> <!-- // END .modal-content -->
    </div> <!-- // END .modal-dialog -->
</div> <!-- // END .modal -->

{{-- Modal --}}
<div id="edit-pegawai" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content" id="modal-edit">
            
        </div> <!-- // END .modal-content -->
    </div>
</div> <!-- // END .modal -->

{{-- Modal --}}
<div id="edit-shift" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content" id="modal-edit-shift">
            
        </div> <!-- // END .modal-content -->
    </div>
</div> <!-- // END .modal -->

{{-- Modal --}}
<div id="tambah-desa" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="post" action="{{url('posko-desa-cover')}}">
            @csrf
                <div class="modal-body">
                    <div class="px-3">
                        <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                            <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                                <img class="navbar-brand-icon" src="{{asset('assets/images/stack-logo-blue.svg')}}" width="25" alt="Stack">
                                <span>Tambah Area Cakupan Desa</span>
                            </a>
                        </div>
                        <input type="hidden" name="id" value="{{$data['id']}}">
                        <div class="form-group">
                            <select name="subdistrict_id" id="pilih_kecamatan" class="form-control">
                                <option value="" hidden>Pilih Kecamatan</option>
                                @foreach ($subdistrict as $item)
                                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="village_id"  class="form-control" id="pilih-desa">
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-1">
                                    <input type="checkbox" name="all" value="1">
                                </div>
                                <div class="col-11 pl-0"><span>Pilih semua desa di kecamatan yang anda pilih</span></div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </div> <!-- // END .modal-body -->
            </form>
        </div> <!-- // END .modal-content -->
    </div> <!-- // END .modal-dialog -->
</div> <!-- // END .modal -->
@endsection

@section('script')
    <script>
        $('#perawat-button').click(function(){
            $('#dokter').addClass('d-none')
            $('#supir').addClass('d-none')
            $('#box-village').addClass('d-none')
            $('#perawat').removeClass('d-none')
            $('#box-shift').addClass('d-none')
        })
        $('#dokter-button').click(function(){
            $('#dokter').removeClass('d-none')
            $('#supir').addClass('d-none')
            $('#box-village').addClass('d-none')
            $('#perawat').addClass('d-none')
            $('#box-shift').addClass('d-none')
        })
        $('#supir-button').click(function(){
            $('#dokter').addClass('d-none')
            $('#supir').removeClass('d-none')
            $('#box-village').addClass('d-none')
            $('#perawat').addClass('d-none')
            $('#box-shift').addClass('d-none')
        })
        $('#village-button').click(function(){
            $('#dokter').addClass('d-none')
            $('#supir').addClass('d-none')
            $('#box-village').removeClass('d-none')
            $('#perawat').addClass('d-none')
            $('#box-shift').addClass('d-none')
        })
        $('#shift-button').click(function(){
            $('#dokter').addClass('d-none')
            $('#supir').addClass('d-none')
            $('#box-village').addClass('d-none')
            $('#perawat').addClass('d-none')
            $('#box-shift').removeClass('d-none')
        })


        // Ajax Get Desa
        $('#pilih_kecamatan').on('change', function(){            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }, 
                url: '{{url('village-subdistrict')}}',
                type: 'POST',
                data: {
                    subdistrict_id : $(this).val()
                },
                success: function(response){
                    $('#pilih-desa').html(response)
                }
            })
        })
    
        // View Shift
        var shiftViewBtn = document.querySelectorAll('#button-shift-date')
        var shiftView = document.querySelectorAll('#box-shift-view')
        $(shiftViewBtn).click(function(){
            $(this).addClass('active')
            for(var i=0; i<shiftViewBtn.length; i++){
                if($(shiftView[i]).attr('number') == $(this).attr('number')){
                    $(shiftView[i]).removeClass('d-none')
                }else{
                    $(shiftView[i]).addClass('d-none')
                }
                if($(this).attr('number') != $(shiftViewBtn[i]).attr('number'))
                    $(shiftViewBtn[i]).removeClass('active')
            }
        })

    </script>
@endsection