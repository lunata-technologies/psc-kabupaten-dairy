<form method="post" action="">
    @csrf
        <div class="modal-body">
            <div class="px-3">
                <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                    <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                        <img class="navbar-brand-icon" src="{{asset('assets/images/stack-logo-blue.svg')}}" width="25" alt="Stack">
                        <span>Edit Pegawai Posko</span>
                    </a>
                </div>
                <div class="form-group">
                    <label for="update_nama">Nama:</label>
                    <input class="form-control" type="text" value="{{$data['name']}}" name="name" required id="update_nama" />
                </div>
                <div class="form-group">
                    <label for="update_nama">Username:</label>
                    <input class="form-control" type="text" value="{{$data['username']}}" name="username" required id="update_nama" />
                </div>
                <div class="form-group">
                    <label for="update_alamat">Password:</label>
                    <input class="form-control" type="text" value="{{$data['password']}}" name="password" required id="update_alamat" />
                </div>
                <div class="form-group">
                    <label for="update_alamat">No Hp:</label>
                    <input class="form-control" type="number" name="phone" value="{{$data['phone']}}" required id="update_alamat" />
                </div>
                <div class="form-group">
                    <label for="">Jabatan</label>
                    <select name="type" id="" class="form-control">
                        <option value="" hidden>Pilih Jabatan</option>
                        <option value="1" @if($data['type'] == 1) selected @endif>Dokter</option>
                        <option value="2" @if($data['type'] == 2) selected @endif>Perawat</option>
                        <option value="3" @if($data['type'] == 3) selected @endif>Sopir Ambulance</option>
                    </select>
                </div>
                <div class="form-group text-center">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </div>
        </div> <!-- // END .modal-body -->
    </form>