@extends('app-layout.master', [
    'mainMenuLink' => url('hospitals/'),
    'mainMenu' => 'Puskesmas',
    'currentPage' => null,
])

@section('title', 'Puskesmas')

@section('content')
<div class="row p-2">
    <div class="col-12">
        <i class="fas fa-building"></i>
        <span class="ml-2">Data Puskesmas</span>
    </div>
</div>
<div class="row p-2">
    <div class="col-md-12 col-lg-4 need-auth">
        <form action="" method="POST">@csrf
            <div class="card">
                <div class="card-header border-bottom">
                    <h5>Tambah Puskesmas Baru</h5>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror">
                        @error('nama')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Address</label>
                        <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror" cols="30" rows="10" placeholder=""></textarea>
                        @error('alamat')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Latitude</label>
                        <input type="text" name="latitude" class="form-control @error('latitude') is-invalid @enderror">
                        @error('latitude')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Longitude</label>
                        <input type="text" name="longitude" class="form-control @error('longitude') is-invalid @enderror">
                        @error('longitude')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="row mt-2">
                        <div class="col-12">
                            <button type="submit" class="btn btn-info btn-block">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12 col-lg-8">
        <div class="card">
            <div class="card-body">
                <div class="container">
                    <div class="table-responsive-lg table-responsive-md table-responsive-sm">
                        <table class="table table-striped table-bordered text-center" cellspacing="0" id="table_puskesmas" style="width: 100%" style="width: 100%">
                            <thead>
                                <tr>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal --}}
<div id="update-puskesmas" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="post" action="{{url('update-puskesmas')}}">
            @csrf
                <div class="modal-body">
                    <div class="px-3">
                        <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                            <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                                <img class="navbar-brand-icon" src="assets/images/stack-logo-blue.svg" width="25" alt="Stack">
                                <span>Puskesmas</span>
                            </a>
                        </div>
                        <input class="form-control" type="hidden" name="update_id" id="update_id" />
                        <div class="form-group">
                            <label for="update_nama">Nama:</label>
                            <input class="form-control @error('update_nama') is-invalid @enderror" type="text" name="update_nama" id="update_nama" />
                            @error('update_nama')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="update_alamat">Alamat:</label>
                            <input class="form-control @error('update_nama') is-invalid @enderror" type="text" name="update_alamat" id="update_alamat" />
                            @error('update_alamat')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="update_latitude">Latitude</label>
                            <input type="text" name="update_latitude" id="update_latitude" class="form-control @error('update_latitude') is-invalid @enderror">
                            @error('update_latitude')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Longitude</label>
                            <input type="text" name="update_longitude" id="update_longitude" class="form-control @error('update_longitude') is-invalid @enderror">
                            @error('update_longitude')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </div> <!-- // END .modal-body -->
            </form>
        </div> <!-- // END .modal-content -->
    </div> <!-- // END .modal-dialog -->
</div> <!-- // END .modal -->
@endsection

@section('script')
    <script>
        $(function(){
            var table = $('#table_puskesmas').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: '{{ url('puskesmas-datatable') }}',
                columns: [
                    {data: 'nama', name: 'nama'},
                    {data: 'alamat', name: 'alamat'},
                    {data: 'action', name: 'action'}
                ],
                "language": {
                    "infoEmpty": "No records available",
                    "emptyTable": "No Data Available",
                },
                columnDefs:[
                    {
                        "targets" : [0],
                        "className": "text-left"
                    },
                ],
                "paging": true,
                "ordering": false,
                "info": true,
                "select": true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        });



        $(document).on('click', '#btn-update-detail', function() {
            const id = $(this).attr('id_puskesmas');
            console.log(id);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "get",
                url: "update-detail-puskesmas/" + id,
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    $("#update_id").val(data.id);
                    $("#update_nama").val(data.name);
                    $("#update_alamat").val(data.address);
                    $("#update_latitude").val(data.latitude);
                    $("#update_longitude").val(data.longtitude);
                },
                error: function(data) {
                    console.log('Error : ', data);
                }
            });
        });
    </script>
@endsection
