@extends('app-layout.master', [
    'mainMenuLink' => url('hospitals/'),
    'mainMenu' => 'Puskesmas',
    'currentPage' => 'Puskesmas Detail',
])

@section('title', 'Puskesmas Detail')

@section('content')
<div class="row p-2">
    <div class="col-12">
        <i class="fas fa-ambulance"></i>
        <span class="ml-2">Atur Puskesmas</span>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <div class="card">
                            <img src="https://www.crushpixel.com/static16/preview2/hospital-rgb-color-icon-2264293.jpg" class="card-img-top img-thumbnail" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{$data['name']}}</h5>
                                <p class="card-text">{{$data['address']}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12">
                        <div class="col">
                            <button class="btn btn-danger" data-toggle="modal" data-target="#tambah-pegawai">
                                <i class="fas fa-plus"></i>
                                <span class="ml-2">Tambah Pegawai</span>
                            </button>
                        </div>
                        <div class="col mt-2">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation" id="supir-button">
                                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home"  aria-selected="true">Supir</a>
                                </li>
                                <li class="nav-item" role="presentation" id="perawat-button">
                                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile"  aria-selected="false">Perawat</a>
                                </li>
                                <li class="nav-item" role="presentation" id="dokter-button">
                                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact"  aria-selected="false">Dokter</a>
                                </li>
                                <li class="nav-item" role="presentation" id="village-button">
                                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact"  aria-selected="false">Village Coverage Area</a>
                                </li>
                              </ul>
                        </div>
                        <div class="col mt-2">
                            <ul class="list-group" id="supir">
                                @if(count($employeeSupir)>0) 
                                    @foreach ($employeeSupir as $item)
                                            <li class="list-group-item position-relative">
                                                {{$item['name']}}
                                                <button class="btn btn-info float-right p-1" data-toggle="modal" data-target="#edit-pegawai" onclick="load('{{url('ajax-edit-pegawai-publichealth/'.$item['uid'])}}', 'modal-edit')">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </li>               
                                    @endforeach
                                @else
                                    <li class="list-group-item position-relative">
                                        <div class="alert alert-info">
                                            <i class="fas fa-warning"></i>
                                            <span class="ml-2">Data not found !</span>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                            <ul class="list-group d-none" id="perawat">
                                @if(count($employeePerawat)>0)          
                                    @foreach ($employeePerawat as $item)
                                            <li class="list-group-item position-relative">
                                                {{$item['name']}}
                                                <button class="btn btn-info float-right p-1"  data-toggle="modal" data-target="#edit-pegawai" onclick="load('{{url('ajax-edit-pegawai-publichealth/'.$item['uid'])}}', 'modal-edit')">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </li>                 
                                    @endforeach
                                @else
                                    <li class="list-group-item position-relative">
                                        <div class="alert alert-info">
                                            <i class="fas fa-warning"></i>
                                            <span class="ml-2">Data not found !</span>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                            <ul class="list-group d-none" id="dokter">
                                @if(count($employeeDokter)>0)
                                    @foreach ($employeeDokter as $item)
                                            <li class="list-group-item position-relative">
                                                {{$item['name']}}
                                                <button class="btn btn-info float-right p-1" data-toggle="modal" data-target="#edit-pegawai" onclick="load('{{url('ajax-edit-pegawai-publichealth/'.$item['uid'])}}', 'modal-edit')">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </li>                 
                                    @endforeach
                                @else
                                    <li class="list-group-item position-relative">
                                        <div class="alert alert-info">
                                            <i class="fas fa-warning"></i>
                                            <span class="ml-2">Data not found !</span>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                            <ul class="list-group d-none" id="box-village">
                                <li class="pb-4" style="list-style: none"><button class="btn btn-danger float-right" data-toggle="modal" data-target="#tambah-desa">Add new</button></li>
                                @foreach ($poskoVillageCoverArea as $item)
                                    <li class="list-group-item position-relative">{{$item['village_name']}} <span class="float-right"><i class="fas text-danger fa-trash" style="cursor: pointer"></i></span></li>
                                @endforeach
                            </ul>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('modal')
{{-- Modal --}}
<div id="tambah-pegawai" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="post" action="">
            @csrf
                <div class="modal-body">
                    <div class="px-3">
                        <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                            <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                                <img class="navbar-brand-icon" src="{{asset('assets/images/stack-logo-blue.svg')}}" width="25" alt="Stack">
                                <span>Posko</span>
                            </a>
                        </div>
                        <div class="form-group">
                            <label for="update_nama">Nama:</label>
                            <input class="form-control" type="text" name="name" required id="update_nama" />
                        </div>
                        <div class="form-group">
                            <label for="update_nama">Username:</label>
                            <input class="form-control" type="text" name="username" required id="update_nama" />
                        </div>
                        <div class="form-group">
                            <label for="update_alamat">Password:</label>
                            <input class="form-control" type="text" name="password" required id="update_alamat" />
                        </div>
                        <div class="form-group">
                            <label for="update_alamat">No Hp:</label>
                            <input class="form-control" type="number" name="phone" required id="update_alamat" />
                        </div>
                        <div class="form-group">
                            <label for="">Jabatan</label>
                            <select name="type" id="" class="form-control">
                                <option value="" hidden>Pilih Jabatan</option>
                                <option value="1">Dokter</option>
                                <option value="2">Perawat</option>
                                <option value="3">Sopir Ambulance</option>
                            </select>
                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </div> <!-- // END .modal-body -->
            </form>
        </div> <!-- // END .modal-content -->
    </div> <!-- // END .modal-dialog -->
</div> <!-- // END .modal -->

{{-- Modal --}}
<div id="edit-pegawai" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content" id="modal-edit">
            
        </div> <!-- // END .modal-content -->
    </div>
</div> <!-- // END .modal -->

{{-- Modal --}}
<div id="tambah-desa" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="post" action="{{url('puskesmas-desa-cover')}}">
            @csrf
                <div class="modal-body">
                    <div class="px-3">
                        <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                            <a href="fixed-dashboard.html" class="navbar-brand" style="min-width: 0">
                                <img class="navbar-brand-icon" src="{{asset('assets/images/stack-logo-blue.svg')}}" width="25" alt="Stack">
                                <span>Tambah Area Cakupan Desa</span>
                            </a>
                        </div>
                        <input type="hidden" name="id" value="{{$data['id']}}">
                        <div class="form-group">
                            <select name="subdistrict_id" id="pilih_kecamatan" class="form-control">
                                <option value="" hidden>Pilih Kecamatan</option>
                                @foreach ($subdistrict as $item)
                                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="village_id"  class="form-control" id="pilih-desa">
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-1">
                                    <input type="checkbox" name="all" value="1">
                                </div>
                                <div class="col-11 pl-0"><span>Pilih semua desa di kecamatan yang anda pilih</span></div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </div> <!-- // END .modal-body -->
            </form>
        </div> <!-- // END .modal-content -->
    </div> <!-- // END .modal-dialog -->
</div> <!-- // END .modal -->
@endsection

@section('script')
    <script>
        $('#perawat-button').click(function(){
            $('#dokter').addClass('d-none')
            $('#supir').addClass('d-none')
            $('#box-village').addClass('d-none')
            $('#perawat').removeClass('d-none')
        })
        $('#dokter-button').click(function(){
            $('#dokter').removeClass('d-none')
            $('#supir').addClass('d-none')
            $('#box-village').addClass('d-none')
            $('#perawat').addClass('d-none')
        })
        $('#supir-button').click(function(){
            $('#dokter').addClass('d-none')
            $('#supir').removeClass('d-none')
            $('#box-village').addClass('d-none')
            $('#perawat').addClass('d-none')
        })
        $('#village-button').click(function(){
            $('#dokter').addClass('d-none')
            $('#supir').addClass('d-none')
            $('#box-village').removeClass('d-none')
            $('#perawat').addClass('d-none')
        })


        // Ajax Get Desa
        $('#pilih_kecamatan').on('change', function(){            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }, 
                url: '{{url('village-subdistrict')}}',
                type: 'POST',
                data: {
                    subdistrict_id : $(this).val()
                },
                success: function(response){
                    $('#pilih-desa').html(response)
                }
            })
        })
    </script>
@endsection