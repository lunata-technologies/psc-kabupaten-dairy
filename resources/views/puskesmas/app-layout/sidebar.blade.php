{{-- <div class="mdk-drawer__content">
    <div class="sidebar sidebar-light sidebar-left simplebar" data-simplebar> --}}
        {{-- <div class="d-flex align-items-center sidebar-p-a border-bottom sidebar-account">
            <a href="profile.html" class="flex d-flex align-items-center text-underline-0 text-body">
                <span class="avatar mr-3">
                    <img src="assets/images/avatar/demi.png" alt="avatar" class="avatar-img rounded-circle">
                </span>
                <span class="flex d-flex flex-column">
                    <strong>{{Auth::user()->name}}</strong>
                    <small class="text-muted text-uppercase">{{Auth::user()->email}}</small>
                </span>
            </a>
            <div class="dropdown ml-auto">
                <a href="#" data-toggle="dropdown" data-caret="false" class="text-muted"><i class="material-icons">more_vert</i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-item-text dropdown-item-text--lh">
                        <div><strong>{{Auth::user()->name}}</strong></div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item active" href="index.html">Dashboard</a>
                    <a class="dropdown-item" href="profile.html">My profile</a>
                    <a class="dropdown-item" href="edit-account.html">Edit account</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="login.html">Logout</a>
                </div>
            </div>
        </div> --}}
        <div class="sidebar-heading sidebar-m-t">Menu</div>
        <ul class="sidebar-menu">
            <li class="sidebar-menu-item @if($mainMenu == 'Dashboard') active open @endif ">
                <a class="sidebar-menu-button" href="{{url('puskesmas')}}">
                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">dvr</i>
                    <span class="sidebar-menu-text">Dashboards</span>
                    {{-- <span class="ml-auto sidebar-menu-toggle-icon"></span> --}}
                </a>
            </li>
            <li class="sidebar-menu-item @if($mainMenu == 'Pegawai') active open @endif ">
                <a class="sidebar-menu-button" href="{{url('puskesmas/pegawai')}}">
                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">local_hospital</i>
                    <span class="sidebar-menu-text">Hospitals</span>
                    {{-- <span class="ml-auto sidebar-menu-toggle-icon"></span> --}}
                </a>
            </li>

            {{-- <li class="sidebar-menu-item @if($mainMenu == 'Territory') active open @endif" data-toggle="collapse" href="#apps_menu">
                <a class="sidebar-menu-button" data-toggle="collapse" href="#apps_menu">
                    <i class="sidebar-menu-icon sidebar-menu-icon--left fa fa-map-marked"></i>
                    <span class="sidebar-menu-text">Apps</span>
                    <span class="ml-auto sidebar-menu-toggle-icon"></span>
                </a>
                <ul class="sidebar-submenu collapse" id="apps_menu">
                    <li class="sidebar-menu-item @if($currentPage == 'Sub District') active @endif">
                        <a class="sidebar-menu-button" href="{{'sub-district'}}">
                            <span class="sidebar-menu-text">Sub-District</span>
                        </a>
                    </li>
                    <li class="sidebar-menu-item @if($currentPage == 'Village Area') active @endif ">
                        <a class="sidebar-menu-button" href="{{url('village')}}">
                            <span class="sidebar-menu-text">Village Area</span>
                        </a>
                    </li>
                    <li class="sidebar-menu-item">
                        <a class="sidebar-menu-button" href="app-fullcalendar.html">
                            <span class="sidebar-menu-text">Event Calendar</span>
                        </a>
                    </li>
                </ul>
            </li> --}}
        </ul>
    {{-- </div>
</div> --}}
