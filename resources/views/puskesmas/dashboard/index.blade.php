@extends('puskesmas.app-layout.master', [
    'mainMenuLink' => url('puskesmas'),
    'mainMenu' => 'Dashboard',
    'currentPage' => null,
])

@section('title', 'Dashboard')

@section('content')
<div class="container-fluid">
    <div class="row second-chart-list third-news-update">
    <div class="col-xl-12 col-lg-12 xl-50 morning-sec box-col-12">
        <div class="card">
            <div class="card-body">

                {{-- <div class="media">
                    <div class="badge-groups w-100">
                        <div class="badge f-12"><i class="mr-1" data-feather="clock"></i><span id="txt"></span></div>
                        <div class="badge f-12"><i class="fa fa-spin fa-cog f-14"></i></div>
                    </div>
                </div>
                <div class="greeting-user text-center">
                    <div class="profile-vector"><img class="img-fluid" src="../assets/images/dashboard/welcome.png" alt=""></div>
                    <h4 class="f-w-600"><span id="greeting">Good Morning</span> <span class="right-circle"><i class="fa fa-check-circle f-14 middle"></i></span></h4>
                    <p><span> You have done 57.6% more sales today. Check your new badge in your profile.</span></p>
                    <div class="whatsnew-btn"><a class="btn btn-primary">Whats New !</a></div>
                    <div class="left-icon"><i class="fa fa-bell"> </i></div>
                </div> --}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
