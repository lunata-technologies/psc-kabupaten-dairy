@extends('puskesmas.app-layout.master', [
    'mainMenuLink' => url('puskesmas/pegawai'),
    'mainMenu' => 'Pegawai',
    'currentPage' => null,
])

@section('title', 'Pegawai')

@section('content')
<div class="container-fluid">
    <div class="row second-chart-list third-news-update">
        <div class="col-xl-12 col-lg-12 xl-50 morning-sec box-col-12">
            <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>
                <table class="table mb-0 thead-border-top-0">
                    <thead>
                        <tr>
                            <th>Employee</th>
                            <th>Status</th>
                            <th>Last Activity</th>
                            <th>Earnings</th>
                        </tr>
                    </thead>
                    <tbody class="list" id="staff">

                        <tr>
                            <td>
                                <div class="media align-items-center">
                                    <div class="avatar avatar-xs mr-2">
                                        <img src="assets/images/256_luke-porter-261779-unsplash.jpg" alt="Avatar" class="avatar-img rounded-circle">
                                    </div>
                                    <div class="media-body">
                                        <span class="js-lists-values-employee-name">Michael Smith</span>
                                    </div>
                                </div>
                            </td>

                            <td><span class="badge badge-warning">ADMIN</span></td>
                            <td><small class="text-muted">3 days ago</small></td>
                            <td>&dollar;12,402</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

{{-- modal --}}

    <div id="modal-standard" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-standard-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="#">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-standard-title">Tambah Pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input id="nama" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input id="email" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input id="password" type="password" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
