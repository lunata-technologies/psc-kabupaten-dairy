
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- Simplebar -->
    <link type="text/css" href="{{asset('assets/vendor/simplebar.min.css')}}" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{asset('assets/css/app.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('assets/css/app.rtl.css')}}" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="{{asset('assets/css/vendor-material-icons.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('assets/css/vendor-material-icons.rtl.css')}}" rel="stylesheet">

    <!-- Font Awesome FREE Icons -->
    <link type="text/css" href="{{asset('assets/css/vendor-fontawesome-free.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('assets/css/vendor-fontawesome-free.rtl.css')}}" rel="stylesheet">

    {{-- datatables --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <style>
        .modal-backdrop.show{
            display: none !important;
        }
        table.dataTable td.focus {
            outline: 1px solid #ac1212;
            outline-offset: -3px;
            background-color: #f8e6e6 !important;
        }
    </style>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133433427-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-133433427-1');
</script>


    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '327167911228268');
  fbq('track', 'PageView');
</script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=327167911228268&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->



    <!-- Flatpickr -->
    <link type="text/css" href="{{asset('assets/css/vendor-flatpickr.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('assets/css/vendor-flatpickr.rtl.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('assets/css/vendor-flatpickr-airbnb.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('assets/css/vendor-flatpickr-airbnb.rtl.css')}}" rel="stylesheet">

    <!-- Vector Maps -->
    <link type="text/css" href="{{asset('assets/vendor/jqvmap/jqvmap.min.css')}}" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        function confirm_me(message,link){
            swal({
                title: "Are you sure?",
                text: message,
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                    swal("Poof! Your imaginary file has been deleted!", {
                    icon: "success",
                    });
                    document.location.href = link;

                }
            });
        }
    </script>





</head>

<body class="layout-default">













    <div class="preloader"></div>

    <!-- Header Layout -->
    <div class="mdk-header-layout js-mdk-header-layout">

        <!-- Header -->

        <div id="header" class="mdk-header js-mdk-header m-0" data-fixed>
            <div class="mdk-header__content">

                <div class="navbar navbar-expand-sm navbar-main navbar-dark bg-danger  pr-0" id="navbar" data-primary>
                    <div class="container-fluid p-0">

                        <!-- Navbar toggler -->

                        <button class="navbar-toggler navbar-toggler-right d-block d-md-none" type="button" data-toggle="sidebar">
                            <span class="navbar-toggler-icon"></span>
                        </button>


                        <!-- Navbar Brand -->
                        <a href="index.html" class="navbar-brand ">
                            <img class="navbar-brand-icon" src="{{asset('assets/images/stack-logo-white.svg')}}" width="22" alt="Stack">
                        <span>{{Session::get('faskes')}} {{Session::get('faskes_detail')}}</span>
                        </a>

                        <ul class="nav navbar-nav ml-auto d-none d-md-flex">
                            <li class="nav-item dropdown mr-3">
                                <a href="#notifications_menu" class="nav-link dropdown-toggle" data-toggle="dropdown" data-caret="false">
                                    <i class="material-icons nav-icon navbar-notifications-indicator">notifications</i>
                                </a>
                                <div id="notifications_menu" class="dropdown-menu dropdown-menu-right navbar-notifications-menu">
                                    <div class="dropdown-item d-flex align-items-center py-2">
                                        <span class="flex navbar-notifications-menu__title m-0">Notifications</span>
                                        <a href="javascript:void(0)" class="text-muted"><small>Clear all</small></a>
                                    </div>
                                    <div class="navbar-notifications-menu__content" data-simplebar>
                                        <div class="py-2 dataNotif">
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>

            </div>
        </div>

        <!-- // END Header -->

        <!-- Header Layout Content -->
        <div class="mdk-header-layout__content">

            <div class="mdk-drawer-layout js-mdk-drawer-layout" data-push data-responsive-width="992px">
                <div class="mdk-drawer-layout__content page">





                    <div class="container-fluid page__heading-container">
                        <div class="page__heading d-flex align-items-center">
                            <div class="flex">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb mb-0">
                                        <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-home"></i></li>
                                        <li class="breadcrumb-item"><a href="{{$mainMenuLink}}">{{$mainMenu}}</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>




                    <div class="container-fluid page__container">
                        @if(isset($information))
                        <div class="alert alert-soft-warning d-flex align-items-center card-margin" role="alert">
                            <i class="material-icons mr-3">error_outline</i>
                            <div class="text-body"><strong>API gateways are now Offline.</strong> Please try the API later. If you want to stay up to date follow our <a href="">Status Page </a></div>
                        </div>
                        @endif
                        @if(session('success'))
                        <div id="information" class="alert alert-soft-success d-flex align-items-center card-margin" role="alert">
                            <i class="fa fa-check"></i>
                            <div class="text-body"><strong>{{session('success')}}</strong></div>
                        </div>
                        @endif
                        @if(session('failed'))
                        <div id="information" class="alert alert-soft-danger d-flex align-items-center card-margin" role="alert">
                            <i class="fa fa-warning"></i>
                            <div class="text-body"><strong>{{session('failed')}}</strong></div>
                        </div>
                        @endif
                        @yield('content')
                    </div>



                </div>
                <!-- // END drawer-layout__content -->

                <div class="mdk-drawer  js-mdk-drawer" id="default-drawer" data-align="start">
                    <div class="mdk-drawer__content">
                        <div class="sidebar sidebar-light sidebar-left simplebar" data-simplebar>
                            <div class="d-flex align-items-center sidebar-p-a border-bottom sidebar-account">
                                <a href="profile.html" class="flex d-flex align-items-center text-underline-0 text-body">
                                    <span class="avatar mr-3">
                                        <img src="{{asset('assets/images/admin.png')}}" alt="avatar" class="avatar-img rounded-circle">
                                    </span>
                                    <span class="flex d-flex flex-column">
                                        <strong>{{Session::get('nama_pegawai')}}</strong>
                                        {{-- <small class="text-muted text-uppercase">Account Manager</small> --}}
                                    </span>
                                </a>
                                <div class="dropdown ml-auto">
                                    <a href="#" data-toggle="dropdown" data-caret="false" class="text-muted"><i class="material-icons">more_vert</i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        {{-- <div class="dropdown-item-text dropdown-item-text--lh">
                                            <div><strong>Adrian Demian</strong></div>
                                            <div>@adriandemian</div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item active" href="index.html">Dashboard</a>
                                        <a class="dropdown-item" href="profile.html">My profile</a>
                                        <a class="dropdown-item" href="edit-account.html">Edit account</a> --}}
                                        {{-- <div class="dropdown-divider"></div> --}}
                                        <a class="dropdown-item" href="{{url('/rumah-sakit/logout')}}">Logout</a>
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar-heading sidebar-m-t">Menu</div>
                            <ul class="sidebar-menu">
                                <li class="sidebar-menu-item text-danger @if($mainMenu == 'Dashboard') active open @endif ">
                                    <a class="sidebar-menu-button" href="{{url('rumah-sakit')}}">
                                        <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons text-danger">dvr</i>
                                        <span class="sidebar-menu-text">Dashboards</span>
                                        {{-- <span class="ml-auto sidebar-menu-toggle-icon"></span> --}}
                                    </a>
                                </li>
                                <li class="sidebar-menu-item text-danger @if($mainMenu == 'Pengaduan') active open @endif ">
                                    <a class="sidebar-menu-button" href="{{url('rumah-sakit/pengaduan')}}">
                                        <i class="sidebar-menu-icon sidebar-menu-icon--left fas fa-file-medical-alt text-danger"></i>
                                        <span class="sidebar-menu-text">Pengaduan</span>
                                        {{-- <span class="ml-auto sidebar-menu-toggle-icon"></span> --}}
                                    </a>
                                </li>
                                {{-- <li class="sidebar-menu-item @if($mainMenu == 'Public Health Center') active open @endif ">
                                    <a class="sidebar-menu-button" href="{{url('publichealth/')}}">
                                        <i class="sidebar-menu-icon sidebar-menu-icon--left fas fa-procedures"></i>
                                        <span class="sidebar-menu-text">Public Health Center</span> --}}
                                        {{-- <span class="ml-auto sidebar-menu-toggle-icon"></span> --}}
                                    {{-- </a>
                                </li>
                                <li class="sidebar-menu-item @if($mainMenu == 'Ambulance Spot') active open @endif ">
                                    <a class="sidebar-menu-button" href="{{url('posko/')}}">
                                        <i class="sidebar-menu-icon sidebar-menu-icon--left fas fa-ambulance"></i>
                                        <span class="sidebar-menu-text">Ambulan Spot</span> --}}
                                        {{-- <span class="ml-auto sidebar-menu-toggle-icon"></span> --}}
                                    {{-- </a>
                                </li> --}}

                                {{-- <li class="sidebar-menu-item @if($mainMenu == 'Territory') active open @endif" data-toggle="collapse" href="#apps_menu">
                                    <a class="sidebar-menu-button" data-toggle="collapse" href="#apps_menu">
                                        <i class="sidebar-menu-icon sidebar-menu-icon--left fa fa-map-marked"></i>
                                        <span class="sidebar-menu-text">Territory</span>
                                        <span class="ml-auto sidebar-menu-toggle-icon"></span>
                                    </a>
                                    <ul class="sidebar-submenu collapse" id="apps_menu">
                                        <li class="sidebar-menu-item @if($currentPage == 'Sub District') active @endif">
                                            <a class="sidebar-menu-button" href="{{'sub-district'}}">
                                                <span class="sidebar-menu-text">Sub-District</span>
                                            </a>
                                        </li>
                                        <li class="sidebar-menu-item @if($currentPage == 'Village Area') active @endif ">
                                            <a class="sidebar-menu-button" href="{{url('village')}}">
                                                <span class="sidebar-menu-text">Village Area</span>
                                            </a>
                                        </li>
                                        <li class="sidebar-menu-item">
                                            <a class="sidebar-menu-button" href="app-fullcalendar.html">
                                                <span class="sidebar-menu-text">Event Calendar</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li> --}}
                            </ul>
                            </div>

                            <div class="sidebar-p-a">
                                <a href="https://themeforest.net/item/stack-admin-bootstrap-4-dashboard-template/22959011" class="btn btn-outline-primary btn-block">Purchase Stack &dollar;35</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- // END drawer-layout -->

        </div>
        <!-- // END header-layout__content -->

    </div>
    <!-- // END header-layout -->

    <!-- App Settings FAB -->
    <div id="app-settings">
        <app-settings layout-active="default" :layout-location="{
      'default': 'index.html',
      'fixed': 'fixed-dashboard.html',
      'fluid': 'fluid-dashboard.html',
      'mini': 'mini-dashboard.html'
    }"></app-settings>
    </div>

    <!-- jQuery -->
    <script src="{{asset('assets/vendor/jquery.min.js')}}"></script>

    <!-- Bootstrap -->
    <script src="{{asset('assets/vendor/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap.min.js')}}"></script>

    <!-- Simplebar -->
    <script src="{{asset('assets/vendor/simplebar.min.js')}}"></script>

    <!-- DOM Factory -->
    <script src="{{asset('assets/vendor/dom-factory.js')}}"></script>

    <!-- MDK -->
    <script src="{{asset('assets/vendor/material-design-kit.js')}}"></script>

    <!-- App -->
    <script src="{{asset('assets/js/toggle-check-all.js')}}"></script>
    <script src="{{asset('assets/js/check-selected-row.js')}}"></script>
    <script src="{{asset('assets/js/dropdown.js')}}"></script>
    <script src="{{asset('assets/js/sidebar-mini.js')}}"></script>
    <script src="{{asset('assets/js/app.js')}}"></script>

    <!-- App Settings (safe to remove) -->
    <script src="{{asset('assets/js/app-settings.js')}}"></script>



    <!-- Flatpickr -->
    <script src="{{asset('assets/vendor/flatpickr/flatpickr.min.js')}}"></script>
    <script src="{{asset('assets/js/flatpickr.js')}}"></script>

    <!-- Global Settings -->
    <script src="{{asset('assets/js/settings.js')}}"></script>

    <!-- Chart.js')}} -->
    <script src="{{asset('assets/vendor/Chart.min.js')}}"></script>

    <!-- App Charts JS -->
    <script src="{{asset('assets/js/charts.js')}}"></script>

    <!-- Chart Samples -->
    <script src="{{asset('assets/js/page.dashboard.js')}}"></script>

    <!-- Vector Maps -->
    <script src="{{asset('assets/vendor/jqvmap/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/jqvmap/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{asset('assets/js/vector-maps.js')}}"></script>

    {{-- Datatable --}}
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

    {{-- Highchart --}}
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    @yield('script')

    <script>
        $(document).ready(function() {
            var awal = $('.awal').val();
            var akhir = $('.akhir').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "get",
                url: "rumah-sakit/data-notif",
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    $('.dataNotif').html('');
                    for(var i = 0; i < data.length; i++){
                        $('.dataNotif').append('<div class="dropdown-item d-flex"><div class="mr-3"><div class="avatar avatar-sm" style="width: 32px; height: 32px;"><img src="' + data[i].reporter_foto +'" alt="Avatar" class="avatar-img rounded-circle"></div></div><div class="flex"><a href="' + data[i].link_laporan_rumah_sakit + '/' + data[i].id + '"><span>'+ data[i].reporter_name +'</span> Melapor di wilayah <span>'+ data[i].village_name +'</span><br></a><small>'+ data[i].estimasi +'</small></div></div>');
                    }
                },
                error: function(data) {
                    console.log('Error : ', data);
                }
            });
        });

        setInterval(function() {
            var awal = $('.awal').val();
            var akhir = $('.akhir').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "get",
                url: "rumah-sakit/data-notif",
                dataType: 'json',
                success: function(data) {
                    $('.dataNotif').html('');
                    for(var i = 0; i < data.length; i++){
                        $('.dataNotif').append('<div class="dropdown-item d-flex"><div class="mr-3"><div class="avatar avatar-sm" style="width: 32px; height: 32px;"><img src="' + data[i].reporter_foto +'" alt="Avatar" class="avatar-img rounded-circle"></div></div><div class="flex"><a href="' + data[i].link_laporan_rumah_sakit + '/' + data[i].id + '"><span>'+ data[i].reporter_name +'</span> Melapor di wilayah <span>'+ data[i].village_name +'</span><br></a><small>'+ data[i].estimasi +'</small></div></div>');
                        // var audio = document.getElementById('audio_sirine');
                        // audio.play();
                        console.log('Ada Pengaduan!');
                    }
                },
                error: function(data) {
                    console.log('Error : ', data);
                }
            });
        }, 5000);
    </script>

</body>

</html>
