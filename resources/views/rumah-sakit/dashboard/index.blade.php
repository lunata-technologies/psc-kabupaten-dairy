@extends('rumah-sakit.app-layout.master', [
    'mainMenuLink' => url('rumah-sakit'),
    'mainMenu' => 'Dashboard',
    'currentPage' => null,
])

@section('title', 'Dashboard')

@section('content')
    <form method="" action="">
        <div class="card card-form d-flex flex-column flex-sm-row">
            <div class="card-form__body card-body-form-group flex">
                <div class="row">
                    <div class="col-sm-auto mx-auto">
                        <div class="form-group" style="width: 200px;">
                            <label for="flatpickrSample01"></label>
                            <input id="flatpickrSample01" name="awal" type="text" class="form-control awal" placeholder="Flatpickr example" data-toggle="flatpickr" value="{{$reqAwal}}">
                        </div>
                    </div>
                    <div class="col-sm-auto">
                        <div class="form-group">
                            <label for="filter_stock"></label>
                            <div class="custom-control custom-checkbox mt-sm-2">
                                <label class="" for="filter_stock">TO</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-auto mx-auto">
                        <div class="form-group" style="width: 200px;">
                            <label for="flatpickrSample01"></label>
                            <input id="flatpickrSample01" name="akhir" type="text" class="form-control akhir" placeholder="Flatpickr example" data-toggle="flatpickr" value="{{$reqAkhir}}">
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn bg-white border-left border-top border-top-sm-0 rounded-top-0 rounded-top-sm rounded-left-sm-0"><i class="material-icons text-primary icon-20pt">refresh</i></button>
        </div>
    </form>

    <div class="row">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <figure class="highcharts-figure">
                    <div id="container-bar"></div>
                </figure>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        Highcharts.chart('container-bar', {
            colors: ['#ffc107','#fd7e14','#dc3545','#36abd7'],
            chart: {
                type: 'column'
            },
            title: {
                text: 'Pasien'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: {!! json_encode($tmpKecamatan) !!},
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Pasien / Kecamatan'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: {!! json_encode($aduanPerkecamatan) !!},
        });
    </script>
@endsection
