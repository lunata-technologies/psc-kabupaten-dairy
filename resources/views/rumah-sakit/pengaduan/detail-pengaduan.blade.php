@extends('rumah-sakit.app-layout.master', [
    'mainMenuLink' => url('rumah-sakit/pengaduan'),
    'mainMenu' => 'Pengaduan',
    'currentPage' => null,
])

@section('title', 'Pengaduan')

@section('content')
<div class="row p-2 mb-2">
    <div class="col-8">
        <i class="fas fa-building"></i>
        <span class="ml-2">Data Pengaduan</span>
    </div>
    @if($data['rest_accept_date'] == null)
        <div class="col-4">
            <span class="btn btn-md btn-danger" onclick="confirm_me('Diterima', '/rumah-sakit/terima-pasien/{!!$data['id']!!}', 'Terima Pasien, Yakin ?')">Terima Pasien</span>
            {{-- $hapus = '<span title="Un Approve" class="btn-sm btn-danger py-1 px-2" onclick="confirm_me('.$link_delete.')"><i class="fas fa-trash"></i></span>'; --}}
            {{-- $link_delete = "'Hapus', 'delete-puskesmas/".$puskesmas->id."', 'Data akan dihapus, Yakin ?'"; --}}
        </div>
    @endif
</div>
{{-- @if($data['rescuer_id'] != null && $data['rest_id'] != null && !isset($_GET['edit'])) --}}
    <div class="col-md-12 col-lg-8">
        {{-- <div class="alert alert-success">
            <i class="fas fa-check"></i>
            <span class="ml-2">Laporan ini telah ditindak lanjuti, terimakasih !</span>
        </div> --}}
        <div class="col">
            <h6><i class="fas fa-laptop-code"></i> <span class="ml-2 bold">Keterangan Laporan</span></h6>
        <p style="text-secondary"><h6>{!!$data['description']!!}</h6></p>
            <hr>
        </div>
        <div class="col mt-4">
            <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Jenis Laporan</span></h6>
            <p style="text-secondary"><h6>@if($data['report_type'] == 1) Diri Sendiri @elseif($data['report_type']==2) Orang Lain @else Panic Button @endif</h6></p>
            <hr>
        </div>
        <div class="col mt-4">
            <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Jenis Penyakit</span></h6>
            <p style="text-secondary"><h6>{{$data['diseases_name']}}</h6></p>
            <hr>
        </div>
        <div class="col mt-4">
            <div class="row">
                <div class="col-6">
                    <div class="card text-white bg-danger mb-3">
                        <div class="card-header bg-danger">
                            <i class="fas fa-ambulance"></i> <span class="ml-2">Ambulance</span>
                            {{-- <a href="{{url('laporan-detail/'.$data['id'].'?edit=true&posko=true')}}"><i class="fas fa-edit text-white float-right" style="cursor:pointer;"></i></a> --}}
                        </div>
                        <div class="card-body">
                            <h5 class="card-title text-white">{{$data['posko_name']}}</h6>
                            <p class="card-text">{{$data['posko_name']}} telah diminta untuk menangani laporan ini !</p>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card text-white bg-success mb-3">
                        <div class="card-header bg-success">
                            <i class="fas fa-building"></i> <span class="ml-2">Hospital</span>
                            {{-- <a href="{{url('laporan-detail/'.$data['id'].'?edit=true&hospital=true')}}"><i class="fas fa-edit text-white float-right" style="cursor:pointer;"></i></a> --}}
                        </div>
                        <div class="card-body">
                            <h5 class="card-title text-white">{{$data['rest_name']}}</h6>
                            <p class="card-text">{{$data['rest_name']}} telah diminta untuk menjadi lokasi rujukan laporan ini !</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col mt-4">
            <h6><i class="fab fa-typo3"></i> <span class="ml-2 bold">Progress Pelayanan</span></h6><p></p>
            @if($data['admin_confirmed'] != null)
                <p style="text-secondary"><i class="far fa-circle" style="font-size:9px"></i><span class="ml-2">Dikonfirmasi oleh admin PSC</span> <span class="float-right">{{$data['admin_confirmed']}}</span></p>
            @endif
            @if($data['rescuer_confirmed'] != null)
                <p style="text-secondary"><i class="far fa-circle" style="font-size:9px"></i><span class="ml-2">Dikonfirmasi oleh pihak Posko</span> <span class="float-right">{{$data['rescuer_confirmed']}}</span></p>
            @endif
            @if($data['rescuer_otw'] != null)
                <p style="text-secondary"><i class="far fa-circle" style="font-size:9px"></i><span class="ml-2">Ambulan menuju lokasi</span> <span class="float-right">{{$data['rescuer_otw']}}</span></p>
            @endif
            @if($data['rescuer_arrived_people'] != null)
                <p style="text-secondary"><i class="far fa-circle" style="font-size:9px"></i><span class="ml-2">Ambulance menuju rumah sakit</span> <span class="float-right">{{$data['rescuer_arrived_people']}}</span></p>
            @endif
            @if($data['rescuer_arrived_hospital'] != null)
                <p style="text-secondary"><i class="far fa-circle" style="font-size:9px"></i><span class="ml-2">Ambulance tiba di rumah sakit</span> <span class="float-right">{{$data['rescuer_arrived_hospital']}}</span></p>
            @endif
            <hr>
        </div>
    </div>
{{-- @endif --}}

@endsection

@section('script')
    <script>

    </script>
@endsection
