@extends('rumah-sakit.app-layout.master', [
    'mainMenuLink' => url('rumah-sakit/pengaduan'),
    'mainMenu' => 'Pengaduan',
    'currentPage' => null,
])

@section('title', 'Pengaduan')

@section('content')
<div class="row p-2">
    <div class="col-12">
        <i class="fas fa-building"></i>
        <span class="ml-2">Daftar Pengaduan</span>
    </div>
</div>
<div class="row p-2">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="container">
                    <div class="table-responsive-lg table-responsive-md table-responsive-sm">
                        <table class="table table-striped table-bordered text-center" cellspacing="0" id="table_laporan_rumah_sakit" style="width: 100%" style="width: 100%">
                            <thead>
                                <tr>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Penyakit</th>
                                    <th scope="col">Kota</th>
                                    <th scope="col">Desa</th>
                                    <th scope="col">Posko</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script>
        $(function(){
            var table = $('#table_laporan_rumah_sakit').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: '{{ url('rumah-sakit/pengaduan-rumah-sakit-datatable') }}/',
                columns: [
                    {data: 'reporter', name: 'reporter'},
                    {data: 'nama_penyakit', name: 'nama_penyakit'},
                    {data: 'sub_district', name: 'sub_district'},
                    {data: 'village', name: 'village'},
                    {data: 'posko', name: 'posko'},
                    {data: 'status', name: 'status'},
                    {data: 'aksi', name: 'aksi'}
                ],
                "language": {
                    "infoEmpty": "No records available",
                    "emptyTable": "No Data Available",
                },
                "paging": true,
                "ordering": false,
                "info": true,
                "select": true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        });
    </script>
@endsection
