<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Get AREA COVERAGE
Route::get('sub-district', 'API\TerritoryController@getSubDistrict');
Route::get('village-area/{id}', 'API\TerritoryController@getVillageArea');

// Get Diseases
Route::get('diseases', 'API\DiseasesController@getDiseases');

Route::post('logout', 'API\AuthController@logout');
Route::post('login','API\AuthController@login');
Route::post('register', 'API\AuthController@register');
Route::post('register-verifying-selfie', 'API\AuthController@registerVerifyingSelfie');
Route::post('register-verifying-selfie-ktp', 'API\AuthController@registerVerifyingSelfieKtp');
Route::namespace('API')->middleware('auth:api')->group(function(){    
    // Get Data User Login
    Route::post('user-data', 'AuthController@UserData');

    // People Report or Etc.
    Route::post('people-report', 'PeopleController@peopleReport');
    Route::post('report-status', 'PeopleController@reportStatus');
    Route::post('laporan-log', 'PeopleController@laporanLog');

    
});



// Supir Ambulance
Route::post('login-pegawai', 'API\AuthController@loginPegawai');
Route::post('report-data', 'API\PeopleController@reportData');
Route::post('confirm', 'API\PeopleController@confirmPickup');