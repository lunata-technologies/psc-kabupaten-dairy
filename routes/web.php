<?php
use App\Http\Middleware\AuthRumahSakit;
Auth::routes();

Route::prefix('admin')->group(function(){
    Route::post('register', 'AdminController@register');
});
Route::post('login-first', 'AdminController@login')->name('login-first');
Route::get('logout', 'AdminController@logout');
Route::middleware('AuthAja')->group(function(){
    Route::get('/', 'DashboardController@index');

    Route::get('admin', 'DashboardController@admin');
    Route::get('admin-delete/{uid}', 'DashboardController@deleteAdmin');

    Route::get('notif', 'DashboardController@Notif');
    Route::get('cek/{awal}/{akhir}', 'DashboardController@grafikLaporanPerJenisAduan');

    // Hospital
    Route::get('hospitals', 'HospitalController@index');
    Route::post('hospitals', 'HospitalController@index');
    Route::get('hospital-datatable', 'HospitalController@hospitalDatatable');
    Route::get('delete-hospital/{id}', 'HospitalController@deleteHospital');
    Route::get('detail-hospital/{id}', 'HospitalController@detailHospital');
    Route::post('detail-hospital/{id}', 'HospitalController@detailHospital');
    Route::post('update-hospital', 'HospitalController@updateHospital');
    Route::post('hospital-posko/{id}', 'HospitalController@detailHospital');
    Route::post('hospital-desa-cover', 'HospitalController@HospitalCoverArea');

    // Public Health
    Route::get('publichealth', 'PublicHealthController@index');
    Route::post('publichealth', 'PublicHealthController@index');
    Route::get('puskesmas-datatable', 'PublicHealthController@puskesmasDatatable');
    Route::get('delete-puskesmas/{id}', 'PublicHealthController@deletePuskesmas');
    Route::get('update-detail-puskesmas/{id}', 'PublicHealthController@updateDetailPuskesmas');
    Route::get('detail-puskesmas/{id}', 'PublicHealthController@detailPuskesmas');
    Route::post('detail-puskesmas/{id}', 'PublicHealthController@detailPuskesmas');
    Route::post('update-puskesmas', 'PublicHealthController@updatePuskesmas');
    Route::get('ajax-edit-pegawai-publichealth/{uid}', 'PublicHealthController@ajaxEditPegawaiPublicHealth');
    Route::post('puskesmas-desa-cover', 'PublicHealthController@PuskesmasCoverArea');

    // Posko
    Route::get('posko', 'PoskoController@index');
    Route::post('posko', 'PoskoController@index');
    Route::get('posko-datatable', 'PoskoController@poskoDatatable');
    Route::get('delete-posko/{id}', 'PoskoController@deletePosko');
    Route::get('detail-posko/{id}', 'PoskoController@detailPosko');
    Route::post('detail-posko/{id}', 'PoskoController@detailPosko');
    Route::get('update-detail-posko/{id}', 'PoskoController@updateDetailPosko');
    Route::post('update-posko', 'PoskoController@updatePosko');
    Route::get('ajax-edit-pegawai-posko/{uid}', 'PoskoController@ajaxEditPegawaiPosko');
    Route::post('edit-pegawai-posko', 'PoskoController@editPegawaiPosko');
    Route::post('posko-desa-cover', 'PoskoController@PoskoCoverArea');
    Route::post('posko-shift', 'PoskoController@PoskoShift');

    // Shift
    Route::get('delete-shift/{id}/{status}', 'PoskoController@deleteShift');

    // Diseases
    Route::get('diseases', 'DiseasesController@index');
    Route::post('diseases', 'DiseasesController@index');
    Route::get('penyakit-datatable', 'DiseasesController@penyakitDatatable');
    Route::get('delete-penyakit/{id}', 'DiseasesController@deletePenyakit');
    Route::get('detail-penyakit/{id}', 'DiseasesController@detailPenyakit');
    Route::post('update-penyakit', 'DiseasesController@updatePenyakit');

    // Member
    Route::get('member', 'MemberController@index');
    Route::get('member-datatable', 'MemberController@memberDatatable');
    Route::get('approve/member/{uid}', 'MemberController@approveMember');
    Route::get('member-detail/{uid}', 'MemberController@memberDetail');
    Route::post('update-member', 'MemberController@tolakMember');

    Route::get('maps', 'MemberController@map');

    // Pengaduan
    Route::get('test-notif', 'LaporanController@testNotif');
    Route::get('pengaduan', 'LaporanController@index');
    Route::get('pengaduan-datatable/{nilai}', 'LaporanController@laporanDatatable');
    Route::get('laporan-detail/{id}', 'LaporanController@laporanDetail');
    Route::post('laporan-process', 'LaporanController@laporanProcess');
    Route::post('laporan-decline', 'LaporanController@laporanDecline');
    Route::post('konfirmasi-pengaduan', 'LaporanController@konfirmasiPengaduan');
    Route::get('get-another-posko/{id}', 'LaporanController@getAnotherPosko');
    Route::post('laporan-completing', 'LaporanController@laporanCompleting');

    // Laporan
    Route::get('laporan', 'LaporanController@dataLaporan');
    Route::get('laporan-pengaduan', 'LaporanController@dataPengaduan');
    Route::get('laporan-pengaduan-datatable', 'LaporanController@dataTableLaporanPengaduan');


    // Territory
    Route::get('sub-district', 'TerritoryController@SubDistrict');
    Route::get('sub-district-detail/{id}','TerritoryController@SubDistrictDetail');
    Route::post('sub-district', 'TerritoryController@SubDistrict');
    Route::post('sub-district-edit', 'TerritoryController@SubDistrictEdit');
    Route::post('sub-district-delete', 'TerritoryController@SubDistrictDelete');
    // ------------------------------------------------------- //
    Route::get('village','TerritoryController@Village');
    Route::post('village','TerritoryController@Village');
    Route::get('village-edit/{id}','TerritoryController@VillageEdit');
    Route::post('village-edit/{id}','TerritoryController@VillageEdit');
    Route::get('village-delete/{id}','TerritoryController@VillageDelete');
    Route::post('village-subdistrict', 'TerritoryController@VillageSubdistrict');
});

    Route::get('rumah-sakit/login', 'RumahSakit\RumahSakitController@login')->name('rumah-sakit/login');
    Route::post('rumah-sakit/login', 'RumahSakit\RumahSakitController@login')->name('rumah-sakit/login');
    Route::get('rumah-sakit/logout', 'RumahSakit\RumahSakitController@logout')->name('rumah-sakit/logout');

Route::middleware('AuthRumahSakit')->group(function () {
    Route::get('rumah-sakit', 'RumahSakit\RSDashboardController@index')->name('rumah-sakit');
    Route::get('rumah-sakit/pengaduan', 'RumahSakit\RSPengaduanController@index')->name('rumah-sakit/pengaduan');
    Route::get('rumah-sakit/pengaduan-rumah-sakit-datatable', 'RumahSakit\RSPengaduanController@pengaduanDataTable')->name('rumah-sakit/pengaduan-rumah-sakit-datatable');
    Route::get('rumah-sakit/detail-laporan/{id}', 'RumahSakit\RSPengaduanController@detailPengaduan')->name('rumah-sakit/detail-laporan/{id}');
    Route::get('rumah-sakit/terima-pasien/{id}', 'RumahSakit\RSPengaduanController@terimaPasien')->name('rumah-sakit/terima-pasien/{id}');
    Route::get('rumah-sakit/data-notif', 'RumahSakit\RSDashboardController@notifRumahSakit')->name('rumah-sakit/data-notif');
});

//     Route::get('puskesmas/login', 'Puskesmas\PuskesmasController@login')->name('puskesmas/login');
//     Route::post('puskesmas/login', 'Puskesmas\PuskesmasController@login')->name('puskesmas/login');
//     Route::get('puskesmas/logout', 'Puskesmas\PuskesmasController@logout')->name('puskesmas/logout');

// Route::middleware('AuthPuskesmas')->group(function () {
//     Route::get('puskesmas', 'Puskesmas\DashboardController@index')->name('puskesmas');
//     Route::get('puskesmas/pegawai', 'Puskesmas\PegawaiController@index')->name('puskesmas/pegawai');
// });
